import { Button } from "@/atoms/index";
import React from "react";
import { FormBody } from "./Components";
import { Wrapper } from "./styles";
import T from "prop-types";

const ContactForm = ({ className, ...restProps }) => {
  //TODO починить стор
  const contactForm = {};

  //TODO починить стор
  const dispatch = () => {};
  const list = contactForm.list;

  return (
    <Wrapper className={className}>
      <FormBody {...restProps} list={list} className="mb2" />
      <Button
        onClick={() => null}
        className="mb2"
        type="reversed"
        color="green"
      >
        Добавить элемент
      </Button>
      <Button color="green">Сохранить</Button>
    </Wrapper>
  );
};

ContactForm.propTypes = {
  list: T.array,
  className: T.string,
};

ContactForm.defaultProps = {
  list: [],
  className: "",
};

export default ContactForm;
