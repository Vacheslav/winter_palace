import styled from "styled-components";
import { lighten } from "polished";

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  padding-bottom: 2px;
  justify-content: center;
  flex-direction: column;
  row-gap: 5px;
  & .title {
    max-width: 100px;
    text-align: center;
    font-weight: 500;
  }
  & .count {
    font-weight: 700;
    font-size: 18px;
  }
`;

export const Icon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${({ color }) => lighten(0.65, color)};
  border-radius: 100%;
  height: 50px;
  width: 50px;
`;
