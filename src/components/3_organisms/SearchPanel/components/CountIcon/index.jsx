import React from "react";
import { Wrapper, Icon } from "./styles";
import { Train } from "../../icons";

export default ({ count = 0, color = "", title = "" }) => {
  return (
    <Wrapper color={color}>
      <Icon color={color}>
        <Train
          styles={{
            color,
          }}
        />
      </Icon>
      <div className="title">{title}</div>
      <div className="count">{count}</div>
    </Wrapper>
  );
};
