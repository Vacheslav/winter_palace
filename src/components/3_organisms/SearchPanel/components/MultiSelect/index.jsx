//@ts-nocheck
import React, { useLayoutEffect, useState } from "react";
import { Select, Tooltip } from "antd";
import { randKey } from "@/utils/index";
import { Wrapper } from "./style";

const MultiSelect = ({
  list,
  placeholder = "",
  onChange,
  value = null,
  ...restProps
}) => {
  const { Option } = Select;
  const [scrollWidth, setstate] = useState(0);

  useLayoutEffect(() => {
    setstate(
      Math.max(
        document.body.scrollWidth,
        document.documentElement.scrollWidth,
        document.body.offsetWidth,
        document.documentElement.offsetWidth,
        document.body.clientWidth,
        document.documentElement.clientWidth
      )
    );
  }, []);
  return (
    <Wrapper {...restProps}>
      <Select
        maxTagPlaceholder={(list) => {
          return (
            <Tooltip
              title={
                scrollWidth > 550
                  ? value?.length
                    ? value.join(", ")
                    : null
                  : null
              }
            >
              <div>{list && `...+${list?.length}`}</div>
            </Tooltip>
          );
        }}
        className="multi-select"
        mode="multiple"
        style={{ width: "100%" }}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        maxTagCount="responsive"
      >
        {list &&
          list?.map((el) => (
            <Option key={randKey()} value={el}>
              <Tooltip
                title={
                  scrollWidth > 550
                    ? value?.length
                      ? value.join(", ")
                      : null
                    : null
                }
              >
                {el}
              </Tooltip>
            </Option>
          ))}
      </Select>
    </Wrapper>
  );
};

export default MultiSelect;
