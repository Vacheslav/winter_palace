import styled from "styled-components";

export const Wrapper = styled.div`
  padding: 10px 20px;
  font-weight: 600;
  font-size: 12px;
  color: gray;
  display: flex;
  column-gap: 5px;
`;
