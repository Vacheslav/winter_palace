import { Wrapper } from "./styles";

export default ({ machineType, railways, orgs }) => {
  const machineName = machineType?.name || null;
  const railwayName = railways?.name || null;
  const orgsName = orgs?.name || null;

  const path = [machineName, railwayName, orgsName].map(
    (el) => el && <div className="legend">{el},</div>
  );

  if (!machineName) return null;

  return <Wrapper>Выбрано: {path}</Wrapper>;
};
