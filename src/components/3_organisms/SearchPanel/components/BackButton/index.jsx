/* eslint-disable import/no-anonymous-default-export */
import React from "react";
import { BackIcon } from "../../icons";
import { Wrapper } from "./styles";

export default (props) => {
  return (
    <Wrapper {...props}>
      <BackIcon
        styles={{
          color: "gray",
          marginRight: 5,
          marginBottom: 2,
        }}
        size={9}
      />
      <p
        style={{
          color: "gray",
          fontSize: 12,
          marginBottom: 0,
        }}
      >
        Предыдущая
      </p>
    </Wrapper>
  );
};
