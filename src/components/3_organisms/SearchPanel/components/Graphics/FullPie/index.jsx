import React from "react";

export default ({ data, changeStep }) => {
  const { hadConnection, hadNotConnection, machines } = data[0];

  return (
    <div
      style={{
        width: "100%",
        height: "100%",
        position: "relative",
      }}
      onClick={() => changeStep(1)}
    >
      <div
        style={{
          position: "absolute",
          top: "50%",
          right: "50%",
          transform: "translate(50%, -84%)",
          textAlign: "center",
        }}
      >
        {machines}
        <div>всего</div>
      </div>
      <ResponsivePie
        data={[
          {
            id: "Выходили на связь",
            value: hadConnection,
          },
          {
            id: "Не выходили на связь",
            value: hadNotConnection,
          },
        ]}
        margin={{ bottom: 20 }}
        innerRadius={0.55}
        padAngle={0.9}
        enableArcLinkLabels={false}
        cornerRadius={0}
        activeOuterRadiusOffset={0}
        borderWidth={1}
        borderColor={{ from: "color", modifiers: [["darker", 0.2]] }}
        colors={["#FF7878", "#3EE489"]}
        defs={[
          {
            id: "dots",
            type: "patternDots",
            background: "inherit",
            color: "rgba(255, 255, 255, 0.3)",
            size: 4,
            padding: 1,
            stagger: true,
          },
          {
            id: "lines",
            type: "patternLines",
            background: "inherit",
            color: "rgba(255, 255, 255, 0.3)",
            rotation: -45,
            lineWidth: 6,
            spacing: 10,
          },
        ]}
        fill={[
          {
            match: {
              id: "Выходили на связь",
            },
          },
          {
            match: {
              id: "Не выходили на связь",
            },
          },
        ]}
      />
    </div>
  );
};
