import BarGraphic from "../BarGraphic";

export default ({ data = [], changeStep, setType }) => {
  return (
    <BarGraphic
      setData={setType}
      changeStep={changeStep}
      data={data.map(({ machines, classificationParentName, ...props }) => ({
        machine: `${classificationParentName || ""} (${machines})`,
        classificationParentName,
        ...props,
      }))}
      newStep={2}
      fieldName="classificationParent"
    />
  );
};
