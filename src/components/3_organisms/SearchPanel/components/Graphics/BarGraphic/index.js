import { useEffect } from "react";

/**
 * SmartSearcher - шаблонный компонент для графиков
 *
 * @category organisms/BarGraphic/TemplateDashGraphic
 *
 * @param {Object} props - пропсы
 * @param {Array} props.data - массив данных для отображения графика
 * @param {Function} props.changeStep - функция перехода навый график
 * @param {Function} props.setData - функция которая устанавливает значения полей для графика
 * @param {number} props.newStep - значение нового шага
 * @param {string} props.fieldName - названия ключего поля графика. например railway
 * @param {number} props.left - отступ слева (чтобы график не обрезался)
 *
 * @example  <BarGraphic
      setData={setRailways}
      changeStep={changeStep}
      data={data.map(
        ({
          hadConnection,
          hadNotConnection,
          machines,
          railwayName,
          ...props
        }) => ({
          machine: `${railwayName} (${machines})`,
          hadConnection,
          hadNotConnection,
          railwayName,
          ...props,
        })
      )}
      newStep={3}
      fieldName="railway"
    />
 *
 */
export default ({
  data = [],
  changeStep,
  setData,
  newStep,
  fieldName,
  left = 80,
}) => {
  useEffect(() => {
    setData && setData(null);
  }, []);

  const keys = ["hadConnection", "hadNotConnection"];

  const commonProps = {
    margin: { bottom: 20, left },
    data,
    indexBy: "machine",
    keys,
    colors: ["#3EE489", "#FF7878"],
    layout: "horizontal",
    axisBottom: null,
  };

  const onClick = (graphData) => {
    if (changeStep && setData && fieldName) {
      const newProp = {
        name: graphData?.data?.[`${fieldName}Name`],
        id: graphData?.data?.[`${fieldName}Id`],
      };
      setData(newProp);
      changeStep(newStep, newProp);
    }
  };

  const newHeight = 300 + (data.length > 6 ? (data.length - 6) * 30 : 0);

  return (
    <div
      style={{
        width: 300,
        height: 300,
        overflowY: "auto",
        position: "relative",
      }}
    >
      <div
        style={{
          width: "100%",
          height: newHeight,
          position: "relative",
        }}
      >
        <ResponsiveBar onClick={onClick} {...commonProps} />
      </div>
    </div>
  );
};
