import { cond, T, equals } from "ramda";

import FullBar from "./FullBar";
import DrpBar from "./DrpBar";
import FullPie from "./FullPie";
import OrgsBar from "./OrgsBar";
import SPSBar from "./SPSBar";

export default ({ graphType, loading, ...props }) => {
  if (loading) return null;
  if (!props.data) return null;

  return cond([
    [equals("FullBar"), () => <FullBar {...props} />],
    [equals("FullPie"), () => <FullPie {...props} />],
    [equals("DrpBar"), () => <DrpBar {...props} />],
    [equals("OrgsBar"), () => <OrgsBar {...props} />],
    [equals("SPSBar"), () => <SPSBar {...props} />],
    [T, () => null],
  ])(graphType);
};
