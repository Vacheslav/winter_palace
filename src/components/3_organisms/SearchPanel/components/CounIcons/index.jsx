import React from "react";
import { Wrapper } from "./styles";
import CountIcon from "../CountIcon";
import { is } from "ramda";
import { randKey } from "@/utils/index";

export default ({ legend }) => {
  if (!is(Array, legend)) return null;
  return (
    <Wrapper>
      {legend &&
        legend?.map((props) => <CountIcon key={randKey()} {...props} />)}
    </Wrapper>
  );
};
