/* eslint-disable import/no-anonymous-default-export */
import React from "react";
import { DownloadIcon } from "./icons";

export default (props) => {
  return (
    <button
      style={{
        border: "1px solid #E1E1E1",
        borderRadius: 3,
        height: 50,
        width: 50,
        alignItems: "center",
        display: "flex",
        justifyContent: "center",
        backgroundColor: "white",
      }}
      className="flex items-center justify-center "
      {...props}
    >
      <DownloadIcon
        styles={{
          color: "#E1E1E1",
        }}
        size={25}
      />
    </button>
  );
};
