/* eslint-disable import/no-anonymous-default-export */
import React, { useState } from "react";
import { Select, Button } from "antd";
import { Wrapper, ContainerShadow, LeftPanel } from "./styles";
import { ViewSwitcher } from "@/atoms/index";
import { parseSepareteDate } from "@/utils/index";
import { MultiSelect } from "./components";

const { Option } = Select;

const today = new Date() - 1000 * 60 * 60 * 24;
const lastWeek = new Date() - 1000 * 60 * 60 * 24 * 7;
const lastMonth = new Date() - 1000 * 60 * 60 * 24 * 30;

export default ({
  setDtStart,
  drpList,
  orgList,
  typeList,
  zavList,
  lStorage,
  cLStorage,
  setNeedUpdate,
  ...restProps
}) => {
  const [active, setActive] = useState(1);
  const [date, setDate] = useState(today);

  const setData = (date) => {
    setDtStart(parseSepareteDate(date, "-", "DateYMD"));
  };

  const setToday = () => {
    const today = new Date() - 1000 * 60 * 60 * 24;
    setActive(1);
    setData(today);
  };

  const setLastWeek = () => {
    setActive(2);
    setData(lastWeek);
  };

  const setLastMonth = () => {
    setActive(3);
    setData(lastMonth);
  };

  return (
    <Wrapper>
      <LeftPanel>
        <MultiSelect
          className="my-select"
          list={drpList}
          field="drpName"
          placeholder="Дорога"
          value={lStorage?.drpName}
          onChange={(val) => {
            cLStorage("drpName", val);
          }}
        />
        <MultiSelect
          className="my-select"
          list={orgList}
          field="orgName"
          value={lStorage?.orgName}
          placeholder="Предприятие"
          onChange={(val) => {
            cLStorage("orgName", val);
          }}
        />
        <MultiSelect
          className="my-select"
          list={typeList}
          field="typeName"
          value={lStorage?.typeName}
          placeholder="Тип машины"
          onChange={(val) => {
            cLStorage("typeName", val);
          }}
        />
        <MultiSelect
          className="my-select"
          list={zavList}
          value={lStorage?.zavNomer}
          field="zavNomer"
          placeholder="Заводской номер"
          onChange={(val) => {
            cLStorage("zavNomer", val);
          }}
        />
        <Select
          value={date}
          onChange={(v) => {
            setData(v);
            setDate(v);
          }}
          className="select"
        >
          <Option title="За сутки" value={today}>
            За сутки
          </Option>
          <Option title="За неделю" value={lastWeek}>
            За неделю
          </Option>
          <Option title="За месяц" value={lastMonth}>
            За месяц
          </Option>
        </Select>
        <Button
          onClick={() => setNeedUpdate(true)}
          type="primary"
          className="button"
        >
          Выгрузить
        </Button>
      </LeftPanel>
      <ContainerShadow>
        <ViewSwitcher
          {...restProps}
          leftTitle="Общий вид"
          rightTitle="По типам"
        />
      </ContainerShadow>
    </Wrapper>
  );
};
