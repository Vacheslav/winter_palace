import { useMutation } from "@apollo/react-hooks";
import { ASKR_ON_OFF_REPORT_BYTYPES } from "../gql";

export default ({ setData, setLegend, dtStart, dtFinish }) => {
  const [getDataByTypes, { loadingByTypes, error }] = useMutation(
    ASKR_ON_OFF_REPORT_BYTYPES,
    {
      variables: {
        start: dtStart,
        finish: dtFinish,
      },
      onCompleted: (data) => {
        if (data?.askrOnOffReport[0]) {
          setData(data?.askrOnOffReport);
          setLegend([
            {
              color: "darkgreen",
              count: data?.askrOnOffReport.reduce(
                (acc, { hadConnection }) => acc + hadConnection,
                0
              ),
              title: "Выходили на связь",
            },
            {
              color: "darkred",
              count: data?.askrOnOffReport.reduce(
                (acc, { hadNotConnection }) => acc + hadNotConnection,
                0
              ),
              title: "Не выходили на связь",
            },
          ]);
        }
      },
    }
  );
  return { getDataByTypes, loadingByTypes, error };
};
