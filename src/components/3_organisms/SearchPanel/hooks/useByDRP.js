import { useMutation } from "@apollo/react-hooks";
import { GET_ON_OFF_AGREGATED_DRP_REPORT } from "../gql";

export default ({ setData, setLegend }) => {
  const [getDataByDrp, { loadingByDrp, error }] = useMutation(
    GET_ON_OFF_AGREGATED_DRP_REPORT,
    {
      onCompleted: (data) => {
        data?.askrOnOffReport && setData(data.askrOnOffReport);
        setLegend([
          {
            color: "darkgreen",
            count: data?.askrOnOffReport.reduce(
              (acc, { hadConnection }) => acc + hadConnection,
              0
            ),
            title: "Выходили на связь",
          },
          {
            color: "darkred",
            count: data?.askrOnOffReport.reduce(
              (acc, { hadNotConnection }) => acc + hadNotConnection,
              0
            ),
            title: "Не выходили на связь",
          },
        ]);
      },
      onError: (e) => console.log(e),
    }
  );
  return { getDataByDrp, loadingByDrp, error };
};
