import gql from "graphql-tag";

export const GET_ON_OFF_AGREGATED_DRP_REPORT = gql`
  mutation ($start: String, $finish: String, $classifications: [Int]) {
    askrOnOffReport(
      dates: { start: $start, finish: $finish }
      group_by: { classifications: $classifications, railways: [] }
    ) {
      id
      railwayId
      railwayName
      machines
      machinesTotal
      hadConnection
      hadNotConnection
    }
  }
`;

export const GET_ON_OFF_AGREGATED_ORG_REPORT = gql`
  mutation (
    $start: String
    $finish: String
    $railways: [Int]
    $classifications: [Int]
  ) {
    askrOnOffReport(
      dates: { start: $start, finish: $finish }
      group_by: {
        classifications: $classifications
        railways: $railways
        orgs: []
      }
    ) {
      id
      drpName
      machines
      orgId
      orgName
      machinesTotal
      hadConnection
      hadNotConnection
    }
  }
`;

export const GET_ON_OFF_AGREGATED_MACHINE_REPORT = gql`
  mutation (
    $start: String
    $finish: String
    $railways: [Int]
    $orgs: [Int]
    $classifications: [Int]
  ) {
    askrOnOffReport(
      dates: { start: $start, finish: $finish }
      group_by: {
        classifications: $classifications
        railways: $railways
        orgs: $orgs
        machines: []
      }
    ) {
      id
      drpName
      orgName
      sps
      machines
      machinesTotal
      hadConnection
      hadNotConnection
    }
  }
`;
