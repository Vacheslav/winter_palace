import gql from "graphql-tag";

export const ASKR_ON_OFF_REPORT_ALL = gql`
  mutation ($start: String, $finish: String) {
    askrOnOffReport(
      dates: { start: $start, finish: $finish }
      group_by: { totals: "true" }
    ) {
      machines
      hadConnection
      hadNotConnection
    }
  }
`;

export const ASKR_ON_OFF_REPORT_BYTYPES = gql`
  mutation ($start: String, $finish: String) {
    askrOnOffReport(
      dates: { start: $start, finish: $finish }
      group_by: { classifications: [] }
    ) {
      classificationParentId
      classificationParentName
      machines
      hadConnection
      hadNotConnection
    }
  }
`;

export * from "./agregatedOnOff";
