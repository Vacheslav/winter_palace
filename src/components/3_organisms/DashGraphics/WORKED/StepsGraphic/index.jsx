/* eslint-disable import/no-anonymous-default-export */
import React from "react";
import {
  Wrapper,
  Body,
  ControllBlock,
  TitleDesktop,
  TitleContent,
  TitleMobile,
} from "./styles";

import {
  ErrorBoundry,
  DownloadButton,
  BackButton,
  Description,
  FullScreenButton,
  MobileBreadButtons,
} from "../../TemplateComponents";
import SmartGraphic from "./components";

import { initState } from "./HOCs";

const Graphic = (props) => {
  const { total, setMenuOpened, menuOpened, handleClick, step } = props;

  const title = "В работе";
  const containerId = "WORKED_TYPES";

  return (
    <Wrapper
      fullScreeEnabled={props?.fullScreeEnabled}
      isFullScreen={props?.isFullScreen}
      id={containerId}
    >
      <TitleDesktop>
        <TitleContent>
          <div>
            <h2>
              {title} ({total})
            </h2>
            <Description {...props} />
          </div>
        </TitleContent>
        <ControllBlock>
          <DownloadButton
            containerId={containerId}
            setMenuOpened={setMenuOpened}
            menuOpened={menuOpened}
          />{" "}
          <FullScreenButton className="fullscreen" graphicIdx={1} {...props} />
          {step > 0 && <BackButton onClick={() => handleClick(step - 1)} />}
        </ControllBlock>
      </TitleDesktop>
      <TitleMobile>
        <TitleContent>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
            }}
          >
            <h2>
              {title} ({total})
            </h2>
            <Description {...props} pathList={null} />
          </div>
        </TitleContent>
        <div
          style={{
            display: "flex",
            width: "100%",
            justifyContent: "space-between",
          }}
        >
          <MobileBreadButtons {...props} />
          <ControllBlock>
            <DownloadButton
              containerId="KRSPS_TYPES"
              setMenuOpened={setMenuOpened}
              menuOpened={menuOpened}
            />{" "}
            <FullScreenButton
              className="fullscreen"
              graphicIdx={2}
              {...props}
            />
            {step > 0 && <BackButton onClick={() => handleClick(step - 1)} />}
          </ControllBlock>
        </div>
      </TitleMobile>
      <Body>
        <ErrorBoundry>
          <SmartGraphic containerId={containerId} title={title} {...props} />
        </ErrorBoundry>
      </Body>
    </Wrapper>
  );
};

export default initState(Graphic);
