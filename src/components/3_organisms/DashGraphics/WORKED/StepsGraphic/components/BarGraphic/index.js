import { FrontendLoader } from "@/wrappers/index";
import { useEffect } from "react";
import { Wrapper, Container } from "./styles";
import dynamic from "next/dynamic";
const Chart = dynamic(() => import("react-apexcharts"), { ssr: false });

/**
 * SmartSearcher - шаблонный компонент для графиков
 *
 * @category organisms/BarGraphic/TemplateDashGraphic
 *
 * @param {Object} props - пропсы
 * @param {Array} props.data - массив данных для отображения графика
 * @param {Function} props.changeStep - функция перехода навый график
 * @param {Function} props.setData - функция которая устанавливает значения полей для графика
 * @param {number} props.newStep - значение нового шага
 * @param {string} props.fieldName - названия ключего поля графика. например railway
 * @param {number} props.left - отступ слева (чтобы график не обрезался)
 *
 * @example  <BarGraphic
      setData={setRailways}
      changeStep={changeStep}
      data={data.map(
        ({
          hadConnection,
          hadNotConnection,
          machines,
          railwayName,
          ...props
        }) => ({
          machine: `${railwayName} (${machines})`,
          hadConnection,
          hadNotConnection,
          railwayName,
          ...props,
        })
      )}
      newStep={3}
      fieldName="railway"
    />
 *
 */
export default ({
  data = [],
  changeStep,
  setData,
  newStep,
  fieldName,
  subtitle = "",
  total,
  isFullScreen,
  title,
}) => {
  useEffect(() => {
    setData && setData(null);
  }, []);

  const onClick = (graphData) => {
    if (changeStep && setData && fieldName) {
      const newProp = {
        name: graphData?.[`${fieldName}Name`],
        id: graphData?.[`${fieldName}Id`],
      };
      setData(newProp);
      changeStep(newStep, newProp);
    }
  };

  const newHeight = 340 + (data.length > 6 ? (data.length - 6) * 40 : 0);

  const machinesNotWorked = data.map(
    ({ machinesNotWorked }) => machinesNotWorked
  );

  const noData = data.map(({ noData }) => noData);

  const machinesWorked = data.map(({ machinesWorked }) => machinesWorked);

  const totalMachinesNotWorked =
    machinesNotWorked && machinesNotWorked?.reduce((a, b) => a + b, 0);

  const totalNoData = noData && noData?.reduce((a, b) => a + b, 0);

  const totalMachinesWorked =
    machinesWorked && machinesWorked?.reduce((a, b) => a + b, 0);

  const scrollWidth = Math.max(
    document.body.scrollWidth,
    document.documentElement.scrollWidth,
    document.body.offsetWidth,
    document.documentElement.offsetWidth,
    document.body.clientWidth,
    document.documentElement.clientWidth
  );

  return (
    <Wrapper
      isFullScreen={isFullScreen}
      scrollWidth={scrollWidth}
      newHeight={scrollWidth < 550 ? 700 : newHeight}
      className="graphic-overflow-container"
    >
      <Container isFullScreen={isFullScreen}>
        {typeof window !== "undefined" && (
          <Chart
            height={
              scrollWidth < 550
                ? "540px"
                : isFullScreen
                ? "700px"
                : newHeight
                ? `${newHeight}px`
                : "400px"
            }
            width={
              scrollWidth < 550 ? "320px" : isFullScreen ? `600px` : "490px"
            }
            options={{
              title: {
                text: `${title} (${total})`,
              },
              subtitle: {
                text: subtitle,
                style: {
                  color: "#959595",
                },
              },
              chart: {
                zoom: {
                  enabled: true,
                  type: "x",
                  resetIcon: {
                    offsetX: -10,
                    offsetY: 0,
                    fillColor: "#fff",
                    strokeColor: "#37474F",
                  },
                  selection: {
                    background: "#90CAF9",
                    border: "#0D47A1",
                  },
                },
                toolbar: {
                  show: true,
                  toolbar: {
                    show: true,
                    tools: {
                      download: true,
                      selection: true,
                      zoom: true,
                      zoomin: true,
                      zoomout: true,
                      pan: true,
                      reset: true,
                      customIcons: [],
                    },
                  },
                },
                events: {
                  dataPointSelection(_, __, config) {
                    data[config.dataPointIndex] &&
                      onClick(data[config.dataPointIndex]);
                  },
                },
                width: "100%",
                height: newHeight,
                stacked: true,
              },
              plotOptions: {
                bar: {
                  barHeight: "60px",
                  columnWidth: "60px",
                  borderRadius: 2,
                  horizontal: true,
                },
              },
              legend: {
                position: "top",
                show: true,
                horizontalAlign: "left",
              },
              tooltip: {
                enabled: false,
              },
              colors: ["#86d483", "#ff7272", "#a5a5a5"],
              stroke: {
                width: 1,
                colors: ["#fff"],
              },
              xaxis: {
                categories: data.map(({ machine }) => machine),
                labels: {
                  formatter: (v) => {
                    return +v.toFixed(1) % 1 === 0 ? Math.round(v) : null;
                  },
                },
              },
            }}
            series={[
              {
                name: `Работали (${totalMachinesWorked})`,
                data: machinesWorked,
              },
              {
                name: `Не работали (${totalMachinesNotWorked})`,
                data: machinesNotWorked,
              },
              {
                name: `Н/Д (${totalNoData})`,
                data: noData,
              },
            ]}
            type="bar"
          />
        )}
      </Container>
    </Wrapper>
  );
};
