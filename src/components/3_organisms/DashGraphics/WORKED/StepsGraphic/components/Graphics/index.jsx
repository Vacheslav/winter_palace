import { cond, T, equals } from "ramda";

import FullBar from "./FullBar";
import DrpBar from "./DrpBar";
import OrgsBar from "./OrgsBar";
import SPSBar from "./SPSBar";
import LoadingWrapper from "@/wrappers/LoadingWrapper";

export default ({ graphType, loading, ...props }) => {
  return (
    <LoadingWrapper isLoading={loading || !props.data}>
      {cond([
        [equals("FullBar"), () => <FullBar {...props} />],
        [equals("DrpBar"), () => <DrpBar {...props} />],
        [equals("OrgsBar"), () => <OrgsBar {...props} />],
        [equals("SPSBar"), () => <SPSBar {...props} />],
        [T, () => null],
      ])(graphType)}
    </LoadingWrapper>
  );
};
