import BarGraphic from "../BarGraphic";

export default ({ data = [], changeStep, setRailways }) => {
  return (
    <BarGraphic
      setData={setRailways}
      changeStep={changeStep}
      data={data.map(({ machines, railwayName, ...props }) => ({
        machine: `${railwayName || ""} (${machines})`,
        railwayName,
        machines,
        ...props,
      }))}
      newStep={2}
      fieldName="railway"
    />
  );
};
