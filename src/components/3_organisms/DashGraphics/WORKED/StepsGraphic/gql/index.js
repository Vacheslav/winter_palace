import gql from "graphql-tag";

export const ASKR_ON_OFF_REPORT_BYTYPES = gql`
  mutation ($start: String, $finish: String, $filterCase: [Int]) {
    askrOnOffReport(
      dates: { start: $start, finish: $finish }
      group_by: { classifications: [] }
      filters: { filterBy: "machineId", filterCase: $filterCase }
    ) {
      classificationParentId
      classificationParentName
      machines
      hadConnection
      hadNotConnection
    }
  }
`;

export * from "./agregatedOnOff";
