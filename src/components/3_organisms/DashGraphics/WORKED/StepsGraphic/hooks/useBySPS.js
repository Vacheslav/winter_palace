import { useState } from "react";
import { request } from "@/utils/index";

export default ({ setData }) => {
  const [loadingBySPS, setLoadingBySPS] = useState(false);

  const [error, setError] = useState(null);

  const getDataBySPS = (data) => {
    setLoadingBySPS(true);
    request({
      schema: "GET_WORK_REPORT_DIAGRAM_DATA_BYSPS",
      token: localStorage.getItem("auth-token"),
      data,
    })
      .then((data) => {
        setLoadingBySPS(false);
        data?.getWorkReportDiagramData &&
          setData(data.getWorkReportDiagramData);
      })
      .catch((e) => {
        console.log(e);
        setError(e);
        setLoadingBySPS(false);
      });
  };
  return { getDataBySPS, loadingBySPS, error };
};
