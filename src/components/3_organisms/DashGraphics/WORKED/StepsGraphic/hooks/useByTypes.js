import { useState } from "react";
import { request } from "@/utils/index";

export default ({ setData, dtStart, dtFinish, machinesidx }) => {
  const [loadingByTypes, setLoadingByTypes] = useState(false);

  const [error, setError] = useState(null);

  const getDataByTypes = () => {
    setLoadingByTypes(true);
    request({
      schema: "GET_WORK_REPORT_DIAGRAM_DATA_BYTYPES",
      token: localStorage.getItem("auth-token"),
      data: {
        start: dtStart,
        finish: dtFinish,
        filterCase: machinesidx,
      },
    })
      .then((data) => {
        setLoadingByTypes(false);
        if (data?.getWorkReportDiagramData[0]) {
          setData(data?.getWorkReportDiagramData);
        }
      })
      .catch((e) => {
        console.log(e);
        setError(e);
        setLoadingByTypes(false);
      });
  };

  return { getDataByTypes, loadingByTypes, error };
};
