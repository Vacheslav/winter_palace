import { useState } from "react";
import { request } from "@/utils/index";

export default ({ setData }) => {
  const [loadingByOrgs, setLoadingByOrgs] = useState(false);

  const [error, setError] = useState(null);

  const getDataByOrgs = (data) => {
    setLoadingByOrgs(true);
    request({
      schema: "GET_WORK_REPORT_DIAGRAM_DATA_BYORG",
      token: localStorage.getItem("auth-token"),
      data,
    })
      .then((data) => {
        data?.getWorkReportDiagramData &&
          setData(data.getWorkReportDiagramData);
        setLoadingByOrgs(false);
      })
      .catch((e) => {
        console.log(e);
        setError(e);
        setLoadingByOrgs(false);
      });
  };
  return { getDataByOrgs, loadingByOrgs, error };
};
