import { useState } from "react";
import { request } from "@/utils/index";

export default ({ setData }) => {
  const [loadingByDrp, setLoadingByDrp] = useState(false);

  const [error, setError] = useState(null);

  const getDataByDrp = (data) => {
    setLoadingByDrp(true);
    request({
      schema: "GET_WORK_REPORT_DIAGRAM_DATA_BYDRP",
      token: localStorage.getItem("auth-token"),
      data,
    })
      .then((data) => {
        data?.getWorkReportDiagramData &&
          setData(data.getWorkReportDiagramData);
        setLoadingByDrp(false);
      })
      .catch((e) => {
        console.log(e);
        setError(e);
        setLoadingByDrp(false);
      });
  };

  return { getDataByDrp, loadingByDrp, error };
};
