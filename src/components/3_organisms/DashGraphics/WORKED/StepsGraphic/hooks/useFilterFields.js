import { useEffect, useState } from "react";
import { curry, cond, T, path, isEmpty, pipe, map, is, uniq } from "ramda";
import { GET_AVAILABLE_MACHINES } from "../gql";
import { useQuery } from "@apollo/react-hooks";
import { fullDrpList } from "../funcs/unit";

const useFilterFields = () => {
  const [c, cc] = useState(0); // каунтер для рендера страницы после обновления стора

  const {
    loading: availableMachinesLoading,
    error: availableMachinesError,
    data: availableMachinesGQL,
  } = useQuery(GET_AVAILABLE_MACHINES);

  const availableMachines = availableMachinesGQL
    ? availableMachinesGQL?.availableMachines
    : [];

  let lStorage = JSON.parse(localStorage.getItem("sRStorage")) || {};

  const filterByField = curry((field, fieldName, list) => {
    let answer = [];
    try {
      answer =
        list &&
        list.filter((el) =>
          cond([
            [isEmpty, () => el],
            [T, () => field.includes(el[fieldName])],
          ])(field)
        );
    } catch (error) {
      return answer;
    }
    return answer;
  });

  const end = new Date(path(["end"], lStorage) || new Date());
  const drpName = path(["drpName"], lStorage) || [];
  const orgName = path(["orgName"], lStorage) || [];
  const typeName = path(["typeName"], lStorage) || [];
  const zavNomer = path(["zavNomer"], lStorage) || [];
  const selectedRowKeys = path(["selectedRowKeys"], lStorage) || [];

  const filtredOrgList = filterByField(drpName, "drpName", availableMachines);

  const machinesidx = pipe(
    filterByField(orgName, "orgName"),
    filterByField(typeName, "typeName"),
    filterByField(zavNomer, "zavNomer"),
    map((el) => el?.machineId)
  )(filtredOrgList);

  useEffect(() => {
    if (!isEmpty(availableMachines)) {
      cond([
        [
          () => isEmpty(machinesidx) && !isEmpty(zavNomer),
          () => {
            setTimeout(() => {
              cLStorage("zavNomer")([]);
            }, 100);
          },
        ],
        [
          () => isEmpty(machinesidx) && !isEmpty(typeName),
          () => {
            setTimeout(() => {
              cLStorage("typeName")([]);
            }, 100);
          },
        ],
        [
          () => isEmpty(machinesidx) && !isEmpty(orgName),
          () => {
            setTimeout(() => {
              cLStorage("orgName")([]);
            }, 100);
          },
        ],
        [
          () => isEmpty(machinesidx) && !isEmpty(drpName),
          () => {
            setTimeout(() => {
              cLStorage("drpName")([]);
            }, 100);
          },
        ],
      ])();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [drpName, orgName, typeName, zavNomer]);

  const cLStorage = curry((name, val) => {
    localStorage.setItem(
      "sRStorage",
      JSON.stringify({
        ...lStorage,
        [name]: val,
      })
    );
    //перендерить компонент
    cc(c + 1);
  });

  const reduceField = (field, list) =>
    list && uniq(list.map((el) => el[field]));

  const filtredTypeList = pipe(
    () => filtredOrgList,
    filterByField(orgName, "orgName")
  )();
  const filtredZavList = pipe(
    () => filtredOrgList,
    filterByField(orgName, "orgName"),
    filterByField(typeName, "typeName")
  )();

  const userDrpList = reduceField("drpName", availableMachines);
  const drpList = fullDrpList.filter(
    (el) => is(Array, userDrpList) && userDrpList.includes(el)
  );

  const orgList = reduceField("orgName", filtredOrgList).sort();

  const typeList = reduceField("typeName", filtredTypeList).sort();
  const zavList = reduceField("zavNomer", filtredZavList).sort(
    (a, b) => +a - +b
  );

  return {
    end,
    drpName,
    orgName,
    typeName,
    zavNomer,
    selectedRowKeys,
    machinesidx,
    availableMachines,
    drpList,
    orgList,
    typeList,
    zavList,
    cLStorage,
    availableMachinesLoading,
    availableMachinesError,
    lStorage,
  };
};

export default useFilterFields;
