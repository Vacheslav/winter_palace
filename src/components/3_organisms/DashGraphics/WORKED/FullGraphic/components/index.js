import FullPie from "./FullPie";

export default ({ graphType, loading, ...props }) => {
  if (loading) return null;
  if (!props.data) return null;

  return <FullPie {...props} />;
};
