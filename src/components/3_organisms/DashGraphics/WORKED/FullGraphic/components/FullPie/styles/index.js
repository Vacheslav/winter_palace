import styled from "styled-components";

export const Wrapper = styled.div`
  position: relative;
  z-index: 1;

  width: ${({ isFullScreen }) => (isFullScreen ? "30vw" : "350px")};
  height: ${({ isFullScreen }) => (isFullScreen ? "30vw" : "350px")};
  margin-left: 20px;

  * .apexcharts-title-text {
    opacity: 0 !important;
  }

  * .apexcharts-subtitle-text {
    opacity: 0 !important;
  }

  & .apexcharts-legend {
    & * {
      display: none !important;
    }
    display: none !important;
  }

  * .apexcharts-menu-icon {
    position: absolute;
    transform: scale(0);
    z-index: 1;
    opacity: 0;
  }

  * .apexcharts-menu {
    position: absolute;
    top: 40px !important;
    right: -80px !important;
    width: 130px;
    padding: 15px 10px 0 10px !important;
    @media only screen and (max-width: 550px) {
      right: -30% !important;
      top: 25px !important;
      height: 100px !important;
    }
  }

  & div {
    transform: translate(0, -10px);
  }

  @media only screen and (max-width: 550px) {
    margin: 0;
    height: 340px;
  }
`;
