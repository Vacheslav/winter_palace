import styled from "styled-components";
import { cond, T, and } from "ramda";

export const Wrapper = styled.div`
  ${({ fullScreeEnabled, isFullScreen }) =>
    cond([
      [and(!isFullScreen), () => "display: none;"],
      [T, () => "display: flex;"],
    ])(fullScreeEnabled)}

  flex-direction: column;
  margin-right: 0;
  align-items: center;
  background-color: white;
  padding: 20px 20px 20px 0;
  position: relative;
  overflow-x: hidden;
  overflow-y: hidden;
  background-color: white;
  width: ${({ isFullScreen }) => (isFullScreen ? "100%" : "520px")};
  height: ${({ isFullScreen }) => (isFullScreen ? "100%" : "410px")};
  box-shadow: 4px 4px 8px 0px rgba(34, 60, 80, 0.2);
  @media screen and (max-width: 550px) {
    width: 100%;
    height: 560px;
    & .fullscreen {
      display: none;
    }
  }
`;

export const Title = styled.div`
  display: flex;
  color: black;
  column-gap: 20px;
  width: 100%;
  padding-left: 20px;
  justify-content: space-between;
  align-items: center;
  position: relative;
  & * h2 {
    font-size: 16px;
    font-weight: 600;
    color: #242320;
    @media screen and (max-width: 550px) {
      font-size: 14px;
      max-width: 250px;
    }
  }
  & * p {
    font-size: 11px;
    color: gray;
  }
`;

export const TitleContent = styled.div`
  display: flex;
  align-items: center;
  column-gap: 20px;
`;

export const Body = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  overflow: hidden;
  justify-content: space-between;
  & .count-icons {
    transform: scale(0.9);
    row-gap: 10px;
    & .count-icon {
      transform: scale(0.9);
    }
  }
  @media screen and (max-width: 550px) {
    width: 100%;
    flex-direction: column;
    justify-content: center;
  }
`;

export const GrapMesures = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  column-gap: 15px;
`;
