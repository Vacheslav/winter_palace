/* eslint-disable import/no-anonymous-default-export */
import React, { useState, useEffect } from "react";
import { Wrapper, Title, Body, TitleContent, GrapMesures } from "./styles";
import {
  CounIcons,
  DownloadButton,
  Description,
  FullScreenButton,
} from "../../TemplateComponents";
import SmartGraphic from "./components";
import { useAll } from "./hooks";
import { getDescription } from "@/organisms/DashGraphics/funcs";
import { isArr } from "@/utils/index";

const FullGraphic = (props) => {
  const { dtStart: start, dtFinish: finish, machinesidx: machIdx } = props;

  const [legend, setLegend] = useState([]);
  const [data, setData] = useState(null);
  const [menuOpened, setMenuOpened] = useState(false);
  const [machinesidx] = useState(machIdx);
  const [dtStart] = useState(start);
  const [dtFinish] = useState(finish);

  const { getDataSummary, loadingSummary } = useAll({
    setData,
    setLegend,
    dtStart,
    dtFinish,
    machinesidx,
  });

  const loading = loadingSummary;

  useEffect(() => {
    getDataSummary();
  }, []);

  const total = isArr(data) ? data[0]?.machines : 0;

  return (
    <Wrapper
      fullScreeEnabled={props?.fullScreeEnabled}
      isFullScreen={props?.isFullScreen}
      id="fullWORED"
    >
      <Title>
        <TitleContent>
          <div>
            <h2>В работе ({total})</h2>
            <Description dtStart={dtStart} dtFinish={dtFinish} />
          </div>
        </TitleContent>
        <GrapMesures>
          <DownloadButton
            containerId="fullWORED"
            setMenuOpened={setMenuOpened}
            menuOpened={menuOpened}
            {...props}
          />
          <FullScreenButton
            className={"fullscreen"}
            graphicIdx={2}
            {...props}
          />
        </GrapMesures>
      </Title>
      <Body>
        <div
          style={{
            flexGrow: 1,
            display: "flex",
            justifyContent: "center",
          }}
        >
          <SmartGraphic
            loading={loading}
            data={data}
            graphType={"FullPie"}
            subtitle={getDescription({ dtStart, dtFinish })}
            total={total}
            {...props}
          />
        </div>
        <CounIcons
          style={{
            marginBottom: 40,
          }}
          legend={legend}
        />
      </Body>
    </Wrapper>
  );
};

export default FullGraphic;
