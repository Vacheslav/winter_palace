import { request } from "@/utils/index";
import { useState } from "react";

export default ({ setData, setLegend, dtStart, dtFinish, machinesidx }) => {
  const [loadingSummary, setLoadingSummary] = useState(false);
  const [error, setError] = useState(null);

  const getDataSummary = () => {
    machinesidx &&
      request({
        schema: "ASKR_ON_OFF_REPORT_MACGINES_WORKED_ALL",
        token: localStorage.getItem("auth-token"),
        data: {
          start: dtStart,
          finish: dtFinish,
          machines: machinesidx,
        },
      })
        .then((data) => {
          console.log(data);
          if (data?.getWorkReportDiagramData[0]) {
            setData(data?.getWorkReportDiagramData);
            const { machinesNotWorked, machinesWorked, noData } =
              data?.getWorkReportDiagramData[0];
            setLegend([
              {
                color: "darkgreen",
                count: machinesWorked,
                title: "Выходили на связь",
              },
              {
                color: "darkred",
                count: machinesNotWorked,
                title: "Не выходили на связь",
              },
              {
                color: "#4e4e4a",
                count: noData,
                title: "Не выходили на связь",
              },
            ]);
          }
          setLoadingSummary(false);
        })
        .catch((e) => {
          console.log(e);
          setError(e);
          setLoadingSummary(false);
        });
  };

  return { getDataSummary, loadingSummary, error };
};
