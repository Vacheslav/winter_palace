import FullGraphic from "./FullGraphic";
import StepsGraphic from "./StepsGraphic";

const WORKED = ({ isFull = true, ...restProps }) => {
  if (isFull) return <FullGraphic {...restProps} />;

  return <StepsGraphic {...restProps} />;
};

export default WORKED;
