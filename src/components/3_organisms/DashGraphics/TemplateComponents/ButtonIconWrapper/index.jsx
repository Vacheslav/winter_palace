import { Wrapper } from "./styles";

export default ({ iconClassName, ...restProps }) => {
  return (
    <Wrapper {...restProps}>
      <i className={iconClassName} />
    </Wrapper>
  );
};
