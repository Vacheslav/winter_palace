import ButtonIconWrapper from "../ButtonIconWrapper";

export default ({
  isFullScreen,
  setFullGraphicIdx,
  graphicIdx,
  ...restProps
}) => {
  if (!graphicIdx || !setFullGraphicIdx) return null;
  const handleClick = () => {
    isFullScreen ? setFullGraphicIdx(null) : setFullGraphicIdx(graphicIdx);
  };

  return (
    <ButtonIconWrapper
      onClick={handleClick}
      iconClassName={
        isFullScreen ? "fal fa-compress-arrows-alt" : "fal fa-expand-arrows"
      }
      {...restProps}
    />
  );
};
