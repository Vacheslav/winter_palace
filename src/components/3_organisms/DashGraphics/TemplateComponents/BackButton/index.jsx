/* eslint-disable import/no-anonymous-default-export */
import React from "react";
import { BackIcon } from "../icons";
import { Wrapper } from "./styles";

export default (props) => {
  return (
    <Wrapper {...props}>
      <BackIcon
        styles={{
          color: "#a3a3a3",
          marginRight: 5,
          marginBottom: 2,
        }}
        size={7}
      />
    </Wrapper>
  );
};
