import React from "react";
import { cond, equals, T } from "ramda";
import { BreadBit, Wrapper } from "./styles";

const Description = ({
  dtStart,
  dtFinish,
  pathList = null,
  setStep = () => {},
}) => {
  const days =
    (new Date(dtFinish).getTime() - new Date(dtStart).getTime()) /
      1000 /
      60 /
      60 /
      24 +
    1;

  const dateText = cond([
    [equals(1), () => "За последние технические сутки"],
    [equals(7), () => "За последнюю неделю"],
    [equals(30), () => "За последний месяц"],
    [T, () => null],
  ])(days);

  const text = `${dateText}`;

  return (
    <Wrapper
      style={{
        marginBottom: 0,
      }}
    >
      <span>{text}</span>
      {pathList && (
        <span
          style={{
            float: "left",
          }}
        >
          , выбрано:{" "}
        </span>
      )}
      {pathList && pathList?.length && (
        <span
          style={{
            display: "flex",
            columnGap: 5,
            paddingLeft: 5,
            float: "left",
          }}
        >
          {pathList?.map(({ getData, name }, idx) => (
            <BreadBit
              onClick={() => {
                getData();
                setStep(idx);
              }}
            >
              {name}
            </BreadBit>
          ))}
        </span>
      )}
    </Wrapper>
  );
};

export const getDescription = ({ dtStart, dtFinish }) => {
  const days =
    (new Date(dtFinish).getTime() - new Date(dtStart).getTime()) /
      1000 /
      60 /
      60 /
      24 +
    1;

  return cond([
    [equals(1), () => "За последние технические сутки"],
    [equals(7), () => "За последнюю неделю"],
    [equals(30), () => "За последний месяц"],
    [T, () => null],
  ])(days);
};

export default Description;
