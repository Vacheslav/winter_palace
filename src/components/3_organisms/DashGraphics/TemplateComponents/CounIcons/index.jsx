import React from "react";
import { Wrapper } from "./styles";
import CountIcon from "../CountIcon";
import { is } from "ramda";
import { randKey } from "@/utils/index";

export default ({ legend, ...restProps }) => {
  if (!is(Array, legend)) return null;
  return (
    <Wrapper {...restProps} className="count-icons">
      {legend &&
        legend?.map((props) => (
          <CountIcon className="count-icon" key={randKey()} {...props} />
        ))}
    </Wrapper>
  );
};
