import { Wrapper } from "./styles";
import ButtonIconWrapper from "../ButtonIconWrapper";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";
import { cond, T as TRUE } from "ramda";
import { randKey } from "src/utils";

export default ({ containerId = "", isFullScreen = false }) => {
  if (isFullScreen) return null;

  const print = (scale = 1) => {
    const width = document.body.offsetWidth;
    const diff = (1903 - document.body.offsetWidth) / 90;
    const diff2 = (1400 - document.body.offsetWidth) / 120;

    const x = cond([
      [() => width > 1400, () => 18 - diff],
      [() => width > 1000, () => 12 - diff2],
      [TRUE, () => 14 - diff2],
    ])(width);

    const y = cond([
      [() => width > 1400, () => 9],
      [TRUE, () => 16],
    ])(width);

    const input = document.getElementById(containerId);

    html2canvas(input, {
      windowWidth: 1920,
      windowHeight: 1080,
      width: 550,
      height: 400,
    }).then((canvas) => {
      var a = document.createElement("a");
      a.href = canvas
        .toDataURL("image/jpeg")
        .replace("image/jpeg", "image/octet-stream");
      a.download = `${randKey()}.jpg`;
      a.click();
    });
  };

  // const print = () => {
  //   const container = document.getElementById(containerId);
  //   if (container) {
  //     const el = container.getElementsByClassName("apexcharts-menu-icon")[0];
  //     const graphicOverflowContainer = container.getElementsByClassName(
  //       "graphic-overflow-container"
  //     )[0];
  //     graphicOverflowContainer &&
  //       graphicOverflowContainer.scrollTo({
  //         top: 0,
  //         left: 0,
  //         behavior: "smooth",
  //       });
  //     toggleMenu();
  //     el && el.click();
  //   }
  // };

  return (
    <div>
      <ButtonIconWrapper onClick={print} iconClassName="fal fa-download" />
    </div>
  );
};
