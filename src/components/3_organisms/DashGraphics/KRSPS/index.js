import FullGraphic from "./FullGraphic";
import MachineTypesGraphic from "./MachineTypesGraphic";

export default ({ isFull = true, ...restProps }) => {
  if (isFull) return <FullGraphic {...restProps} />;

  return <MachineTypesGraphic {...restProps} />;
};
