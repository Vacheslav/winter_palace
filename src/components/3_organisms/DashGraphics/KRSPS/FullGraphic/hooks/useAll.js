import { request, addHours } from "@/utils/index";
import { useState } from "react";

export default ({ setData, setLegend, dtStart, dtFinish, machinesidx }) => {
  const [loadingSummary, setLoadingSummary] = useState(false);
  const [error, setError] = useState(null);

  const getDataSummary = () => {
    machinesidx &&
      request({
        schema: "ASKR_ON_OFF_REPORT_ALL",
        token: localStorage.getItem("auth-token"),
        data: {
          start: dtStart,
          finish: dtFinish,
          machines: machinesidx,
        },
      })
        .then((data) => {
          console.log(data);
          if (data?.askrOnOffReport[0]) {
            setData(data?.askrOnOffReport);
            const { hadConnection, hadNotConnection } =
              data?.askrOnOffReport[0];
            setLegend([
              {
                color: "darkgreen",
                count: hadConnection,
                title: "Выходили на связь",
              },
              {
                color: "darkred",
                count: hadNotConnection,
                title: "Не выходили на связь",
              },
            ]);
          }
          setLoadingSummary(false);
        })
        .catch((e) => {
          console.log(22);
          console.log(e);
          setError(e);
          setLoadingSummary(false);
        });
  };

  return { getDataSummary, loadingSummary, error };
};
