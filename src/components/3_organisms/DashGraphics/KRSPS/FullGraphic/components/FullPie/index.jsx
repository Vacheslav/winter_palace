import React from "react";
import dynamic from "next/dynamic";
const Chart = dynamic(() => import("react-apexcharts"), { ssr: false });
import { Wrapper } from "./styles";

export default ({ data, subtitle, total, isFullScreen }) => {
  const { hadConnection, hadNotConnection } = data[0];

  const series = [hadNotConnection, hadConnection];

  return (
    <Wrapper isFullScreen={isFullScreen}>
      {typeof window !== "undefined" && (
        <Chart
          options={{
            title: {
              text: `Количество КРСПС, выходивших на связь (${total || ""})`,
            },
            subtitle: {
              text: subtitle,
              style: {
                color: "#959595",
              },
            },
            chart: {
              toolbar: {
                show: true,
                tools: {
                  download: true,
                  selection: true,
                  zoom: true,
                  zoomin: true,
                  zoomout: true,
                  pan: true,
                  reset: true,
                  customIcons: [],
                },
              },
            },
            legend: {
              show: false,
            },
            tooltip: {
              enabled: false,
            },
            dataLabels: {
              enabled: true,
              enabledOnSeries: [1, 2],
              formatter: function (_, { seriesIndex }) {
                return series[seriesIndex];
              },
            },
            colors: ["#ff7272", "#86d483"],
            labels: ["Не выходили на связь", "Выходили на связь"],
            series,
            plotOptions: {
              pie: {
                dataLabels: {},
                donut: {
                  labels: {
                    show: true,
                    total: {
                      show: true,
                    },
                    name: {
                      show: true,
                      color: "gray",
                      formatter: function (val) {
                        return "Всего";
                      },
                    },
                    value: {
                      show: true,
                      formatter: function () {
                        return hadNotConnection + hadConnection;
                      },
                    },
                  },
                },
              },
            },
          }}
          series={series}
          type="donut"
          width="100%"
          height="100%"
        />
      )}
    </Wrapper>
  );
};
