import { cond, T, equals } from "ramda";
import FullPie from "./FullPie";

export default ({ graphType, loading, ...props }) => {
  if (loading) return null;
  if (!props.data) return null;

  return cond([
    [equals("FullPie"), () => <FullPie {...props} />],
    [T, () => null],
  ])(graphType);
};
