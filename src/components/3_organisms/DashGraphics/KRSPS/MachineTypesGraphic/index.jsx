/* eslint-disable import/no-anonymous-default-export */
import React from "react";
import {
  Wrapper,
  Body,
  ControllBlock,
  TitleDesktop,
  TitleContent,
  TitleMobile,
} from "./styles";

import {
  ErrorBoundry,
  DownloadButton,
  BackButton,
  Description,
  FullScreenButton,
  MobileBreadButtons,
} from "../../TemplateComponents";
import SmartGraphic from "./components";

import { initState } from "./HOCs";

const Graphic = (props) => {
  const { total, setMenuOpened, menuOpened, handleClick, step } = props;

  return (
    <Wrapper
      fullScreeEnabled={props?.fullScreeEnabled}
      isFullScreen={props?.isFullScreen}
      id="KRSPS_TYPES"
    >
      <TitleDesktop>
        <TitleContent>
          <div>
            <h2>Количество КРСПС, выходивших на связь ({total})</h2>
            <Description {...props} />
          </div>
        </TitleContent>
        <ControllBlock>
          <DownloadButton
            containerId="KRSPS_TYPES"
            setMenuOpened={setMenuOpened}
            menuOpened={menuOpened}
          />{" "}
          <FullScreenButton className="fullscreen" graphicIdx={1} {...props} />
          {step > 0 && <BackButton onClick={() => handleClick(step - 1)} />}
        </ControllBlock>
      </TitleDesktop>
      <TitleMobile>
        <TitleContent>
          <div
            style={{
              display: "flex",
            }}
          >
            <h2>Количество КРСПС, выходивших на связь ({total})</h2>
            <Description {...props} pathList={null} />
          </div>
        </TitleContent>
        <div
          style={{
            display: "flex",
            width: "100%",
            justifyContent: "space-between",
          }}
        >
          <MobileBreadButtons {...props} />
          <ControllBlock>
            <DownloadButton
              containerId="KRSPS_TYPES"
              setMenuOpened={setMenuOpened}
              menuOpened={menuOpened}
            />{" "}
            <FullScreenButton
              className="fullscreen"
              graphicIdx={1}
              {...props}
            />
            {step > 0 && <BackButton onClick={() => handleClick(step - 1)} />}
          </ControllBlock>
        </div>
      </TitleMobile>
      <Body>
        <ErrorBoundry>
          <SmartGraphic {...props} />
        </ErrorBoundry>
      </Body>
    </Wrapper>
  );
};

export default initState(Graphic);
