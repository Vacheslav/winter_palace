import BarGraphic from "../BarGraphic";

export default ({ data = [], changeStep, setOrgs, ...restProps }) => {
  return (
    <BarGraphic
      setData={setOrgs}
      changeStep={changeStep}
      data={data.map(({ machines, orgName, ...props }) => ({
        machine: `${orgName || ""} (${machines})`,
        orgName,
        ...props,
      }))}
      newStep={3}
      fieldName="org"
      {...restProps}
    />
  );
};
