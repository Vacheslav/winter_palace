import { FrontendLoader } from "@/wrappers/index";
import { useEffect } from "react";
import Chart from "react-apexcharts";
import { Wrapper } from "./styles";

/**
 * SmartSearcher - шаблонный компонент для графиков
 *
 * @category organisms/BarGraphic/TemplateDashGraphic
 *
 * @param {Object} props - пропсы
 * @param {Array} props.data - массив данных для отображения графика
 * @param {Function} props.changeStep - функция перехода навый график
 * @param {Function} props.setData - функция которая устанавливает значения полей для графика
 * @param {number} props.newStep - значение нового шага
 * @param {string} props.fieldName - названия ключего поля графика. например railway
 * @param {number} props.left - отступ слева (чтобы график не обрезался)
 *
 * @example  <BarGraphic
      setData={setRailways}
      changeStep={changeStep}
      data={data.map(
        ({
          hadConnection,
          hadNotConnection,
          machines,
          railwayName,
          ...props
        }) => ({
          machine: `${railwayName} (${machines})`,
          hadConnection,
          hadNotConnection,
          railwayName,
          ...props,
        })
      )}
      newStep={3}
      fieldName="railway"
    />
 *
 */
export default ({ data = [], changeStep, setData, newStep, fieldName }) => {
  useEffect(() => {
    setData && setData(null);
  }, []);

  const onClick = (graphData) => {
    if (changeStep && setData && fieldName) {
      const newProp = {
        name: graphData?.[`${fieldName}Name`],
        id: graphData?.[`${fieldName}Id`],
      };
      setData(newProp);
      changeStep(newStep, newProp);
    }
  };

  const newHeight = 300 + (data.length > 6 ? (data.length - 6) * 40 : 0);

  const hadConnections = data.map(({ hadConnection }) => hadConnection);
  const hadNotConnections = data.map(
    ({ hadNotConnection }) => hadNotConnection
  );

  return (
    <Wrapper
      className="graphic-overflow-container"
      style={{
        width: 400,
        height: 280,
        overflowY: "auto",
        overflowX: "none",
        position: "relative",
      }}
    >
      <div
        style={{
          width: 380,
          height: newHeight,
          position: "relative",
          padding: "0 20px 0 0",
        }}
      >
        <div
          style={{
            width: "100%",
            height: "100%",
          }}
        >
          <FrontendLoader times={1000} title="">
            <Chart
              height={newHeight}
              width={380}
              options={{
                chart: {
                  toolbar: {
                    show: true,
                    tools: {
                      download: true,
                    },
                  },
                  events: {
                    dataPointSelection(_, __, config) {
                      data[config.dataPointIndex] &&
                        onClick(data[config.dataPointIndex]);
                    },
                  },
                  width: 300,
                  height: newHeight,
                  stacked: true,
                },
                plotOptions: {
                  bar: {
                    horizontal: true,
                  },
                },
                legend: {
                  show: false,
                },
                tooltip: {
                  enabled: false,
                },
                colors: ["#86d483", "#ff7272"],
                stroke: {
                  width: 1,
                  colors: ["#fff"],
                },
                xaxis: {
                  categories: data.map(({ machine }) => machine),
                },
              }}
              series={[
                {
                  name: "Выходили на связь",
                  data: hadConnections,
                },
                {
                  name: "Не выходили на связь",
                  data: hadNotConnections,
                },
              ]}
              type="bar"
            />
          </FrontendLoader>
        </div>
      </div>
    </Wrapper>
  );
};
