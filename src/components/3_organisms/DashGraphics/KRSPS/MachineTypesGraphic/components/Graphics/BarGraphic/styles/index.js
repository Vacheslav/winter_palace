import styled from "styled-components";

export const Wrapper = styled.div`
  z-index: 1;
  & .apexcharts-legend {
    & * {
      display: none !important;
    }
    display: none !important;
  }

  * .apexcharts-menu-icon {
    opacity: 0;
  }

  * .apexcharts-menu {
    position: absolute;
    z-index: 5;
    left: -310px;
    top: -100px;
    width: 120px;
    top: 0px;
  }

  * .apexcharts-xaxis {
    display: none !important;
  }
`;
