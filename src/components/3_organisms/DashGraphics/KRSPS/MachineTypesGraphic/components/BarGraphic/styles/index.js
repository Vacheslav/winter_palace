import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  height: ${({ scrollWidth, isFullScreen }) =>
    scrollWidth < 550 ? "700px" : isFullScreen ? "100%" : "400px"};
  overflow-y: auto;
  overflow-x: hidden;
  position: relative;

  padding-top: 20px;
  justify-content: center;

  width: 100%;

  z-index: 1;
  * .apexcharts-menu-icon {
    opacity: 0;
  }

  * .apexcharts-title-text {
    opacity: 0 !important;
  }

  * .apexcharts-subtitle-text {
    opacity: 0 !important;
  }

  * .apexcharts-subtitle-text {
  }

  * .apexcharts-menu {
    position: absolute;
    opacity: 0;
    z-index: 55;
    top: 45px;
    right: 10px;
    width: 110px;
    @media screen and (max-width: 2000px) {
      ${({ isFullScreen }) => isFullScreen && "right:  -1300%"};
    }
    @media screen and (max-width: 1800px) {
      ${({ isFullScreen }) => isFullScreen && "right:  -1100%"};
    }
    @media screen and (max-width: 1600px) {
      ${({ isFullScreen }) => isFullScreen && "right:  -1000%"};
    }
    @media screen and (max-width: 1440px) {
      ${({ isFullScreen }) => isFullScreen && "right:  -800%"};
    }

    @media screen and (max-width: 1100px) {
      ${({ isFullScreen }) => isFullScreen && "right:  -300%"};
    }
    @media screen and (max-width: 800px) {
      ${({ isFullScreen }) => isFullScreen && "right:  300%"};
    }
  }

  @media screen and (max-width: 500px) {
    width: 340px;
  }
`;

export const Container = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: center;
  width: 100%;
  height: 100%;
  padding: "0 20px 0 0";
  padding-top: 10px;

  @media screen and (max-width: 550px) {
    padding-top: 40px;
  }
`;
