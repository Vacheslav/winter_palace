import React, { useState, useEffect } from "react";

import { cond, T, equals } from "ramda";
import { useByTypes, useByDRP, useByOrg, useBySPS } from "../hooks";
import {
  getDescription,
  pathText,
  pathList,
} from "@/organisms/DashGraphics/funcs";

const initState = (WrappedComponent) => {
  const ModifiedComponent = (
    ownProps // модифицированная версия компонента
  ) => {
    const {
      dtStart: start,
      dtFinish: finish,
      machinesidx: machIdx = [],
    } = ownProps;

    const [step, setStep] = useState(0);
    const [data, setData] = useState(null);
    const [machineType, setType] = useState(null);
    const [railways, setRailways] = useState(null);
    const [orgs, setOrgs] = useState(null);
    const [menuOpened, setMenuOpened] = useState(false);
    const [machinesidx] = useState(machIdx);
    const [dtStart] = useState(start);
    const [dtFinish] = useState(finish);

    useEffect(() => {
      setMenuOpened(false);
    }, [step]);

    const { getDataByTypes, loadingByTypes } = useByTypes({
      setData,
      dtStart,
      dtFinish,
      machinesidx,
    });
    const { getDataByDrp, loadingByDrp } = useByDRP({
      setData,
    });
    const { getDataByOrgs, loadingByOrgs } = useByOrg({
      setData,
    });
    const { getDataBySPS, loadingBySPS } = useBySPS({
      setData,
    });

    const loading =
      loadingByTypes || loadingByDrp || loadingByOrgs || loadingBySPS;

    useEffect(() => {
      getDataByTypes();
    }, []);

    const graphics = ["FullBar", "DrpBar", "OrgsBar", "SPSBar"];

    const getTypesData = () => {
      getDataByTypes();
      setType(null);
      setRailways(null);
      setOrgs(null);
    };

    const getDrpData = (args = {}) => {
      setRailways(null);
      setOrgs(null);
      getDataByDrp({
        classifications: [machineType?.id || args?.id],
        start: dtStart,
        finish: dtFinish,
        filterCase: machinesidx,
      });
    };

    const getOrgsData = (args = {}) => {
      setOrgs(null);
      getDataByOrgs({
        classifications: [machineType?.id],
        railways: [railways?.id || args?.id],
        start: dtStart,
        finish: dtFinish,
        filterCase: machinesidx,
      });
    };

    const getSpsData = (args = {}) => {
      getDataBySPS({
        classifications: [machineType?.id],
        railways: [railways?.id],
        orgs: [orgs?.id || args?.id],
        start: dtStart,
        finish: dtFinish,
        filterCase: machinesidx,
      });
    };

    const handleClick = (newStep, args) => {
      setStep(newStep);
      cond([
        [
          equals(0),
          () => {
            getTypesData();
          },
        ],
        [
          equals(1),
          () => {
            getDrpData(args);
          },
        ],
        [
          equals(2),
          () => {
            getOrgsData(args);
          },
        ],
        [
          equals(3),
          () => {
            getSpsData(args);
          },
        ],
        [T, () => null],
      ])(newStep);
    };

    const path = pathText({ machineType, railways, orgs });

    const total =
      data &&
      data.reduce(
        (acc, { hadConnection, hadNotConnection }) =>
          acc + hadConnection + hadNotConnection,
        0
      );

    const newProps = {
      ...ownProps,
      dtStart,
      dtFinish,
      total,
      setStep,
      getDataByTypes,
      setRailways,
      setType,
      setOrgs,
      pathList: pathList({
        machineType,
        orgs,
        railways,
        getDrpData,
        getOrgsData,
        getTypesData,
        railways,
      }),
      setMenuOpened,
      menuOpened,
      handleClick,
      step,
      loading,
      data,
      graphType: graphics[step] || "",
      changeStep: handleClick,
      subtitle: getDescription({ dtStart, dtFinish, path }),
    };

    return <WrappedComponent {...newProps} />;
  };

  return ModifiedComponent;
};

export default initState;
