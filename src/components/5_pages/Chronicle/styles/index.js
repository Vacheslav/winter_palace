import styled from "styled-components";

export const Figure = styled.figure`
  position: absolute;
  left: 0;
  top: 0;
  z-index: 0;
  height: 100%;
  width: 100%;
  opacity: 0.4;
  overflow: hidden;
  & video {
    width: 110vw;
  }
`;

export const Wrapper = styled.main`
  background-color: white;
  position: relative;
  width: 100%;
  height: 100vh;
  padding: 150px 180px 20px 180px;
  & h2 {
    font-size: 36px;
    font-weight: 600;
    padding-left: 1%;
    padding-top: 3%;
  }
`;

export const RawInfo = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 1% 3% 3% 1%;
`;

export const ItemWrapper = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  align-items: center;
  column-gap: 30px;
  border-radius: 10px;
  border: 1px solid lightgray;
  width: 340px;
  padding: 20px 40px;
`;

export const CardWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

export const MainContent = styled.div`
  flex-grow: 1;
  display: flex;
  & .left {
    width: 50%;
    height: 100%;
    display: flex;
    flex-direction: column;
    row-gap: 50px;
    align-items: center;
    justify-content: center;
    background-color: #2e2d29;
    background-image: url("/images/contacts_bgc.jpg");
    background-repeat: no-repeat;
    background-size: cover;
  }
  & .right {
    width: 50%;
    height: 100%;
    background-color: blue;
  }
`;
