import React, { useState } from "react";
import {
  Wrapper,
  ItemWrapper,
  RawInfo,
  CardWrapper,
  MainContent,
} from "./styles";
import { Button, notification } from "antd";

import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";

import L from "leaflet";

const iconPerson = new L.icon({
  iconUrl:
    "https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png",
  iconSize: [30, 41],
  iconAnchor: [20, 51],
});

const Item = ({ icon, title, description }) => (
  <ItemWrapper>
    <i
      className={icon}
      style={{
        fontSize: 24,
        marginBottom: 10,
        color: "gray",
      }}
    />
    <div
      style={{
        color: "gray",
        display: "flex",
        flexDirection: "column",
        maxWidth: 160,
      }}
    >
      <div
        style={{
          color: "black",
          display: "inline-block",
          fontWeight: 700,
        }}
      >
        {title}
      </div>
      <div
        style={{
          wordWrap: "break-word",
          display: "flex",
          flexDirection: "column",
        }}
      >
        {description}
      </div>
    </div>
  </ItemWrapper>
);

const Contats = () => {
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");

  const send = () => {
    notification.open({
      type: "success",
      message: "Мы получили вашу заявку",
      description: `Спасибо, ${name}, наш менеджер свяжется с вами в ближайшее время`,
      onClick: () => {},
    });
  };

  return (
    <Wrapper>
      <CardWrapper>
        <h2>Контакты</h2>
        <RawInfo>
          <Item
            title="Адрес:"
            description="​Дворцовая набережная, 38"
            icon="fal fa-globe-stand"
          />
          <Item
            title="Телефон:"
            description={["​+7 (812) 710-96-11 ", <br />, "+7 (812) 710-90-79"]}
            icon="fal fa-phone"
          />
          <Item
            title="Почта:"
            description={[
              "​chancery@hermitage.ru",
              <br />,
              "press@hermitage.ru",
            ]}
            icon="fal fa-envelope"
          />
          <Item
            title="Вк:"
            description="https://vk.com/hermitage_museum"
            icon="fab fa-vk"
          />
        </RawInfo>
        <MainContent>
          <div className="left">
            <div class="form__group field">
              <input
                type="input"
                class="form__field"
                placeholder="Name"
                name="Имя"
                id="name"
                required
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
              <label for="name" class="form__label">
                Имя
              </label>
            </div>
            <div
              class="form__group field"
              style={{
                marginBottom: 20,
              }}
            >
              <input
                type="input"
                class="form__field"
                placeholder="Телефон или почта"
                name="phone"
                id="phone"
                required
                value={phone}
                onChange={(e) => setPhone(e.target.value)}
              />
              <label for="phone" class="form__label">
                Телефон или почта
              </label>
            </div>
            <Button
              onClick={send}
              style={{
                backgroundColor: "#ebe1bc",
                borderColor: "#ebe1bc",
                color: "black",
              }}
              size="large"
              type="primary"
            >
              Связаться
            </Button>
          </div>
          <div className="right">
            <MapContainer
              center={[59.939898, 30.314784]}
              zoom={13}
              scrollWheelZoom={false}
              style={{ height: "100%", width: "100%" }}
            >
              <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              />
              <Marker icon={iconPerson} position={[59.939898, 30.314784]}>
                <Popup>
                  <div
                    style={{
                      maxWidth: 130,
                    }}
                  >
                    Государственный Эрмитаж, ​Дворцовая набережная, 38
                  </div>
                </Popup>
              </Marker>
            </MapContainer>
          </div>
        </MainContent>
      </CardWrapper>
    </Wrapper>
  );
};

export default Contats;
