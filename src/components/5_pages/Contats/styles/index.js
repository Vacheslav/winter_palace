import styled from "styled-components";

export const Wrapper = styled.main`
  .form__group {
    position: relative;
    padding: 15px 0 0;
    margin-top: 10px;
    width: 50%;
  }

  .form__field {
    font-family: inherit;
    width: 100%;
    border: 0;
    border-bottom: 2px solid #fff3d3;
    outline: 0;
    font-size: 1.3rem;
    color: #fff;
    padding: 7px 0;
    background: transparent;
    transition: border-color 0.2s;

    &::placeholder {
      color: transparent;
    }

    &:placeholder-shown ~ .form__label {
      font-size: 1.3rem;
      cursor: text;
      top: 20px;
    }
  }

  .form__label {
    position: absolute;
    top: 0;
    display: block;
    transition: 0.2s;
    font-size: 1rem;
    color: #fff3d3;
  }

  .form__field:focus {
    ~ .form__label {
      position: absolute;
      top: 0;
      display: block;
      transition: 0.2s;
      font-size: 1rem;
      color: #fff3d3;
      font-weight: 700;
    }
    padding-bottom: 6px;
    font-weight: 700;
    border-width: 3px;
    border-image: linear-gradient(to right, #fff3d3, #fff3d3);
    border-image-slice: 1;
  }
  /* reset input */
  .form__field {
    &:required,
    &:invalid {
      box-shadow: none;
    }
  }

  background-color: white;
  height: 100vh;
  width: 100%;
  padding: 100px 40px 20px 40px;
  & h2 {
    font-size: 36px;
    font-weight: 600;
    padding-left: 1%;
    padding-top: 3%;
  }
`;

export const RawInfo = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 1% 3% 3% 1%;
`;

export const ItemWrapper = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  align-items: center;
  column-gap: 30px;
  border-radius: 10px;
  border: 1px solid lightgray;
  width: 340px;
  padding: 20px 40px;
`;

export const CardWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

export const MainContent = styled.div`
  flex-grow: 1;
  display: flex;
  & .left {
    width: 50%;
    height: 100%;
    display: flex;
    flex-direction: column;
    row-gap: 50px;
    align-items: center;
    justify-content: center;
    background-color: #2e2d29;
    background-image: url("/images/contacts_bgc.jpg");
    background-repeat: no-repeat;
    background-size: cover;
  }
  & .right {
    width: 50%;
    height: 100%;
    background-color: blue;
  }
`;
