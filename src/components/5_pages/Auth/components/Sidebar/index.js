import React, { useState } from "react";
import { Wrapper, Body } from "./styles";
import { Logo, Button, Input } from "@/atoms/index";
import { request, CacheStore, openGqlErrNotifi } from "@/utils/index";

const Sidebar = () => {
  const [status, setStatus] = useState("error");
  const [login, setLogin] = useState(null);
  const [password, setPass] = useState(null);

  const setSession = ({ user, token }) => {
    let date = new Date();

    localStorage.setItem("auth-token", token);
    localStorage.setItem("login", user.login);
    localStorage.setItem("last-logged", date.getTime());

    CacheStore.set("user", user);
    CacheStore.set("token", token);
    CacheStore.cookie.set("jwtToken", token);
  };

  const onSubmit = () => {
    request({
      schema: "SIGN_IN_MUTATION",
      data: {
        login,
        password,
      },
    })
      .then((data) => {
        if (data) {
          data?.signin && setSession(data.signin);
          window.location.href = "/";
        }
      })
      .catch((e) => {
        const err = openGqlErrNotifi(e);
        console.log(e);
      });
  };

  return (
    <Wrapper>
      <Logo size={40} />
      <Body>
        <h2>Здравствуйте,</h2>
        <h3>чтобы пользоваться сайтом, необходимо войти</h3>
        {/* <h4>
          <span>Проблемы с аккаунтом? Можете попробовать их решить </span>
          <a href="#">тут</a>
        </h4> */}
        <Input
          className="input"
          id="login"
          label="логин"
          value={login}
          onChange={setLogin}
          type="login"
          isActive
        />
        <Input
          className="input"
          id="password"
          label="пароль"
          value={password}
          onChange={setPass}
          type="password"
          isActive
        />
        <Button onClick={onSubmit}>Войти</Button>
      </Body>
    </Wrapper>
  );
};

export default Sidebar;
