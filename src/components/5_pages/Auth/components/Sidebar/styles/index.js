import styled from "styled-components";
import { lighten } from "polished";

export const Wrapper = styled.div`
  padding: 20px;
  padding-top: 22vh;
  display: flex;
  flex-direction: column;
  height: 100vh;
  width: 320px;
  background-color: white;
  box-shadow: 3px 0px 30px 0px rgba(255, 224, 255, 0.5);
  position: relative;
  z-index: 2;
`;

export const Body = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 4vh;
  & h1 {
    font-size: 32px;
    font-weight: 800;
    color: #313039;
    margin-bottom: 5vh;
  }
  & h2 {
    font-size: 24px;
    font-weight: 700;
    color: #414049;
    margin-bottom: 1.5vh;
  }
  & h3 {
    font-size: 18px;
    font-weight: 400;
    color: gray;
    margin-bottom: 1.5vh;
  }
  & h4 {
    font-size: 14px;
    font-weight: 400;
    margin-bottom: 5vh;
    & span {
      color: ${lighten(0.15, "red")};
    }
  }
  & .input {
    margin-bottom: 1.5vh;
  }
`;
