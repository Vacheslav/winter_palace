import styled from "styled-components";

export const Wrapper = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  background-image: blue;
`;

export const ImgFrame = styled.div`
  display: flex;
  position: relative;
  align-items: center;
  justify-content: center;
  flex-grow: 1;
  overflow: hidden;
`;

export const Placeholder = styled.div`
  height: 100vh;
  width: 100vh;
  background-color: blue;
`;
