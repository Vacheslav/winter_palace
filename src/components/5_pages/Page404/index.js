import { Wrapper } from "./styles";

const Page404 = () => (
  <Wrapper>
    <div id="container">
      <div class="content">
        <h2>404</h2>
        <h4>Такая страница еще не создана</h4>
        <p>Или сейчас мы не смогли ее найти</p>
        <a href="/">Вернуться на главную</a>
      </div>
    </div>
  </Wrapper>
);

export default Page404;
