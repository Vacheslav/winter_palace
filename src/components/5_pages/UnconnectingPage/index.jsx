import { Wrapper } from "./styles";

const UnconnectingPage = () => {
  return (
    <Wrapper>
      <i className="fal fa-sync" />
      <h4>Идет обновление приложения</h4>
    </Wrapper>
  );
};

export default UnconnectingPage;
