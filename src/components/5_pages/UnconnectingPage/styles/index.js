import styled from "styled-components";

export const Wrapper = styled.div`
  @media (prefers-reduced-motion: no-preference) {
    i {
      animation: App-logo-spin infinite 2s linear;
    }
  }
  margin: 0 !important;
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding-bottom: 3%;
  & i {
    font-size: 72px;
    margin-bottom: 40px;
    pointer-events: none;
  }
  & * {
    text-align: center;
  }
  @keyframes App-logo-spin {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }
`;
