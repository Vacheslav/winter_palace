import React from "react";
import { Wrapper, InnerContainer, Text } from "./styles";

const Petr1 = () => {
  return (
    <Wrapper>
      <InnerContainer>
        <div>
          <img src="/images/history_1.jpg" />
        </div>
        <div>
          <h2>Свадебные палаты Петра I</h2>
          <Text>
            <p className="description">
              Пётр Великий владел участком между Невой и Миллионной улицей (на
              месте нынешнего Эрмитажного театра).
            </p>
            <p className="description">
              Пётр Великий владел участком между Невой и Миллионной улицей (на
              месте нынешнего Эрмитажного театра). В 1712 году были выстроены
              каменные Свадебные палаты Петра I. Этот дворец стал подарком
              губернатора Санкт-Петербурга Александра Даниловича Меншикова к
              свадьбе Петра I и Екатерины Алексеевны.
            </p>
          </Text>
        </div>
      </InnerContainer>
    </Wrapper>
  );
};

export default Petr1;
