import styled from "styled-components";

export const Wrapper = styled.section`
  height: 100vh;
  position: relative;
  width: 100%;
  box-shadow: 4px 4px 18px 0px rgba(34, 60, 80, 0.7);
  z-index: 2;
  display: flex;
  background-image: url("/images/history_petr.png");
  background-repeat: no-repeat;
  background-size: cover;
  padding: 120px 10vw;
`;

export const InnerContainer = styled.div`
  display: flex;
  align-items: center;
  column-gap: 70px;
  & h2 {
    font-size: 36px;
    margin-bottom: 50px;
  }
  & .description {
    font-size: 24px;
    color: #eae2d7;
  }
`;

export const Text = styled.div`
  display: flex;
  flex-direction: column;
`;
