import React from "react";
import { Wrapper, InnerContainer, Text } from "./styles";

const SectionTemplate = ({
  src = "",
  title = "",
  text = [],
  reverse,
  background = "",
}) => {
  return (
    <Wrapper background={background}>
      <InnerContainer
        style={{
          flexDirection: reverse ? "row-reverse" : "row",
        }}
      >
        <div>
          <img src={src} />
        </div>
        <div>
          <h2>{title}</h2>
          <Text>
            {text.map((paragraph) => (
              <p className="description">{paragraph}</p>
            ))}
          </Text>
        </div>
      </InnerContainer>
    </Wrapper>
  );
};

export default SectionTemplate;
