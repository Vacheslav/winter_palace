import styled from "styled-components";

export const Wrapper = styled.section`
  height: 100vh;
  position: relative;
  width: 100%;
  box-shadow: 4px 4px 18px 0px rgba(34, 60, 80, 0.7);
  z-index: 2;
`;

export const ContentWrapper = styled.div`
  position: absolute;
  z-index: 2;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  color: white;
  display: flex;
  align-items: center;
  background: linear-gradient(
    104.64deg,
    rgba(0, 0, 0, 0.9) 3.38%,
    rgba(0, 0, 0, 0.6) 95.31%
  );
`;

export const InnerContentWrapper = styled.div`
  padding: 10%;
`;

export const Figure = styled.figure`
  position: relative;
  height: 100vh;
  z-index: 1;
  width: 100%;
  overflow: hidden;
  & video {
    width: 10vw;
  }
`;
