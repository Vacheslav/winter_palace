import React from "react";
import { Wrapper, ContentWrapper, InnerContentWrapper, Figure } from "./styles";

const Banner = () => {
  return (
    <Wrapper>
      <ContentWrapper className="">
        <InnerContentWrapper>
          <h1 className="text-7xl">Кашалот</h1>
          <h2 className="text-xl font-light mb-8 text-blue-200">
            physeter macrocephalus
          </h2>
          <div className="font-light max-w-lg mb-16">
            стадное животное, живущее большими группами, достигающими иногда
            сотен и даже тысяч голов.
          </div>
          <div className="flex flex-wrap">1 2 3</div>
        </InnerContentWrapper>
      </ContentWrapper>
      <Figure>
        <video autoPlay muted loop="true" autoplay="autoplay">
          <source src="/videos/banner.mp4" type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      </Figure>
    </Wrapper>
  );
};

export default Banner;
