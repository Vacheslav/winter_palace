import styled from "styled-components";

export const Wrapper = styled.main`
  background-color: #1c1918;

  * h2 {
    color: #ffe8aa;
  }
`;

export const ImgFrame = styled.div`
  display: flex;
  position: relative;
  align-items: center;
  justify-content: center;
  flex-grow: 1;
  overflow: hidden;
`;

export const Placeholder = styled.div`
  height: 100vh;
  width: 100vh;
  background-color: blue;
`;
