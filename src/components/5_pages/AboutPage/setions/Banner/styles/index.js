import styled from "styled-components";

export const Wrapper = styled.section`
  height: 100vh;
  position: relative;
  width: 100%;
  box-shadow: 4px 4px 18px 0px rgba(34, 60, 80, 0.7);
  z-index: 2;
`;

export const ContentWrapper = styled.div`
  position: absolute;
  z-index: 2;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  color: white;
  display: flex;
  flex-direction: column;
  justify-content: center;
  background: linear-gradient(
    104.64deg,
    rgba(0, 0, 0, 0.9) 3.38%,
    rgba(0, 0, 0, 0.6) 95.31%
  );
`;

export const InnerContentWrapper = styled.div`
  padding-left: 10%;
  margin-bottom: 50px;
  & h1,
  h2 {
    color: #fff3d3;
  }
  & h1 {
    font-weight: 700;
    font-size: 96px;
    line-height: 50px;
  }
  & h2 {
    font-weight: 500;
    font-size: 24px;
    opacity: 0.6;
  }
`;

export const Figure = styled.figure`
  position: relative;
  height: 100vh;
  z-index: 1;
  width: 100%;
  overflow: hidden;
  & video {
    width: 110vw;
  }
`;

export const RowContainer = styled.div`
  display: flex;
  column-gap: 100px;
  padding-left: 10%;
  margin-bottom: 80px;
`;

export const ButtonsContainer = styled.div`
  display: flex;
  column-gap: 50px;
  padding-left: 10%;
`;

export const InfoContainer = styled.div`
  display: "flex";
  & h3 {
    font-weight: 700;
    font-size: 48px;
    color: #ffe8aa;
  }
  & h4 {
    font-size: 24px;
    color: #fff3d3;
  }
`;
