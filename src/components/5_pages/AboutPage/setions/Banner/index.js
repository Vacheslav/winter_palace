import React from "react";
import {
  Wrapper,
  ContentWrapper,
  InnerContentWrapper,
  Figure,
  RowContainer,
  InfoContainer,
  ButtonsContainer,
} from "./styles";
import { Button } from "@/atoms/index";
import Link from "next/link";

const InfoBlock = ({ title, text, width }) => (
  <InfoContainer>
    <h3>{title}</h3>
    <h4
      style={{
        maxWidth: width,
      }}
    >
      {text}
    </h4>
  </InfoContainer>
);

const Banner = () => {
  return (
    <Wrapper>
      <ContentWrapper className="">
        <InnerContentWrapper>
          <h1>Эрмитаж</h1>
          <h2>Музей мирового уровня</h2>
        </InnerContentWrapper>
        <RowContainer>
          <InfoBlock
            title="~3.000.000"
            width={360}
            text="Произведений искусства и памятников мировой культуры"
          />
          <InfoBlock
            width={250}
            title="233 345 м²"
            text="Общая площадь помещений (зданий)"
          />
          <InfoBlock
            width={200}
            title="4 956 524"
            text="Посетителей в 2019 году"
          />
        </RowContainer>
        <ButtonsContainer>
          <Link href="/model">
            <Button
              style={{
                width: 180,
              }}
              color="#FFF3D3"
            >
              3D Модель
            </Button>
          </Link>
          <Link href="/history">
            <Button
              style={{
                width: 180,
              }}
              color="#FFF3D3"
              type="reversed"
            >
              История
            </Button>
          </Link>
        </ButtonsContainer>
      </ContentWrapper>
      <Figure>
        <video autoPlay muted loop="true" autoplay="autoplay">
          <source src="/videos/banner.mp4" type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      </Figure>
    </Wrapper>
  );
};

export default Banner;
