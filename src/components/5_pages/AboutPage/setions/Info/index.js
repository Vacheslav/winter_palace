import React from "react";
import { Wrapper, RowWrapper, Figure } from "./styles";
import Image from "next/image";
import Link from "next/link";

const Row = ({ img, reversed = false, href, linkText = "", p, h2 }) => {
  const image = (
    <Figure>
      <img layout="responsive" width="70%" height="100%" src={img} />
    </Figure>
  );

  const infoBlock = (
    <div className="info">
      <h2>{h2}</h2>
      <p>{p}</p>
      <Link href={href}>{linkText}</Link>
    </div>
  );
  return (
    <RowWrapper>
      {reversed && (
        <>
          {image}
          {infoBlock}
        </>
      )}
      {!reversed && (
        <>
          {infoBlock}
          {image}
        </>
      )}
    </RowWrapper>
  );
};

const Info = () => {
  return (
    <Wrapper>
      <Row
        h2="О проекте"
        p="Существует множество интересных архитектурных сооружений. Фотографии
        легко найти, но они не передают красоту в полном объеме. На этом сайте
        вы можете узнать о зимнем дворце и дать возможность увидеть зимний
        дворец в 3D."
        href="/model"
        linkText="3D Модель"
        img="/images/winter_c1.jpg"
      />
      <Row
        h2="Краткая история"
        p={
          <>
            <span>
              "Здание дворца было построено в 1754—1762 годах русским
              архитектором итальянского происхождения
            </span>{" "}
            <a
              href="https://ru.wikipedia.org/wiki/%D0%A0%D0%B0%D1%81%D1%82%D1%80%D0%B5%D0%BB%D0%BB%D0%B8,_%D0%91%D0%B0%D1%80%D1%82%D0%BE%D0%BB%D0%BE%D0%BC%D0%B5%D0%BE_%D0%A4%D1%80%D0%B0%D0%BD%D1%87%D0%B5%D1%81%D0%BA%D0%BE"
              target="_blank"
            >
              {" "}
              Бартоломео Франческо Растрелли{" "}
            </a>
            <span>
              {" "}
              в стиле пышного елизаветинского барокко с элементами французского
              рококо в интерьерах. Начиная с советского времени в стенах дворца
              размещена основная экспозиция Государственного Эрмитажа."
            </span>
          </>
        }
        href="/history"
        linkText="История"
        img="/images/demonstrators.jpg"
        reversed
      />
    </Wrapper>
  );
};

export default Info;
