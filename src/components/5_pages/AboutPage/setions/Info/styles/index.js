import styled from "styled-components";

export const Wrapper = styled.section`
  position: relative;
  width: 100%;
  box-shadow: 4px 4px 8px 0px rgba(34, 60, 80, 0.2);
`;

export const RowWrapper = styled.section`
  width: 100%;
  display: flex;
  height: 700px;
  & figure {
    width: 50%;
    height: 700px;
    overflow: hidden;
  }
  & .info {
    width: 50%;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;
    padding: 7% 10%;
    & p {
      line-height: 200%;
      & a {
        font-weight: 500;
        text-decoration: none;
        color: blue;
      }
    }
    & h2 {
      font-size: 36px;
      font-weight: 900;
    }
    & a {
      color: #555555;
      text-decoration: underline;
      font-weight: 700;
      font-size: 14px;
      transition: all 0.3s;
      &:hover {
        color: gray;
      }
    }
  }
`;

export const Figure = styled.div`
  overflow: hidden;
  width: 50%;
  & img {
    width: 100%;
  }
`;
