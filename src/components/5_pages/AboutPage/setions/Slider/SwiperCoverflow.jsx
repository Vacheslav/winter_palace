import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Navigation, Pagination, EffectCoverflow } from "swiper";
import { randKey } from "@/utils/index";

SwiperCore.use([Navigation, Pagination, EffectCoverflow]);

export default function SwiperCoverflow() {
  const styles = {
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
  };

  const links = [
    "/images/winter_3123wd.jpg",
    "/images/winter_new.jpg",
    "/images/slider/1.jpg",
    "/images/slider/2.jpg",
    "/images/slider/3.jpg",
    "/images/slider/4.jpg",
    "/images/slider/5.jpg",
    "/images/slider/6.jpg",
  ];

  return (
    <div>
      <Swiper
        navigation
        pagination={{ clickable: true }}
        effect="coverflow"
        loop={true}
        coverflowEffect={{
          rotate: 50,
          stretch: 0,
          depth: 100,
          modifier: 1,
          slideShadows: true,
        }}
        slidesPerView={2}
        centeredSlides
        style={{ height: "700px" }}
      >
        {links.map((link) => (
          <SwiperSlide
            key={randKey()}
            style={{
              backgroundImage: `url(${link})`,
              ...styles,
            }}
          >
            <span style={{ opacity: 0 }}>1</span>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
}
