import styled from "styled-components";

export const Wrapper = styled.section`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-size: cover;
  background-repeat: no-repeat;
  width: 100%;
  height: calc(100vh - 100px);
  margin-top: 10px;
  padding: 100px 0;
  & * h2 {
    font-size: 36px;
    font-weight: 900;
  }
`;

export const ContentWrapper = styled.div`
  position: absolute;
  z-index: 2;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  color: white;
  display: flex;
  align-items: center;
  background: linear-gradient(
    104.64deg,
    rgba(0, 0, 0, 0.9) 3.38%,
    rgba(0, 0, 0, 0.6) 95.31%
  );
`;

export const InnerContentWrapper = styled.div`
  padding: 10%;
`;

export const Figure = styled.figure`
  position: absolute;
  z-index: 0;
  height: 100%;
  width: 100%;
  opacity: 0.4;
  overflow: hidden;
  & video {
    width: 110vw;
  }
`;
