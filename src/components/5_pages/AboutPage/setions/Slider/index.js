import React from "react";
import { Navigation, Pagination, EffectCube } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import Image from "next/image";
import { Wrapper, Figure } from "./styles";
import dynamic from "next/dynamic";

const SwiperCoverflow = dynamic(() => import("./SwiperCoverflow"), {
  ssr: false,
});

const Slider = () => {
  return (
    <Wrapper>
      <Figure>
        <video autoPlay muted loop="true" autoplay="autoplay">
          <source src="/videos/clouds.mp4" type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      </Figure>
      <div
        style={{
          width: "100%",
          zIndex: 2,
          position: "relative",
        }}
      >
        <h2
          style={{
            marginBottom: 60,
            marginLeft: 100,
          }}
        >
          Галерея
        </h2>
      </div>
      <div
        style={{
          maxWidth: "99.5vw",
          transform: "translate(-0.5vw, 0)",
        }}
      >
        <SwiperCoverflow />
      </div>
    </Wrapper>
  );
};

export default Slider;
