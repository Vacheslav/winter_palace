import { Banner, Slider, Info } from "./setions";

const AboutPage = () => {
  return (
    <main>
      <Banner />
      <Info />
      <Slider />
    </main>
  );
};

export default AboutPage;
