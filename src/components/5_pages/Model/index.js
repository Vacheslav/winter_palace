import React, { useRef, useState, Suspense, useEffect } from "react";
import { Canvas, useFrame, useLoader, useThree } from "@react-three/fiber";
import { OrbitControls } from "@react-three/drei";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { Wrapper, ControlBlock, LoadingWrapper } from "./styles";
import * as THREE from "three";
import Loader from "./Loader.jsx";

const Scene = ({ newMaterialOpt }) => {
  const {
    scene,
    camera,
    gl: { domElement },
  } = useThree();

  useEffect(() => {
    scene.background = new THREE.Color("#54547d");
    scene.fog = new THREE.Fog("#7d7d8c", -10, 60);
    camera.fov = 100000;
    camera.position.x = -2.7;
    camera.position.y = -0.3;
  }, []);

  return <> </>;
};

function useInterval(callback, delay, stop = false) {
  const savedCallback = useRef();

  useEffect(() => {
    if (!stop) {
      savedCallback.current = callback;
    }
  }, [callback]);

  useEffect(() => {
    if (!stop) {
      function tick() {
        savedCallback.current();
      }
      if (delay !== null) {
        let id = setInterval(tick, delay);
        return () => clearInterval(id);
      }
    }
  }, [delay]);
}

const LoadingScene = ({ rendered }) => {
  const [progress, setProgress] = useState(0);

  const randTick = rendered ? 100 : 10;

  const [tick, setTick] = useState(Math.random() * randTick);

  const realyProgress = Math.round(progress / 40);

  const isRendered = realyProgress > 99;

  useInterval(
    () => {
      setTick(Math.random() * randTick);
    },
    2000,
    isRendered
  );

  useInterval(
    () => {
      setProgress(progress + tick);
    },
    100,
    isRendered
  );

  if (isRendered) return <></>;

  return (
    <LoadingWrapper>
      <div>
        <div
          style={{
            display: "flex",
            columnGap: 10,
          }}
        >
          <Loader />
          <h2>Идет загрузка ({realyProgress}%)...</h2>
        </div>
        <progress
          style={{
            height: 10,
            width: "100%",
          }}
          max="100"
          value={`${realyProgress}`}
        >
          10%
        </progress>
      </div>
    </LoadingWrapper>
  );
};

function Keen({ isRotation = false, speed, rendered, setRendered }) {
  const [init, setInit] = useState(false);

  const ref = useRef();

  useFrame((state) => {
    if (!init) {
      setInit(true);
      ref.current.rotation.y = 2.8;
    }

    ref.current.rotation.y += isRotation ? speed : 0;
    ref.current.rotation.x += 0;
    ref.current.rotation.z += 0;
  });

  const { nodes, materials } = useLoader(
    GLTFLoader,
    "/model/winter_summary.gltf"
  );

  if (!rendered && nodes?.mesh_0) {
    setRendered(true);
  }

  return (
    <group ref={ref} scale={10} position={[-1, -1, 1]} dispose={null}>
      <mesh
        material={materials["Material"]}
        geometry={nodes.mesh_0.geometry}
        castShadow
        receiveShadow
      />
    </group>
  );
}

const Model = () => {
  const [isRotation, cIsRotation] = useState(false);
  const [speed, setSpeed] = useState(0.001);
  const [rendered, setRendered] = useState(false);

  return (
    <Wrapper>
      <LoadingScene rendered={rendered} />
      <ControlBlock>
        <button onClick={() => cIsRotation(!isRotation)}>
          {!isRotation && <i class="fad fa-play green"></i>}
          {isRotation && <i class="fad fa-stop red"></i>}
        </button>
        <button onClick={() => setSpeed(speed + 0.001)}>
          <i class="fas fa-forward"></i>
        </button>
        <button onClick={() => setSpeed(speed - 0.001)}>
          <i class="fas fa-backward"></i>
        </button>
      </ControlBlock>
      <Canvas>
        <OrbitControls />
        <Scene />
        <mesh receiveShadow rotation={[-1.55, 0, 0]} position={[0, -1.39, -0]}>
          <planeBufferGeometry attach="geometry" args={[500, 500]} />
          <meshStandardMaterial attach="material" color="#6c806f" />
        </mesh>
        <mesh receiveShadow rotation={[-1.55, 0, 0]} position={[0, -1.45, -0]}>
          <planeBufferGeometry attach="geometry" args={[500, 500]} />
          <meshStandardMaterial attach="material" color="#6c806f" />
        </mesh>
        <mesh receiveShadow rotation={[-1.55, 0, 0]} position={[0, -1.5, -0]}>
          <planeBufferGeometry attach="geometry" args={[500, 500]} />
          <meshStandardMaterial attach="material" color="#6c806f" />
        </mesh>
        <mesh receiveShadow rotation={[-1.55, 0, 0]} position={[0, -2, -0]}>
          <planeBufferGeometry attach="geometry" args={[500, 500]} />
          <meshStandardMaterial attach="material" color="#6c806f" />
        </mesh>
        <Suspense fallback={null}>
          <Keen
            rendered={rendered}
            setRendered={setRendered}
            speed={speed}
            isRotation={isRotation}
          />
        </Suspense>
        <ambientLight />
        <pointLight power={10} position={[40, 50, 10]} />
      </Canvas>
    </Wrapper>
  );
};

export default Model;
