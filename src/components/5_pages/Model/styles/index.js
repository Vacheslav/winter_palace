import styled from "styled-components";

export const Wrapper = styled.main`
  background-color: gray;
  height: 100vh;
  width: 100%;
`;

export const ImgFrame = styled.div`
  display: flex;
  position: relative;
  align-items: center;
  justify-content: center;
  flex-grow: 1;
  overflow: hidden;
`;

export const Placeholder = styled.div`
  height: 100vh;
  width: 100vh;
  background-color: blue;
`;

export const ControlBlock = styled.div`
  transition: all 0.3s;
  z-index: 5;
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  row-gap: 10px;
  background-color: white;
  height: 180px;
  width: 50px;
  display: flex;
  right: 0;
  top: 50%;
  opacity: 0.1;

  transform: translate(0, -50%);

  &:hover {
    opacity: 1;
  }

  & button {
    color: gray;
    background-color: white;
    cursor: pointer;
    border-radius: 9999px;
    border: 1px solid lightgray;
    padding: 5px;
    display: flex;
    align-items: center;
    justify-content: center;
    height: 35px;
    width: 35px;
    & .red {
      color: red;
    }
    & .green {
      color: green;
    }
  }
`;

export const LoadingWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  background-color: #3b3937;
  position: fixed;
  z-index: 10;
  display: flex;
  align-items: center;
  justify-content: center;
  & h2 {
    color: #c9c0a7;
  }
`;
