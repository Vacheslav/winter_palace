import React, { useState } from "react";
import {
  Wrapper,
  CardWrapper,
  MainContent,
  GraphicsWrapper,
  ControlBlock,
  PageTextWrapper,
  InfoCard,
} from "./styles";
import {
  Switcher,
  GraphicPeoples,
  MoneyBySPB,
  MoneyFronOutside,
  MoneyInsideOuside,
  MoneyFull,
  GraphicIncome,
  BarIncome,
  GraphicExpenses,
  BarExpenses,
  BarPeoples,
  ModalInfo,
} from "./components";
import { Tooltip as AntTooltip, Modal } from "antd";

import { Chart as ChartJS, ArcElement, Tooltip, Legend, Title } from "chart.js";
ChartJS.register(ArcElement, Tooltip, Legend, Title);

const ActiveGrapic = ({ keyObj = "", children, actived = {} }) => {
  return actived[keyObj] ? <>{children}</> : "";
};

const btns = [
  {
    value: "text",
    render: <i className="far fa-eye" />,
    tooltip: "Описание страницы",
  },
  {
    value: 1,
    render: <i className="far fa-book" />,
    tooltip: "Статистика посещений зимнего дворца по годам",
  },
  {
    value: 2,
    render: <i className="far fa-file-chart-line" />,
    tooltip: "Опрос о суммах на пожертвование 'Эрмитажу'",
  },
  {
    value: 3,
    render: <i className="far fa-file-code" />,
    tooltip: "Статистика доходности зимнего дворца",
  },
  {
    value: 4,
    render: <i className="far fa-file-certificate" />,
    tooltip: "Основные источники дохода (млн. руб)",
  },
  {
    value: 5,
    render: <i className="far fa-file-contract" />,
    tooltip: "Расходы Эрмитажа (млн. руб)",
  },
];

const Statistic = () => {
  const [actived, setActived] = useState({
    text: true,
    1: true,
    2: true,
    3: true,
    4: false,
    5: false,
  });

  return (
    <Wrapper>
      <ControlBlock>
        {btns.map(({ value, render, tooltip }) => (
          <AntTooltip placement="left" title={tooltip}>
            <button
              className={actived[value] ? "active" : "disabled"}
              onClick={() => {
                setActived({ ...actived, [value]: !actived[value] });
              }}
            >
              {render}
            </button>
          </AntTooltip>
        ))}
      </ControlBlock>

      <CardWrapper>
        <ActiveGrapic keyObj={"text"} actived={actived}>
          <PageTextWrapper>
            <div
              style={{
                paddingLeft: 80,
                display: "flex",
                flexDirection: "column",
                maxWidth: 800,
              }}
            >
              <AntTooltip
                title="Подведите курсор к правому краю браузера (середина экрана по
              высоте) чтобы увидеть переключатели графиков"
              >
                <h2
                  style={{
                    fontWeight: 900,
                    fontSize: 22,
                    maxWidth: 330,
                  }}
                >
                  Зимний дворец в цифрах
                  <span
                    style={{
                      verticalAlign: "super",
                      fontSize: 16,
                      color: "gray",
                      fontWeight: 600,
                    }}
                  >
                    (?)
                  </span>
                </h2>
              </AntTooltip>
              <p>
                Эрмитаж стал первым российским музеем, который провел подобную
                оценку своей деятельности
              </p>
              <p
                style={{
                  maxWidth: 800,
                }}
              >
                По данным исследования ЕУ, историко-культурному музею на
                Дворцовой площади уступает только ГМЗ «Петергоф», а Русский
                музей занимает лишь шестое место. В 2020 музей был закрыт из -
                за пандемии на 104 дня.
              </p>
              <p
                style={{
                  maxWidth: 800,
                }}
              >
                В целом, произведений искусства и памятников мировой культуры,
                живопись, графика, скульптура и предметы прикладного искусства,
                археологические находки и нумизматический материал — 3 106 071
                (на 2013 год).
              </p>
            </div>
            <div
              style={{
                display: "flex",
                flexWrap: "wrap",
                columnGap: 40,
                rowGap: 40,
              }}
            >
              <InfoCard>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                  <path d="M306.6 452.4L288 443.1V432c0-13.23-10.77-24-24-24h-208C42.77 408 32 418.8 32 432v11.06l-18.58 9.344C5.266 456.3 0 464.7 0 473.9V488C0 501.2 10.77 512 24 512h272c13.23 0 24-10.77 24-24v-14.12C320 464.8 314.8 456.3 306.6 452.4zM304 488c0 4.484-3.516 8-8 8h-272c-4.484 0-8-3.516-8-8v-14.12c0-3.016 1.703-5.781 4.469-7.109L48 452.9V432c0-4.484 3.516-8 8-8h208c4.484 0 8 3.516 8 8v20.94l27.67 13.91C302.3 468.1 304 470.9 304 473.9V488zM71.86 210.7c0 0-7.854 165.2-7.854 165.3c0 1.447 1.081 7.992 8.316 7.992c4.06 0 7.471-3.444 7.663-7.643c0 0 8.012-168.3 8.012-168.4c0-1.851-.6644-3.64-1.855-5.078L53.56 163.8C49.97 159.5 48 154 48 148.4V80h32v20C80 115.5 92.54 128 107.1 128S136 115.5 136 100V80h48v20C184 115.5 196.5 128 211.1 128S240 115.5 240 100V80h32v68.42c0 5.614-1.968 11.05-5.562 15.36l-32.58 39.09C232.7 204.3 232 206.1 232 207.1c0 .1405 .0038 .2814 .0115 .4225l8 168C240.2 380.7 243.8 384 248 384c4.408 0 7.994-3.576 7.994-7.974c0-.1331-.0033-.2669-.01-.4014l-7.844-164.9l30.6-36.73C284.7 166.8 288 157.8 288 148.4V72C288 67.58 284.4 64 280 64h-48C227.6 64 224 67.58 224 72v28C224 106.6 218.6 112 212 112S200 106.6 200 100V72C200 67.58 196.4 64 192 64H128C123.6 64 120 67.58 120 72v28C120 106.6 114.6 112 108 112S96 106.6 96 100V72C96 67.58 92.42 64 88 64h-48C35.58 64 32 67.58 32 72v76.42C32 157.8 35.28 166.8 41.26 174L71.86 210.7zM160 192C142.4 192 128 206.4 128 224v56C128 284.4 131.6 288 136 288h48C188.4 288 192 284.4 192 280V224C192 206.4 177.6 192 160 192zM176 272h-32V224c0-8.828 7.172-16 16-16s16 7.172 16 16V272z" />
                </svg>
                <div className="info_wrap">
                  <div className="title">Количество комнат и залов</div>
                  <div className="count">1057</div>
                </div>
              </InfoCard>
              <InfoCard>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                  <path d="M381.9 337.8l33.5-82.03c2.875-7.062 2.031-15.06-2.219-21.41C408.9 227.8 401.6 224 393.7 224h-46.22c2.668-10.29 4.534-20.88 4.534-32c0-4.406-3.596-7.875-8.003-7.875s-8 3.688-8 8.094C335.1 253.8 285.7 304 223.1 304S112 253.8 112 192.2c0-4.406-3.594-8.094-8-8.094S96 187.7 96 192.1C96 203.1 97.88 213.8 100.5 224H54.31C46.44 224 39.13 227.8 34.78 234.3C30.53 240.7 29.69 248.7 32.56 255.8l33.5 82C24 370.6 0 418.9 0 471.3C0 493.7 18.59 512 41.44 512h144c2.5 0 4.844-1.156 6.375-3.156c1.5-2 2-4.594 1.344-7l-33.41-120c-1.188-4.25-5.594-6.594-9.875-5.562c-4.25 1.188-6.75 5.594-5.562 9.875L174.9 496H41.44C27.41 496 16 484.9 16 471.3c0-49.19 23.53-94.5 64.53-124.3c2.969-2.156 4.094-6.094 2.688-9.5L47.38 249.7C46.5 247.6 46.75 245.2 48.06 243.3C49.44 241.2 51.72 240 54.31 240h51.16C124.5 286.8 170.3 320 223.1 320c53.61 0 99.46-33.13 118.5-80h51.2c2.594 0 4.875 1.188 6.25 3.25c1.312 1.938 1.562 4.312 .6875 6.438l-35.84 87.72c-1.406 3.406-.2813 7.344 2.688 9.5C408.5 376.8 432 422.1 432 471.3C432 484.9 420.6 496 406.6 496h-133.5l30.59-109.8c1.188-4.281-1.312-8.688-5.562-9.875c-4.281-1.094-8.688 1.281-9.875 5.562l-33.41 120c-.6562 2.406-.1562 5 1.344 7C257.7 510.8 260.1 512 262.6 512h144C429.4 512 448 493.7 448 471.3C448 418.9 424 370.6 381.9 337.8zM110.3 124.3C110.8 124.4 111.4 124.5 112 124.5c3.654 0 6.967-2.531 7.811-6.25C134.3 53.28 153.8 16 173.3 16c6.875 0 14 2.781 21.12 8.281c17.44 13.62 41.84 13.59 59.22 .0313C260.8 18.78 267.9 16 274.8 16c19.56 0 39.03 37.09 53.38 101.8c.9687 4.312 5.279 7.094 9.529 6.094c4.312-.9687 7.062-5.219 6.094-9.531C326.1 38.47 303.8 0 274.8 0c-10.5 0-20.91 3.906-30.94 11.66C232.2 20.78 216 20.78 204.2 11.62C194.2 3.906 183.8 0 173.3 0C144.5 0 121.3 38.59 104.2 114.8C103.2 119.1 105.9 123.3 110.3 124.3zM128 176.8v20.47C128 220.8 147.4 240 171.2 240h11.31c17.84 0 34-11.31 40.16-28.16h2.625C231.5 228.7 247.6 240 265.5 240h11.31C300.6 240 320 220.8 320 197.2V176.8c81.03-13.81 101.6-42.72 102.7-44.31c2.438-3.625 1.438-8.5-2.156-10.97c-3.594-2.406-8.562-1.562-11.09 2C409.1 123.9 375.9 168 224 168c-150.1 0-184.7-43.56-185.4-44.53C36.13 119.8 31.22 118.9 27.56 121.3C23.88 123.8 22.91 128.8 25.34 132.4C26.41 134 46.97 162.9 128 176.8zM144 179.2C166.4 182.2 192.8 184 224 184s57.56-1.844 80-4.805v18.02C304 212 291.8 224 276.8 224h-11.31c-11.34 0-21.22-6.938-25.12-17.62C237.8 199.3 231.3 194.7 224 194.7S210.3 199.3 207.7 206.3C203.8 217.1 193.9 224 182.5 224H171.2C156.2 224 144 212 144 197.2V179.2zM248 344h-48C195.6 344 192 347.6 192 352s3.594 8 8 8h16v144C216 508.4 219.6 512 224 512s8-3.594 8-8v-144h16c4.406 0 8-3.594 8-8S252.4 344 248 344z" />
                </svg>
                <div className="info_wrap">
                  <div className="title">Количество потайных комнат</div>
                  <div className="count">~20</div>
                </div>
              </InfoCard>
              <InfoCard>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                  <path d="M504 496H480v-168c0-4.406-3.594-8-8-8s-8 3.594-8 8v168h-416v-168C48 323.6 44.41 320 40 320S32 323.6 32 328v168H8C3.594 496 0 499.6 0 504S3.594 512 8 512h496c4.406 0 8-3.594 8-8S508.4 496 504 496zM40 288h432C476.4 288 480 284.4 480 280v-240C480 17.94 462.1 0 440 0H72C49.94 0 32 17.94 32 40v240C32 284.4 35.59 288 40 288zM264 16h176c13.22 0 24 10.78 24 24V272h-200V16zM48 40c0-13.22 10.78-24 24-24h176v256H48V40z" />
                </svg>
                <div className="info_wrap">
                  <div className="title">Число окон эрмитажа</div>
                  <div className="count">1945</div>
                </div>
              </InfoCard>
              <InfoCard>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                  <path d="M576 40c0 4.422-3.594 8-8 8h-128v136C440 188.4 436.4 192 432 192h-136v136c0 4.422-3.594 8-8 8H152v136C152 476.4 148.4 480 144 480H8C3.594 480 0 476.4 0 472s3.594-8 8-8h128v-136C136 323.6 139.6 320 144 320h136V184C280 179.6 283.6 176 288 176h136V40C424 35.58 427.6 32 432 32h136C572.4 32 576 35.58 576 40z" />
                </svg>
                <div className="info_wrap">
                  <div className="title">Число лестниц эрмитажа</div>
                  <div className="count">177</div>
                </div>
              </InfoCard>
            </div>
          </PageTextWrapper>
        </ActiveGrapic>
        <MainContent>
          <ActiveGrapic keyObj={1} actived={actived}>
            <GraphicsWrapper>
              <Switcher
                leftTitle="Бар"
                rightTitle="График"
                title="Статистика посещений зимнего дворца по годам"
                First={BarPeoples}
                Second={GraphicPeoples}
              />
            </GraphicsWrapper>
          </ActiveGrapic>
          <ActiveGrapic keyObj={2} actived={actived}>
            <GraphicsWrapper>
              <Switcher
                leftTitle="Жители СПБ"
                rightTitle="Приезжие"
                title="Опрос о суммах на пожертвование 'Эрмитажу'"
                First={MoneyBySPB}
                Second={MoneyFronOutside}
              />
            </GraphicsWrapper>
          </ActiveGrapic>
          <ActiveGrapic keyObj={3} actived={actived}>
            <GraphicsWrapper>
              <Switcher
                leftTitle="Подробно"
                rightTitle="Приезжие/местные"
                title="Статистика доходности зимнего дворца"
                First={MoneyFull}
                Second={MoneyInsideOuside}
              />
            </GraphicsWrapper>
          </ActiveGrapic>
          <ActiveGrapic keyObj={4} actived={actived}>
            <GraphicsWrapper>
              <Switcher
                leftTitle="График"
                rightTitle="Бар"
                title="Основные источники дохода (млн. руб)"
                First={GraphicIncome}
                Second={BarIncome}
              />
            </GraphicsWrapper>
          </ActiveGrapic>
          <ActiveGrapic keyObj={5} actived={actived}>
            <GraphicsWrapper>
              <Switcher
                leftTitle="График"
                rightTitle="Бар"
                title="Расходы Эрмитажа (млн. руб)"
                First={GraphicExpenses}
                Second={BarExpenses}
              />
            </GraphicsWrapper>
          </ActiveGrapic>
        </MainContent>
      </CardWrapper>
    </Wrapper>
  );
};

export default Statistic;
