import React from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip,
  Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";

const stat = [
  {
    year: 2009,
    personal: 655.8,
    cur_expenses: 742.2,
    capital_expenses: 1649.5,
    summary: "3047.5",
  },
  {
    year: 2010,
    personal: 781.5,
    cur_expenses: 477.7,
    capital_expenses: 1249.5,
    summary: "2508.7",
  },
  {
    year: 2011,
    personal: 1017.8,
    cur_expenses: 591.3,
    capital_expenses: 945.2,
    summary: "2554.3",
  },
  {
    year: 2012,
    personal: 1180.6,
    cur_expenses: 928.7,
    capital_expenses: 1647.2,
    summary: "3756.5",
  },
  {
    year: 2013,
    personal: 1401.2,
    cur_expenses: 943.3,
    capital_expenses: 2847.9,
    summary: "5192.4",
  },
  {
    year: 2014,
    personal: 1553.2,
    cur_expenses: 1121.2,
    capital_expenses: 2431.2,
    summary: "5105.6",
  },
  {
    year: 2015,
    personal: 1672.1,
    cur_expenses: 1321.2,
    capital_expenses: 2721.1,
    summary: "5714.4",
  },
  {
    year: 2016,
    personal: 1721,
    cur_expenses: 1301.5,
    capital_expenses: 3039.7,
    summary: "6062.2",
  },
  {
    year: 2017,
    personal: 1793.2,
    cur_expenses: 1425.2,
    capital_expenses: 2903.1,
    summary: "6121.5",
  },
  {
    year: 2018,
    personal: 1801.2,
    cur_expenses: 1592.9,
    capital_expenses: 3201.2,
    summary: 6595.3,
  },
  {
    year: 2019,
    personal: 1950.2,
    cur_expenses: 1602.4,
    capital_expenses: 2781.2,
    summary: 6333.8,
  },
  {
    year: 2020,
    personal: 1949.1,
    cur_expenses: 989.2,
    capital_expenses: 3131.2,
    summary: 6569.5,
  },
  {
    year: 2021,
    personal: 2003.2,
    cur_expenses: 1002.4,
    capital_expenses: 3210.2,
    summary: "6215.8",
  },
];

// console.log(
//   JSON.stringify(
//     stat.map((el) => {
//       const { personal, cur_expenses, capital_expenses } = el;
//       return {
//         ...el,
//         summary: (personal + cur_expenses + capital_expenses).toFixed(1),
//       };
//     })
//   )
// );

const options = {
  responsive: true,
  plugins: {
    legend: {
      position: "top",
    },
    title: {
      display: true,
    },
  },
};

const labels = stat.map(({ year }) => `${year}`);

const data = {
  labels,
  datasets: [
    {
      label: "Cуммарные расходы",
      data: stat.map(({ summary }) => summary),
      borderColor: "rgb(255, 99, 132)",
      backgroundColor: "rgba(255, 99, 132, 0.5)",
    },
    {
      label: "Затраты на персонал",
      data: stat.map(({ personal }) => personal),
      borderColor: "#5e4fd1",
      backgroundColor: "#7361ff",
    },
    {
      label: "Текущие расходы",
      data: stat.map(({ cur_expenses }) => cur_expenses),
      borderColor: "#4db2d1",
      backgroundColor: "#61daff",
    },
    {
      label: "Капитальные расходы",
      data: stat.map(({ capital_expenses }) => capital_expenses),
      borderColor: "#47c480",
      backgroundColor: "#5effa7",
    },
  ],
};

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip,
  Legend
);

const DounhnutPeoples = () => {
  return (
    <div
      style={{
        width: "100%",
        height: "100%",
      }}
    >
      <Line height="340px" options={options} data={data} />
    </div>
  );
};

export default DounhnutPeoples;
