import React from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";

ChartJS.register(CategoryScale, LinearScale, BarElement, Tooltip, Legend);

const stat = [
  { year: 2009, fed: 2552.5, main: 524.5, sacrifice: 35.9, summary: 3112.9 },
  { year: 2010, fed: 1873.3, main: 586.5, sacrifice: 28.6, summary: 2488.4 },
  {
    year: 2011,
    fed: 1810.5,
    main: 717.8,
    sacrifice: 45.9,
    summary: 2574.2000000000003,
  },
  { year: 2012, fed: 3424.6, main: 869.2, sacrifice: 30.2, summary: 4324 },
  { year: 2013, fed: 4766.6, main: 935.6, sacrifice: 41.9, summary: 5744.1 },
  {
    year: 2014,
    fed: 4786.6,
    main: 975.6,
    sacrifice: 32.1,
    summary: 5794.300000000001,
  },
  {
    year: 2015,
    fed: 4861.6,
    main: 1035.1,
    sacrifice: 49.2,
    summary: 5945.900000000001,
  },
  {
    year: 2016,
    fed: 5166.6,
    main: 1095.6,
    sacrifice: 35.3,
    summary: 6297.500000000001,
  },
  { year: 2017, fed: 5766.6, main: 1102.2, sacrifice: 48.2, summary: 6917 },
  { year: 2018, fed: 5423.2, main: 1132.4, sacrifice: 45.2, summary: 6600.8 },
  {
    year: 2019,
    fed: 5361.2,
    main: 1267.2,
    sacrifice: 51.2,
    summary: 6679.599999999999,
  },
  {
    year: 2020,
    fed: 6113.1,
    main: 321.6,
    sacrifice: 114,
    summary: 6548.700000000001,
  },
  { year: 2021, fed: 6012.2, main: 632.6, sacrifice: 45.9, summary: 6690.7 },
];

const options = {
  plugins: {
    title: {
      display: true,
    },
  },
  responsive: true,
  interaction: {
    mode: "index",
    intersect: false,
  },
  scales: {
    x: {
      stacked: true,
    },
    y: {
      stacked: true,
    },
  },
};

const labels = stat.map(({ year }) => `${year}`);

const data = {
  labels,
  datasets: [
    {
      label: "Cуммарный доход",
      data: stat.map(({ summary }) => summary),
      borderColor: "rgb(255, 99, 132)",
      backgroundColor: "rgba(255, 99, 132, 0.5)",
    },
    {
      label: "Федернальные взносы",
      data: stat.map(({ fed }) => fed),
      borderColor: "#5e4fd1",
      backgroundColor: "#7361ff",
    },
    {
      label: "Доход от основных видов деятельности",
      data: stat.map(({ main }) => main),
      borderColor: "#4db2d1",
      backgroundColor: "#61daff",
    },
    {
      label: "Доход от пожертвований",
      data: stat.map(({ sacrifice }) => sacrifice),
      borderColor: "#47c480",
      backgroundColor: "#5effa7",
    },
  ],
};

const BarIncome = () => {
  return (
    <div
      style={{
        width: "100%",
        height: "100%",
      }}
    >
      <Bar height="340px" options={options} data={data} />
    </div>
  );
};

export default BarIncome;
