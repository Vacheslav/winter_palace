import React, { useState } from "react";
import { ViewSwitcher } from "@/atoms/index";
import { Wrapper } from "./styles";

const Switcher = ({
  title,
  First,
  Second,
  leftTitle = "",
  rightTitle = "",
}) => {
  const [isActive, setIsActive] = useState(true);

  return (
    <Wrapper>
      <h4
        style={{
          fontSize: 16,
        }}
      >
        {title}
      </h4>
      <div
        style={{
          marginBottom: 20,
        }}
      >
        <ViewSwitcher
          isActive={isActive}
          setIsActive={setIsActive}
          leftTitle={leftTitle}
          rightTitle={rightTitle}
        />
      </div>
      {isActive ? <First /> : <Second />}
    </Wrapper>
  );
};

export default Switcher;
