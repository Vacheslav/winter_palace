import React from "react";
import { Doughnut } from "react-chartjs-2";
import { Chart as ChartJS, ArcElement, Tooltip, Legend, Title } from "chart.js";
ChartJS.register(ArcElement, Tooltip, Legend, Title);

const stat = [
  {
    year: 2009,
    peoples: 2426203,
  },
  {
    year: 2010,
    peoples: 2490387,
  },
  {
    year: 2011,
    peoples: 2879686,
  },
  {
    year: 2012,
    peoples: 2882385,
  },
  {
    year: 2013,
    peoples: 3120170,
  },
  {
    year: 2014,
    peoples: 3400120,
    free: 0, //%
  },
  {
    year: 2015,
    peoples: 3600000,
    free: 0, //%
  },
  {
    year: 2016,
    peoples: 3700100,
    free: 0, //%
  },
  {
    year: 2017,
    peoples: 3839000,
    free: 34, //%
  },
  {
    year: 2018,
    peoples: 4374600,
  },
  {
    year: 2019,
    peoples: 4956524,
  },
  {
    year: 2020,
    peoples: 968604,
    coomment: "был закрыл из за пандемии 104 дня",
  },
  {
    year: 2021,
    peoples: 1500000,
  },
];

const MoneyBySPB = () => {
  return (
    <div
      style={{
        width: "100%",
        height: "100%",
      }}
    >
      <Doughnut
        title="Статистика посещений"
        datasetIdKey="year"
        options={{
          plugins: {
            tooltip: {
              callbacks: {
                title: function () {
                  return "Статистика";
                },
              },
            },
          },
        }}
        data={{
          labels: stat.map((el) => el.year),
          datasets: [
            {
              label: "# of Votes",
              data: stat.map((el) => el.peoples),
              backgroundColor: [
                "rgba(255, 99, 132, 0.2)",
                "rgba(54, 162, 235, 0.2)",
                "rgba(255, 206, 86, 0.2)",
                "rgba(75, 192, 192, 0.2)",
                "rgba(153, 102, 255, 0.2)",
                "rgba(255, 159, 64, 0.2)",
              ],
              borderColor: [
                "rgba(255, 99, 132, 1)",
                "rgba(54, 162, 235, 1)",
                "rgba(255, 206, 86, 1)",
                "rgba(75, 192, 192, 1)",
                "rgba(153, 102, 255, 1)",
                "rgba(255, 159, 64, 1)",
              ],
              borderWidth: 1,
            },
          ],
        }}
      />
    </div>
  );
};

export default MoneyBySPB;
