import React from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";

const stat = [
  {
    year: 2009,
    peoples: 2426203,
    fromRussia: 31,
    fromOutside: 29,
    free: 35,
    other: 5,
  },
  {
    year: 2010,
    peoples: 2490387,
    fromRussia: 31,
    fromOutside: 29,
    free: 35,
    other: 5,
  },
  {
    year: 2011,
    peoples: 2879686,
    fromRussia: 31,
    fromOutside: 29,
    free: 35,
    other: 5,
  },
  {
    year: 2012,
    peoples: 2882385,
    fromRussia: 31,
    fromOutside: 29,
    free: 35,
    other: 5,
  },
  {
    year: 2013,
    peoples: 3120170,
    fromRussia: 31,
    fromOutside: 29,
    free: 35,
    other: 5,
  },
  {
    year: 2014,
    peoples: 3400120,
    free: 0, //%
    fromRussia: 31,
    fromOutside: 29,
    free: 35,
    other: 5,
  },
  {
    year: 2015,
    peoples: 3600000,
    free: 0, //%
    fromRussia: 31,
    fromOutside: 29,
    free: 35,
    other: 5,
  },
  {
    year: 2016,
    peoples: 3700100,
    free: 0, //%
    fromRussia: 29,
    fromOutside: 33,
    free: 35,
    other: 3,
  },
  {
    year: 2017,
    peoples: 3839000,
    free: 34, //%
    fromRussia: 31,
    fromOutside: 29,
    free: 35,
    other: 5,
  },
  {
    year: 2018,
    peoples: 4374600,
    fromRussia: 32,
    fromOutside: 28,
    free: 35,
    other: 5,
  },
  {
    year: 2019,
    peoples: 4956524,
    fromRussia: 31,
    fromOutside: 29,
    free: 35,
    other: 5,
  },
  {
    year: 2020,
    peoples: 968604,
    coomment: "был закрыл из за пандемии 104 дня",
    fromRussia: 46,
    fromOutside: 9,
    free: 35,
    other: 10,
  },
  {
    year: 2021,
    peoples: 1500000,
    fromRussia: 31,
    fromOutside: 29,
    free: 35,
    other: 5,
  },
];

const options = {
  responsive: true,
  plugins: {
    legend: {
      position: "top",
    },
    title: {
      display: true,
    },
  },
};

const labels = stat.map(({ year }) => `${year}`);

const data = {
  labels,
  datasets: [
    {
      label: "Количество посетителей",
      data: stat.map(({ peoples }) => peoples),
      borderColor: "rgb(255, 99, 132)",
      backgroundColor: "rgba(255, 99, 132, 0.5)",
    },
    {
      label: "Количество посетителей из россии",
      data: stat.map(({ peoples, fromRussia }) => (peoples * fromRussia) / 100),
      borderColor: "#5e4fd1",
      backgroundColor: "#7361ff",
    },
    {
      label: "Количество посетителей из других стран",
      data: stat.map(
        ({ peoples, fromOutside }) => (peoples * fromOutside) / 100
      ),
      borderColor: "#4db2d1",
      backgroundColor: "#61daff",
    },
    {
      label: "Бесплатные билеты",
      data: stat.map(({ peoples, free }) => (peoples * free) / 100),
      borderColor: "#47c480",
      backgroundColor: "#5effa7",
    },
    {
      label: "Другие категории",
      data: stat.map(({ peoples, other }) => (peoples * other) / 100),
      borderColor: "#c4a149",
      backgroundColor: "#ffd15e",
    },
  ],
};

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip,
  Legend
);

const DounhnutPeoples = () => {
  return (
    <div
      style={{
        width: "100%",
        height: "100%",
      }}
    >
      <Line height="340px" options={options} data={data} />
    </div>
  );
};

export default DounhnutPeoples;
