import React from "react";
import { Doughnut } from "react-chartjs-2";
import { Chart as ChartJS, ArcElement, Tooltip, Legend, Title } from "chart.js";
ChartJS.register(ArcElement, Tooltip, Legend, Title);

const stat = [
  {
    label: "не готовы делать вклад",
    proc: 7.7,
  },
  {
    label: "до 100 руб",
    proc: 10.2,
  },
  {
    label: "от 100 до 1000 руб",
    proc: 53.6,
  },
  {
    label: "от 1000 до 3000 руб",
    proc: 14.5,
  },
  {
    label: "от 3000 до 5000 руб",
    proc: 7.7,
  },
  {
    label: "свыше 5000 руб.",
    proc: 6.3,
  },
];

const DounhnutPeoples = () => {
  return (
    <div
      style={{
        width: "100%",
        height: "100%",
      }}
    >
      <Doughnut
        title="Статистика посещений"
        datasetIdKey="year"
        options={{
          plugins: {
            tooltip: {
              callbacks: {
                title: function () {
                  return "Статистика в %";
                },
              },
            },
          },
        }}
        data={{
          labels: stat.map((el) => el.label),
          datasets: [
            {
              label: "# of Votes",
              data: stat.map((el) => el.proc),
              backgroundColor: [
                "rgba(255, 99, 132, 0.2)",
                "rgba(54, 162, 235, 0.2)",
                "rgba(255, 206, 86, 0.2)",
                "rgba(75, 192, 192, 0.2)",
                "rgba(153, 102, 255, 0.2)",
                "rgba(255, 159, 64, 0.2)",
              ],
              borderColor: [
                "rgba(255, 99, 132, 1)",
                "rgba(54, 162, 235, 1)",
                "rgba(255, 206, 86, 1)",
                "rgba(75, 192, 192, 1)",
                "rgba(153, 102, 255, 1)",
                "rgba(255, 159, 64, 1)",
              ],
              borderWidth: 1,
            },
          ],
        }}
      />
    </div>
  );
};

export default DounhnutPeoples;
