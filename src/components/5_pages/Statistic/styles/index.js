import styled from "styled-components";

export const Wrapper = styled.div`
  min-height: 100vh;
  padding-top: 150px;
  background-image: url("./images/palace_holl_white_bgc.jpg");
`;

export const GraphicsWrapper = styled.div`
  width: 400px;
  height: 500px;
`;

export const RawInfo = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 1% 3% 3% 1%;
`;

export const ItemWrapper = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  align-items: center;
  column-gap: 30px;
  border-radius: 10px;
  border: 1px solid lightgray;
  width: 340px;
  padding: 20px 40px;
`;

export const CardWrapper = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  & * {
    font-weight: 700;
    color: "#2b2928";
  }
`;

export const MainContent = styled.div`
  flex-grow: 1;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  padding: 40px 60px;
`;

export const ControlBlock = styled.div`
  transition: all 0.3s;
  z-index: 5;
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  row-gap: 10px;
  background-color: white;
  height: 280px;
  width: 50px;
  display: flex;
  right: 0;
  top: 50%;
  opacity: 0.1;

  transform: translate(0, -50%);

  &:hover {
    opacity: 1;
  }

  & button {
    color: gray;
    background-color: white;
    cursor: pointer;
    border-radius: 9999px;
    border: 1px solid lightgray;
    padding: 5px;
    display: flex;
    align-items: center;
    justify-content: center;
    height: 35px;
    width: 35px;
    & .red {
      color: red;
    }
    & .green {
      color: green;
    }
  }
`;

export const PageTextWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  column-gap: 80px;
`;

export const InfoCard = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  row-gap: 4;
  column-gap: 4;
  border: 1px solid gray;
  border-radius: 10px;
  padding: 5px 15px;
  width: 220px;
  height: 100px;
  justify-content: space-between;
  & svg {
    height: 40px;
    color: gray;
    margin-right: 15px;
  }
  & .info_wrap {
    display: flex;
    flex-direction: column;
  }
  & .title {
    font-size: 14px;
    line-height: 20px;
    font-weight: 400;
  }
  & .count {
    font-size: 24px;
  }
`;
