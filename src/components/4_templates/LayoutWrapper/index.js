import React, { useState, useLayoutEffect } from "react";
import Footer from "../Footer";
import Body from "../Body";
import { Wrapper, MainSpaceContainer } from "./styles";
import dynamic from "next/dynamic";

const Header = dynamic(() => import("../Header"));

const LayoutWrapper = ({ children }) => {
  const [c, cc] = useState(0);

  useLayoutEffect(() => {
    setTimeout(() => {
      cc(c + 1);
    }, 1);
  }, []);

  if (c === 0) {
    return null;
  }

  return (
    <Wrapper>
      <MainSpaceContainer>
        <Header />
        <MainSpaceContainer>
          <Body>{children}</Body>
          <Footer />
        </MainSpaceContainer>
      </MainSpaceContainer>
    </Wrapper>
  );
};

export default LayoutWrapper;
