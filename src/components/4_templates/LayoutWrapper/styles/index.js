/* eslint-disable import/prefer-default-export */
import styled from "styled-components";
import { lighten } from "polished";

export const Wrapper = styled.div`
  display: flex;
  width: 100vw;
  background-color: #e8e8e8;
  transition: all 0.3s;
`;

export const WorkSpaceContainer = styled.div`
  display: flex;
  flex-direction: column;
  transition: all 0.3s;
`;

export const MainSpaceContainer = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  transition: all 0.3s;
  overflow: auto;
  position: relative;
`;
