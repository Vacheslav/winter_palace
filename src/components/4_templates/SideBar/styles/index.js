import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  padding: ${({ theme }) => theme?.padding};
  width: 200px;
  min-width: 200px;
  background-color: ${({ theme }) => theme?.backgroundColor};
  box-shadow: ${({ theme }) => theme?.shadow};
  z-index: ${({ theme }) => theme?.zIndex};
  border-right: 1px solid lightgray;
  @media only screen and (max-width: 3000px) {
    top: 61px;
    position: fixed;
    opacity: 0;
    left: -250px;
    transition: all 0.3s;
    ${({ menuIsActive }) =>
      menuIsActive
        ? `
      opacity: 1;
      left: 0;
    `
        : ""}
    & .sidebar-logo {
      display: none;
    }
  }
`;
