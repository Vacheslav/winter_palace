import React from "react";
import { cond, T, equals } from "ramda";
import { Logo, Settings, SideBarMenu } from "../Components";
import { randKey } from "@/utils/index";

const renderItem = (item, opts) => {
  const { name } = item;

  return cond([
    [
      equals("logo"),
      () => (
        <Logo {...opts} key={randKey()} {...item}>
          logo
        </Logo>
      ),
    ],
    [
      equals("settings"),
      () => (
        <Settings key={randKey()} {...item}>
          footer
        </Settings>
      ),
    ],
    [
      equals("links"),
      () => (
        <SideBarMenu key={randKey()} {...item}>
          SideBarMenu
        </SideBarMenu>
      ),
    ],
    [T, () => null],
  ])(name);
};

const renderSideBarItems = (renderList = [], opts) => {
  return (
    <>
      {renderList &&
        renderList.map((el) => (
          <React.Fragment key={randKey()}>
            {renderItem(el, opts)}
          </React.Fragment>
        ))}
    </>
  );
};

export default renderSideBarItems;
