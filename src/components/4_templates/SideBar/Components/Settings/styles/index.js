import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  height: 50px;
  width: 100%;
  transition: all 0.5s;
  & i {
    color: darkgray;
  }
  &:hover {
    transform: rotate(90deg);
    & i {
      color: gray;
    }
  }
`;
