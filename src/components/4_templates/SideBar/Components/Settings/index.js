import React from "react";
import { Wrapper } from "./styles";

const Settings = () => {
  return (
    <Wrapper>
      <i className="fal fa-cog"></i>
    </Wrapper>
  );
};

export default Settings;
