import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px 20px;
  & h2 {
    font-size: 0;
    margin-bottom: 15px;
    color: #686868;
    &::before {
      content: "";
      display: block;
      width: 100%;
      height: 3px;
      background-color: #686868;
      border-radius: 3px;
      opacity: 0.8;
    }
    &.menu-opened {
      font-size: 1em;
      font-weight: bold;
      color: #4e387b;
      &::before {
        display: none;
      }
    }
  }
`;
