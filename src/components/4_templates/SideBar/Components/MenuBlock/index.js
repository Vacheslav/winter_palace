import React from "react";
import { Wrapper } from "./styles";
import SmartLinkItem from "../SmartLinkItem";
import { connectStores } from "@/store/helper";
import { randKey } from "@/utils/index";
import { path } from "ramda";

const checkAccess = (pRoles = [], access) => pRoles && pRoles.includes(access);

const MenuBlock = ({ access, links, title, store }) => {
  const roles = path(["user", "me", "roles"], store);

  //TODO получить из стора
  const { isOpenedMenu } = true;

  const pRoles = roles && roles.reduce((acc, { slug }) => [...acc, slug], []);

  if (!checkAccess(pRoles, access)) return null;
  return (
    <Wrapper>
      <h2 className={"menu-opened"}>{title}</h2>
      {links.map((el) => (
        <SmartLinkItem key={randKey()} {...el} />
      ))}
    </Wrapper>
  );
};

export default connectStores(MenuBlock);
