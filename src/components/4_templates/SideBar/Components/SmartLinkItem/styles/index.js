import styled from "styled-components";

export const Wrapper = styled.ul`
  padding: 0;
  margin-bottom: 15px;
  & .buck-menu-link {
    color: rgba(255, 255, 255, 0.7);
    &:hover {
      color: white;
      background: rgba(255, 255, 255, 0.1);
    }
    &.active {
      background: rgba(255, 255, 255, 0.1);
    }
  }
  &.buck-menu-container {
    &.visible .buck-menu-item {
      & > .buck-menu-link {
        color: rgba(255, 255, 255, 0.6);
      }
      &.active {
        background: rgba(255, 255, 255, 0.1);
        color: rgba(255, 255, 255, 0.8);
      }
      &:hover {
        color: white;
      }
    }
    &::before {
      background: rgba(255, 255, 255, 0.3);
    }
  }
`;
