import React from "react";
import { Wrapper } from "./styles";
import { Link, Links } from "./components";

const SmartLinkItem = (props) => {
  const { content } = props;

  return (
    <Wrapper>{content ? <Links {...props} /> : <Link {...props} />}</Wrapper>
  );
};

export default SmartLinkItem;
