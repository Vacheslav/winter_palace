import React from "react";
import { randKey } from "@/utils/index";

const ToggleContainer = ({ content }) => {
  return (
    <Ul>
      {content.map((link) => (
        <Link key={randKey()} {...link} />
      ))}
    </Ul>
  );
};

export default ToggleContainer;
