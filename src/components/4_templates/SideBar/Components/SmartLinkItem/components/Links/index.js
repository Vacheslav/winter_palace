import { useState } from "react";
import Link from "../Link";
import { randKey } from "@/utils/index";
import { Wrapper, Ul, Head } from "./styles";

const Links = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const { content, label, icon } = props;

  const handleToggle = () => setIsOpen(!isOpen);

  return (
    <Wrapper>
      <Head onClick={handleToggle}>
        <Link {...props} className="mr1" />
        <i
          style={{
            transform: `rotate(${!isOpen ? "60" : "0"}deg)`,
          }}
          className="fas fa-triangle"
        />
      </Head>
      <Ul isOpen={!isOpen}>
        {content.map((link) => (
          <Link key={randKey()} {...link} containerClass="link-row" />
        ))}
      </Ul>
    </Wrapper>
  );
};

export default Links;
