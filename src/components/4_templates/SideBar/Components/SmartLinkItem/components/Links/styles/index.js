import styled from "styled-components";

export const Wrapper = styled.div`
  & h3 {
    font-size: 15px;
    color: gray;
    & i {
      margin-right: 5px;
    }
  }
  padding: 0 !important;
`;

export const Ul = styled.ul`
  margin-top: 10px;
  padding-top: 10px;
  padding-bottom: 10px;
  border-left: 2px solid lightgray;
  transition: all 0.3s;
  font-size: 12px;
  ${({ isOpen }) =>
    isOpen &&
    `
    margin: 0;
    padding: 0;
    opacity: 0;
    font-size: 0px;
    pointer-events: none;
  `}

  & .li-link-row, & * .li-link-row {
    ${({ isOpen }) =>
      !isOpen
        ? `
      margin-bottom: 10px;
      &:last-child {
        margin-bottom: 00px;
      }
    
    `
        : `
    position: absolute;
    z-index: -1;
    left: -200px;
    pointer-events: none;
    `}
  }
`;

export const Head = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  & i.fa-triangle {
    color: #454541;
    font-size: 8px;
    transition: all 0.3s;
  }
`;
