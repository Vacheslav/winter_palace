import styled from "styled-components";

export const Li = styled.li`
  padding: 0;
  & * .link,
  & .link {
    color: #454541;
    display: flex;
    text-decoration: none;
    align-items: center;
    font-size: 14px;
    & .icon {
      display: flex;
      justify-content: center;
      min-width: 20px;
      margin-right: 10px;
    }
    &:hover {
      color: #373978;
    }
  }
`;
