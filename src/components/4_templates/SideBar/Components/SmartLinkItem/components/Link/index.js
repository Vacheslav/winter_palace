import { Li } from "./styles";
import NextLink from "next/link";
import T from "prop-types";

const Link = ({ icon, label, to, containerClass, className = "" }) => {
  const pClass = `link ${className}`;

  if (!to)
    return (
      <Li className="li-link-row">
        <div className={pClass}>
          <span className="icon">{icon ? icon : null}</span>
          <span>{label}</span>
        </div>
      </Li>
    );

  return (
    <Li className="li-link-row">
      <NextLink href={to}>
        <a className={pClass} href={to}>
          <span className="icon">{icon ? icon : null}</span>
          <span
            style={{
              color: !icon && "gray",
            }}
          >
            {label}
          </span>
        </a>
      </NextLink>
    </Li>
  );
};

Link.propTypes = {
  className: T.string,
  containerClass: T.string,
};

Link.defaultProps = {
  className: "",
  containerClass: "",
};

export default Link;
