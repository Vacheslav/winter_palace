import React from "react";
import { Wrapper, MenuWrapper } from "./styles";
import MenuBlock from "../MenuBlock";
import { isArr } from "@/utils/index";
import { randKey } from "@/utils/index";

const SideBarMenu = ({ menu }) => {
  return (
    <Wrapper>
      <MenuWrapper>
        {isArr(menu) && menu.map((el) => <MenuBlock key={randKey()} {...el} />)}
      </MenuWrapper>
    </Wrapper>
  );
};

export default SideBarMenu;
