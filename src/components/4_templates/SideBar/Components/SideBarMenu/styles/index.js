import styled from "styled-components";

export const Wrapper = styled.div`
  padding-top: 2vh;
  display: flex;
  flex-direction: column;
  flex-grow: 1;
`;

export const MenuWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;
