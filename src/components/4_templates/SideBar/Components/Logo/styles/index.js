import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: calc(${({ theme }) => theme?.headerHeight} + 1px);
  padding-left: ${({ theme }) => theme?.sidebarPaddingLeft};
  border-bottom: 1px solid lightgray;
  padding: 0 20px;

  & .burger {
    display: none;
    @media only screen and (max-width: 800px) {
      display: flex;
    }
  }
  & h1 {
    margin: 0;
    font-weight: 600;
    font-size: 24px;
    color: #3f3d4a;
  }
  * {
    display: flex;
    align-items: center;
  }
`;
