import React from "react";
import { Wrapper } from "./styles";
import T from "prop-types";
import Image from "next/image";
import { useTheme } from "@/themes/index";
import { randKey } from "src/utils";

const Logo = ({ src, title }) => {
  const theme = useTheme({ type: "SidebarLogo" });

  return (
    <Wrapper className="sidebar-logo" key={randKey()} theme={theme}>
      <a href="/">
        <div className="mr1">{src && <Image width={25} src={src} />}</div>
        <h1>{title}</h1>
      </a>
    </Wrapper>
  );
};

Logo.propTypes = {
  name: T.string,
  title: T.string,
  src: T.oneOfType([T.string, () => null]),
};

Logo.defaultProps = {
  name: "",
  title: "",
  src: "",
};

export default Logo;
