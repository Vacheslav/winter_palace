import React, { useState } from "react";
import { Wrapper } from "./styles";
import { AlternativeWrapper } from "@/wrappers/index";
import T from "prop-types";
import { isArr } from "@/utils/index";
import { renderSideBarItems } from "./funcs";
import { useTheme } from "@/themes/index";
import { connectStore } from "@/store/index";

const SideBar = ({ renderList, theme: themeStore }) => {
  const theme = useTheme({ type: "Sidebar" });

  const { menuIsActive } = themeStore;

  const [isActive, setIsActive] = useState(false);

  return (
    <AlternativeWrapper alternative={null} isAvaliable={isArr(renderList)}>
      <Wrapper menuIsActive={menuIsActive} theme={theme}>
        {renderSideBarItems(renderList, {
          isActive,
          setIsActive,
        })}
      </Wrapper>
    </AlternativeWrapper>
  );
};

SideBar.propTypes = {
  renderList: T.array,
};

SideBar.defaultProps = {
  renderList: [],
};

export default connectStore(SideBar, "theme");
