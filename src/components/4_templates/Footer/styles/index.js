/* eslint-disable import/prefer-default-export */
import styled from "styled-components";

export const TopFooter = styled.div`
  padding: 0 0 50px;
  border-bottom: 1px solid #403e36;
  & .row {
    display: flex;
    column-gap: 20px;
  }
`;

export const Wrapper = styled.div`
  & .footer {
    background-color: #403e36;
    padding: 90px 105px 25px 105px;
    color: #d6d9db;
    .footer-logo a {
      font-size: 1.5rem;
      color: #ffffff;
      opacity: 0.75;
    }
    .tagline {
      font-size: 0.875rem;
      margin-left: 10px;
      padding: 5px 0 15px;
      color: #fff;
      opacity: 0.5;
    }
    h4 {
      font-size: 1.125rem;
      font-weight: bold;
      color: #d6d9db;
      line-height: 22px;
      margin: 14px 0 0;
      padding: 0 0 25px;
    }
    & .footer-link {
      margin: 0;
      padding: 0;
      list-style: none;
      font-size: 16px;
      font-weight: normal;
      & a {
        text-decoration: none;
        color: #fff;
        opacity: 0.75;
        display: block;
        padding-bottom: 20px;
        cursor: pointer;
      }
    }
    & .social-icons {
      display: inline-block !important;
      padding-bottom: 8px !important;
      margin-right: 15px;
    }
    & .bottom-footer {
      padding: 30px 0 0;
      & .copyright {
        font-size: 1.125rem;
        font-weight: 600;
        margin: 0;
        padding: 0;
        color: #fff;
        opacity: 0.75;
      }
      & .scc-footer-logo a {
        font-size: 1.5rem;
        color: #ffffff;
        margin-bottom: 15px;
        display: block;
      }
    }
  }
`;
