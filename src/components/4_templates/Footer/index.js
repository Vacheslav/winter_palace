import React from "react";
import { Wrapper, TopFooter } from "./styles";
import { useRouter } from "next/router";
import Link from "next/link";
import { Logo } from "@/atoms/index";

const Footer = () => {
  const router = useRouter();

  if (router.pathname === "/model") return null;

  return (
    <Wrapper>
      <footer className="footer">
        <TopFooter>
          <div className="row">
            <div
              style={{
                marginRight: 70,
              }}
            >
              <div className="footer-logo">
                <a
                  style={{
                    marginBottom: 10,
                    display: "flex",
                    columnGap: 20,
                    alignItems: "center",
                    justifyContent: "center",
                    fontSize: 32,
                    fontWeight: 300,
                  }}
                  href="/"
                  title="Mercury"
                >
                  <Logo logoType="white" color="white" size={70} />
                  <span>Зимний дворец</span>
                </a>
                <p className="tagline">Главный императорский дворец России</p>
              </div>
            </div>
            <div
              style={{
                display: "flex",
                columnGap: 40,
                marginTop: 13,
              }}
            >
              <div>
                <h4>Карта</h4>
                <ul className="footer-link">
                  <li>
                    <Link href="/" title="Home">
                      Главная
                    </Link>
                  </li>
                  <li>
                    <Link href="/history" title="Home">
                      История
                    </Link>
                  </li>
                  <li>
                    <Link href="/model" title="Home">
                      3D модель
                    </Link>
                  </li>
                </ul>
              </div>
              <div>
                <h4>Связаться с нами</h4>
                <ul className="footer-link">
                  <li>
                    <Link href="/contacts" title="Faq">
                      Контакты
                    </Link>
                  </li>
                </ul>
              </div>
              <div>
                <h4>Наши контакты</h4>
                <ul className="footer-link">
                  <li>
                    <a href="mail-to:smartlight.eip@gmail.com" title="Contact">
                      press@hermitage.ru
                    </a>
                  </li>
                  <li>
                    <div
                      style={{
                        display: "flex",
                        columnGap: 10,
                      }}
                      className="icons"
                    >
                      <i class="fab fa-facebook" />
                      <i class="fab fa-vk"></i>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </TopFooter>
        <div className="bottom-footer">
          <div className="row">
            <div className="col-md-5">
              <p className="copyright pt-3">
                Copyright &copy; 2022 Winter Palace
              </p>
            </div>
          </div>
        </div>
      </footer>
    </Wrapper>
  );
};

export default Footer;
