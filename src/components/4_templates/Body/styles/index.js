/* eslint-disable import/prefer-default-export */
import styled from "styled-components";

export const Wrapper = styled.div`
  flex-grow: 1;
  transition: all 0.3s;
`;
