import React from "react";
import { Wrapper } from "./styles";

const Body = ({ children }) => {
  return <Wrapper className="body_container">{children}</Wrapper>;
};

export default Body;
