/* eslint-disable import/prefer-default-export */
import styled from "styled-components";
import { lighten, darken } from "polished";

export const Wrapper = styled.header`
  top: 0;
  left: 0;
  height: 120px;
  width: 100%;
  padding: 10px 30px;
  display: flex;
  z-index: 3;
  align-items: center;
  justify-content: start;
  column-gap: 5%;
  position: fixed;
  color: "white";

  ${({ isPanel, bgc }) =>
    isPanel &&
    `background-color: rgba(${bgc}); box-shadow: 0px 2px 8px 0px rgba(34, 60, 80, 0.2);`}
  ${({ isPanel }) => isPanel && "height: 100px;"}
  transition: all 2s;
  @media screen and (max-width: 550px) {
    padding-right: 10px;
  }
`;

export const Nav = styled.nav`
  & ul {
    display: flex;
    align-items: center;
    column-gap: 20px;
    margin-bottom: 0;
    & li {
      color: ${({ color }) => color};
      font-weight: 600;
      cursor: pointer;
      & a {
        transition: all 0.3s;
        color: ${({ color }) => color};
        &:hover {
          color: ${({ color }) => lighten(0.2, color)};
        }
      }
      &.active a {
        color: ${({ color }) => darken(0.2, color)};
      }
    }
  }
`;
