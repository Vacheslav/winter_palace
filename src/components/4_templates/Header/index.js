import React, { useEffect, useLayoutEffect } from "react";
import { Wrapper, Nav } from "./styles";
import { Logo } from "@/atoms/index";
import Link from "next/link";
import { useRouter } from "next/router";
import { cond, T, equals } from "ramda";
import { useScroll } from "@/hooks/index";

const Header = () => {
  const router = useRouter();
  const { y } = useScroll();

  const opt = cond([
    [
      equals("/"),
      () => {
        if (document.documentElement.clientHeight < y)
          return { logoType: "black", color: "gray", bgc: "255,255,255, 90%" };
        return { logoType: "yellow", color: "#fff3d3", bgc: "0,0,0, 90%" };
      },
    ],
    [
      equals("/model"),
      () => ({ logoType: "yellow", color: "#fff3d3", bgc: "0,0,0, 90%" }),
    ],
    [
      equals("/history"),
      () => ({ logoType: "yellow", color: "#fff3d3", bgc: "0,0,0, 90%" }),
    ],
    [T, () => ({ logoType: "black", color: "gray", bgc: "255,255,255, 90%" })],
  ])(router.pathname);

  const isActive = (path) => (path === router.pathname ? "active" : "");

  return (
    <Wrapper isPanel={y} {...opt}>
      <a href="/">
        <Logo {...opt} size={y ? 70 : 90} />
      </a>
      <Nav {...opt}>
        <ul>
          <li className={isActive("/")}>
            <Link href="/">Главная</Link>
          </li>
          <li className={isActive("/history")}>
            <Link href="/history">История</Link>{" "}
          </li>
          <li className={isActive("/statistic")}>
            <Link href="/statistic">Статистика</Link>{" "}
          </li>
          <li className={isActive("/chronicle")}>
            <Link href="/chronicle">Хроника</Link>{" "}
          </li>
          <li className={isActive("/model")}>
            <Link href="/model">3D модель</Link>{" "}
          </li>
          <li className={isActive("/contacts")}>
            <Link href="/contacts">Контакты</Link>
          </li>
        </ul>
      </Nav>
    </Wrapper>
  );
};

export default Header;
