import React from "react";
import { cond, T, equals } from "ramda";
import { Bell, LocaleSwitcher, User } from "../Components";
import { Devider } from "@/atoms/index";
import { randKey } from "@/utils/index";
import { Space } from "@/wrappers/index";

const renderItem = (item) => {
  const { name } = item;

  return cond([
    [equals("devider"), () => <Devider {...item} direction="vertical" />],
    [equals("bell"), () => <Bell {...item} />],
    [equals("user"), () => <User {...item} />],
    [equals("locale"), () => <LocaleSwitcher {...item} />],
    [T, () => null],
  ])(name);
};

const renderItems = (renderList = []) => {
  console.log(renderList);
  return (
    <Space>
      {renderList &&
        renderList.map((item) => renderItem({ ...item, key: randKey() }))}
    </Space>
  );
};

export default renderItems;
