import React from "react";
import { Wrapper, LogoWrapper } from "./styles";
import T from "prop-types";
import Image from "next/image";
import { useTheme } from "@/themes/index";
import { randKey } from "src/utils";
import { BurgerMenu } from "@/atoms/index";
import logo from "@/images/logo.svg";
import { connectStore } from "@/store/index";
import { Divider } from "antd";

const Logo = ({ theme: themeStore }) => {
  const { menuIsActive, toggleMenu } = themeStore;

  const theme = useTheme({ type: "SidebarLogo" });

  return (
    <Wrapper key={randKey()} theme={theme}>
      <BurgerMenu
        className="burger"
        isActive={menuIsActive}
        onClick={toggleMenu}
      />
      <Divider
        type="vertical"
        style={{
          height: 30,
        }}
      />
      <LogoWrapper href="/">
        <div
          style={{
            width: 25,
            height: 25,
          }}
        >
          {logo && <Image width={25} src={logo} />}
        </div>
        <h1>Садко</h1>
      </LogoWrapper>
    </Wrapper>
  );
};

Logo.propTypes = {
  name: T.string,
  title: T.string,
  src: T.oneOfType([T.string, () => null]),
};

Logo.defaultProps = {
  name: "",
  title: "",
  src: "",
};

export default connectStore(Logo, "theme");
