import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  max-width: 120px;
  height: calc(${({ theme }) => theme?.headerHeight} + 1px);
  padding-left: ${({ theme }) => theme?.sidebarPaddingLeft};
  border-bottom: 1px solid lightgray;
  padding: 0 20px;
  column-gap: 10px;
  @media only screen and (max-width: 550px) {
    padding: 10px;
  }
  & h1 {
    margin: 0;
    font-weight: 600;
    font-size: 22px;
    color: #3f3d4a;
  }
  * {
    display: flex;
    align-items: center;
  }
`;

export const LogoWrapper = styled.a`
  display: flex;
  column-gap: 5;
  transition: all 0.3s;
  @media only screen and (max-width: 550px) {
    transform: scale(0.8) translate(-20px);
  }
`;
