import styled from "styled-components";

export const Wrapper = styled.div`
  & .icon-lang {
    width: 30px;
    height: 30px;
    z-index: 4;
    overflow: hidden;
    margin: 0 auto;
    border-radius: 50%;
    & > img {
      border-radius: 50%;
      position: relative;
      top: 50%;
      left: 50%;
      margin: -27px 0 0 -30px;
    }
  }

  .dropdown-menu {
    max-width: 95%;
    min-width: 400px;
    padding-top: 0;
    & > .dropdown-title {
      background-color: #d5d5d5;
    }
  }

  & .dropdown-xl {
    max-width: 95%;
    min-width: 400px;
  }
  .dropdown-title {
    text-align: center;
  }
  .dropdown-menu {
    margin-top: 5px;
  }
`;

export const IconWrapper = styled.div`
  position: relative;
  width: 40px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const IconLang = styled.div`
  width: 30px;
  height: 30px;
  z-index: 4;
  overflow: hidden;
  margin: 0 !important;
  border-radius: 50%;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  & > img {
    border-radius: 50%;
    top: 0%;
    left: 20%;
  }
`;

export const IconWrapperBg = styled.div`
  opacity: 0.1;
  transition: opacity 0.2s;
  border-radius: 50%;
  width: 100%;
  height: 100%;
  background-color: #6c757d !important;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  position: absolute;
  & > svg {
    margin: 0 auto;
  }
`;

export const DropdownMenuHeader = styled.div`
  height: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const DropdownMenuHeaderInner = styled.div`
  margin: 0;

  & div.colored {
    background-color: #686868;
    width: 100%;
    height: 60px;
    top: 0;
    left: 0;
    margin: 0;
    position: absolute;
  }
  & div.header {
    position: relative;
    padding: 10px 0;
    text-align: center;
    display: flex;
    justify-content: center;
    width: 100%;
    & h6.subtitle {
      text-align: center;
      width: 100%;
      display: flex;
      justify-content: center;
      margin: 0;
      font-weight: 600;
    }
  }
`;
