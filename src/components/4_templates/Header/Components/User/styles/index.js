/* eslint-disable import/prefer-default-export */
import styled from "styled-components";

export const Wrapper = styled.div`
  position: relative;
  & .control {
    display: flex;
    align-items: center;
    cursor: pointer;
    & .name {
      margin-right: 7px;
      @media screen and (max-width: 380px) {
        font-size: 11px;
      }
    }
    & i {
      color: gray;
      font-size: 8px;
      transition: all 0.3s;
    }
  }
`;

export const DropContainer = styled.div`
  padding: 10px;
  cursor: pointer;
  &:hover {
    background-color: lightgray;
  }
`;
