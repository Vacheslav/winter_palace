import React, { useState } from "react";
import { Wrapper, DropContainer } from "./styles";
import { Drop } from "@/atoms/index";
import { dropSession } from "@/utils/index";
import { connectStores } from "@/store/index";
import { path } from "ramda";

const User = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const me = path(["store", "user", "me"], props);

  const firstName = path(["firstName"], me);
  const surname = path(["surname"], me);

  const handleToggle = () => setIsOpen(!isOpen);

  const handleLogout = () => {
    dropSession();
    handleToggle();
    window.location.href = "/auth";
  };

  return (
    <Wrapper>
      <div className="control" onClick={handleToggle}>
        <span className="name">{`${firstName || ""} ${surname || ""}`}</span>
        <i
          style={{
            transform: `rotate(${!isOpen ? "60" : "0"}deg)`,
            marginBottom: !isOpen ? 4 : 0,
          }}
          className="fas fa-triangle"
        ></i>
      </div>
      <Drop className="dropdown" isActive={isOpen}>
        <DropContainer onClick={handleLogout}>
          <div>Выйти</div>
        </DropContainer>
      </Drop>
    </Wrapper>
  );
};

export default connectStores(User);
