import React from "react";
import Link from "next/link";
import { NotificationCountHelper, Wrapper } from "./styles";

const getChatLink = () => "/";

const Bell = () => {
  const isNotifications = false;

  return (
    <Wrapper>
      <Link href={getChatLink()}>
        <div className="icon-wrapper ">
          <i style={{ color: "black", fontSize: 14 }} className="fas fa-bell" />
          <div className={`icon-wrapper-bg bg-danger`} />
          {isNotifications ? (
            <NotificationCountHelper>
              {notificationsCount}
            </NotificationCountHelper>
          ) : null}
        </div>
      </Link>
    </Wrapper>
  );
};

export default Bell;
