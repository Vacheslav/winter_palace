import { lighten } from "polished";
import styled from "styled-components";

const NotificationCountHelper = styled.div`
  color: white;
  background: lightgray;
  position: absolute;
  top: 2px;
  right: 2px;
  font-size: 7px;
  border-radius: 50%;
  padding: 2px;
  width: 15px;
  height: 15px;
`;

const Wrapper = styled.div`
  display: flex;
  height: 35px;
  width: 35px;
  align-items: center;
  justify-content: center;
  border-radius: 100%;
  cursor: pointer;
  background-color: lightgray;
  opacity: 0.5;
  transition: all 0.3s;
  &:hover {
    opacity: 0.7;
  }
`;

export { NotificationCountHelper, Wrapper };
