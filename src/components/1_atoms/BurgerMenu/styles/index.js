import styled from "styled-components";

export const Wrapper = styled.div`
  position: relative;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  transition: all 0.3s;
  & div {
    height: 2px;
    width: 25px;
    border-radius: 100px;
    background-color: black;
    margin-bottom: 5px;
    transition: all 0.3s;
    &:nth-child(3) {
      margin-bottom: 0;
    }
  }
  &.active {
    div {
      &:nth-child(1) {
        transform: rotate(45deg) translate(5px, 5px);
      }
      &:nth-child(2) {
        transform: rotate(160deg);
        opacity: 0;
      }
      &:nth-child(3) {
        transform: rotate(-45deg) translate(4px, -5px);
      }
    }
  }
`;
