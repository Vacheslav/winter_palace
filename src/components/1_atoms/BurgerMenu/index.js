import { Wrapper } from "./styles";

export default ({ isActive, className = "", ...restProps }) => (
  <Wrapper
    {...restProps}
    className={isActive ? `${className} active` : `${className}`}
  >
    <div></div>
    <div></div>
    <div></div>
  </Wrapper>
);
