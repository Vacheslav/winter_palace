import React from "react";
import { Switch } from "antd";
import T from "prop-types";
import { SwitchWrapper } from "./styles";

const ViewSwitcher = ({
  isActive,
  setIsActive,
  className,
  leftTitle = "",
  rightTitle = "",
}) => {
  const handleToggle = () => setIsActive(!isActive);

  return (
    <div className={`flex pointer ${className}`}>
      <span
        style={{
          color: !isActive && "gray",
        }}
        onClick={() => {
          !isActive && setIsActive(true);
        }}
        className="mr1"
      >
        {leftTitle}
      </span>
      <SwitchWrapper>
        <Switch checked={!isActive} onClick={handleToggle} />
      </SwitchWrapper>
      <span
        style={{
          color: isActive && "gray",
        }}
        onClick={() => {
          isActive && setIsActive(false);
        }}
      >
        {rightTitle}
      </span>
    </div>
  );
};

ViewSwitcher.propTypes = {
  isActive: T.bool,
  setIsActive: T.func,
  className: T.string,
};

ViewSwitcher.defaultProps = {
  isActive: false,
  setIsActive: () => {},
  className: "",
};

export default ViewSwitcher;
