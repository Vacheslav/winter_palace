import styled from "styled-components";

export const Wrapper = styled.div`
  background-color: lightgray;

  ${({ direction }) =>
    direction === "vertical"
      ? `
height: 100%;
width: 1px;
`
      : `
height: 1px;
width: 100%;  
`}
`;
