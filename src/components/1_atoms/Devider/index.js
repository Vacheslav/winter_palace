import React from "react";
import { Wrapper } from "./styles";
import T from "prop-types";

const Devider = ({ ...restProps }) => {
  return <Wrapper {...restProps} />;
};

Devider.propTypes = {
  direction: T.oneOf(["vertical", "horizontal"]),
};

Devider.defaultProps = {
  direction: "horizontal",
};

export default Devider;
