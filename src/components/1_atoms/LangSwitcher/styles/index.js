import styled from "styled-components";

const dfcc = `
display: flex;
justify-content: center;
align-items: center;
`;

const pa = `
position: absolute;
`;

const pac = `
top: 50%;
left: 50%;
transform: translate(-50%, -50%);
${pa}
`;

const patl = `
top: 0;
left: 0;
${pa}
`;

export const Wrapper = styled.div`
  & .icon-lang {
    width: 30px;
    height: 30px;
    z-index: 4;
    overflow: hidden;
    margin: 0 auto;
    border-radius: 50%;
    & > img {
      border-radius: 50%;
      position: relative;
      top: 50%;
      left: 50%;
      margin: -27px 0 0 -30px;
    }
  }

  .dropdown-menu {
    max-width: 95%;
    min-width: 400px;
    padding-top: 0;
    & > .dropdown-title {
      background-color: #d5d5d5;
    }
  }

  & .dropdown-xl {
    max-width: 95%;
    min-width: 400px;
  }
  .dropdown-title {
    text-align: center;
  }
  .dropdown-menu {
    margin-top: 5px;
  }
`;

export const IconWrapper = styled.div`
  position: relative;
  width: 40px;
  height: 40px;
  ${dfcc}
`;

export const IconLang = styled.div`
  width: 30px;
  height: 30px;
  z-index: 4;
  overflow: hidden;
  margin: 0 auto;
  border-radius: 50%;
  & > img {
    border-radius: 50%;
    position: relative;
    top: 50%;
    left: 50%;
    margin: -27px 0 0 -30px;
  }
`;

export const IconWrapperBg = styled.div`
  opacity: 0.1;
  transition: opacity 0.2s;
  border-radius: 50%;
  width: 100%;
  height: 100%;
  background-color: #6c757d !important;
  ${pac}
  &>svg {
    margin: 0 auto;
  }
`;

export const DropdownMenuHeader = styled.div`
  height: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const DropdownMenuHeaderInner = styled.div`
  & div.colored {
    background-color: #686868;
    width: 100%;
    height: 60px;
    ${patl}
  }
  & div.header {
    position: relative;
    padding: 10px 0;
    & h6.subtitle {
      margin: 0;
      font-weight: 600;
    }
  }
`;
