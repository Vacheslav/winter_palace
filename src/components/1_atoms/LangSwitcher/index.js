import React from "react";
//@ts-ignore
import Flag from "react-flagkit";
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { randKey } from "@/utils/index";
import {
  IconWrapper,
  IconLang,
  DropdownMenuHeader,
  DropdownMenuHeaderInner,
  IconWrapperBg,
  Wrapper,
} from "./styles";

const LangSwitcher = () => {
  //@ts-ignore

  const availibleLangs = [
    { short: "ru-RU", long: "Русский" },
    // { short: 'de-DE', long: 'Deutsch' },
    // { short: 'en-US', long: 'English' }
  ];

  return (
    <Wrapper>
      <UncontrolledDropdown>
        <DropdownToggle color="link">
          <IconWrapper>
            <IconLang>
              <Flag country={"RU"} size={42} />
            </IconLang>
            <IconWrapperBg />
          </IconWrapper>
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownMenuHeader>
            <DropdownMenuHeaderInner>
              <div className="colored" />
              <div className="header">
                <h6 className="subtitle">Выберите язык</h6>
              </div>
            </DropdownMenuHeaderInner>
          </DropdownMenuHeader>
          <DropdownItem header>Доступные языки</DropdownItem>
          {availibleLangs.map((el) => {
            const title = el.short.split("-")[1];
            return (
              <DropdownItem key={randKey()} onClick={() => null}>
                <Flag className="mr-3 opacity-8" country={title} />
                {el.long}
              </DropdownItem>
            );
          })}
        </DropdownMenu>
      </UncontrolledDropdown>
    </Wrapper>
  );
};

export default LangSwitcher;
