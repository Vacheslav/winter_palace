import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  padding: 10px;
  border-bottom: 1px solid lightgray;
  & h1 {
    font-size: 24px;
    margin: 0;
  }
`;
