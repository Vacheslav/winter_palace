import React from 'react'
import { Select, Tooltip } from 'antd'
import { is } from 'ramda'
import { randKey } from '@/utils/index'
import T from 'prop-types'

const { Option } = Select

const SelectorFromList = ({ list, value, onChange, title, ...restProps }) => {
  if (!is(Array, list)) return null

  return (
    <Tooltip title={title}>
      <Select
        style={{ minWidth: 150 }}
        onChange={onChange}
        value={value}
        showSearch
        allowClear
        filterOption={(input, option) =>
          `${option.children}`.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
        optionFilterProp="children"
        {...restProps}
      >
        {list.map((el) => (
          <Option key={randKey()} value={el}>
            {el}
          </Option>
        ))}
      </Select>
    </Tooltip>
  )
}

SelectorFromList.propTypes = {
  list: T.oneOfType([T.array, () => null]),
  title: T.oneOfType([T.string, () => null]),
  value: T.any,
  onChange: T.func,
}

SelectorFromList.defaultProps = {
  list: null,
  title: null,
  value: null,
  onChange: () => {},
}

export default SelectorFromList
