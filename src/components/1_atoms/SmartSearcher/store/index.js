import React from "react";
import { cond, T, equals } from "ramda";
import { prepReducer, prepEventes } from "@/utils/index";
import { filterList } from "../funcs";

/**
 * @typedef InitSate
 * @type {object}
 * @param {string[]} filterOrder - порядок фильтрации массива
 * @param {Array} values - значения селектов
 * @param {Array} dataSource - начальные данные
 * @param {Array} filtredLists - значения селектов
 * @param {Function} externalUpdateFilterListFunction - функция для обновления внешнего фильтрованного массива
 */

/** @type {InitSate} */
const initialState = {
  values: {},
  filterOrder: [],
  filtredLists: [],
  dataSource: [],
  externalUpdateFilterListFunction: () => {},
};

const ComponentCtx = React.createContext({ state: initialState });

const reducer = (state, action) => {
  return cond([
    ...prepReducer({ initialState, state, action }),
    [
      equals("CHANGE_VALUE"),
      () => ({
        ...state,
        values: {
          ...state.values,
          [action.payload.key]: action.payload.value,
        },
      }),
    ],
    [
      equals("UPDATE_FILTER_ORDER"),
      () => {
        const { dataSource, values } = state;
        console.log(values);
        setTimeout(() => {
          state.externalUpdateFilterListFunction(
            filterList({ dataSource, values, filters: action.payload })
          );
        }, 10);
        return {
          ...state,
          filterOrder: action.payload,
        };
      },
    ],
    [T, () => state],
  ])(action.type);
};

const Store = (props) => {
  const [state, dispatch] = React.useReducer(reducer, initialState);

  return (
    <ComponentCtx.Provider
      value={{
        state,
        ...prepEventes(initialState, dispatch),
        cValue: (payload) => dispatch({ type: `CHANGE_VALUE`, payload }),
        updateStoreFilterOrder: (payload) =>
          dispatch({ type: "UPDATE_FILTER_ORDER", payload }),
      }}
    >
      {props.children}
    </ComponentCtx.Provider>
  );
};

export { Store, ComponentCtx };
