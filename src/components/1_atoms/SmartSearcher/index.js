import React, { memo } from "react";
import { Space, LoadingWrapper } from "@/wrappers/index";
import { Select } from "./components";
import { randKey } from "src/utils";
import { withStore } from "@/hoc/index";
import { Store } from "./store";
import { useInitState } from "./hooks";

/**
 * listField
 * @typedef {Object} ListField
 * @property {string} title
 * @property {string} desc
 */

/**
 * SmartSearcher - компонент который автоматически генерирует импуты по которым будет идти фильтрация
 *
 * @category atoms/SmartSearcher
 *
 * @param {Object} props - пропсы
 * @param {ListField[]} props.listFields - список полей по которым будет идти фильтрация
 * @param {boolean} props.isLoading - список полей по которым будет идти фильтрация
 * @param {Array} props.dataSource - массив с изначальными данными
 * @param {Array} props.filtredList - массив который будет отфильтрован
 * @param {Function} props.cFilteredList - изменить массив который будет отфильтрован
 * @param {Object} props.outerStore - изменить массив который будет отфильтрован
 * @param {Object} props.cOuterStore - изменить массив который будет отфильтрован
 *
 * @example <SmartSearcher columns={[]} />
 *
 * @returns возвращает компонет с инпутами
 */
const SmartSearcher = ({
  listFields,
  dataSource,
  cFilteredList,
  isLoading,
  outerStore,
  cOuterStore,
}) => {
  useInitState({ listFields, cFilteredList, dataSource });
  return (
    <LoadingWrapper
      style={{
        transform: "scale(0.5)",
      }}
      isLoading={isLoading}
    >
      <Space key="1dq32">
        {listFields.map((el_props) => (
          <Select dataSource={dataSource} key={randKey()} {...el_props} />
        ))}
      </Space>
    </LoadingWrapper>
  );
};

export default withStore(Store, memo(SmartSearcher));
