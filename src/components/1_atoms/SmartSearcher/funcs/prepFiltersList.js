/**
 * prepFiltersList - массив который сообщаем порядок фильтрации для текущего импута
 *
 * Если зачение путое, нужно убрать его из списка.
 *
 * Если значение не пустое и его нет в массиве, нужно добавить в конец
 *
 * @category atoms/SmartSearcher
 *
 * @param {Object} props - пропсы
 * @param {string} props.title - ключ поля
 * @param {string} props.value - Значение поля
 * @param {string[]} props.filterOrder - массив с изначальными данными (стор)
 *
 * @returns {string[]} - поряд фильтрации текущего импута
 */

const prepFiltersList = ({ filterOrder, title, value }) => {
  if (!value) return filterOrder;

  const index = filterOrder.findIndex((el) => el === title);
  return filterOrder.slice(0, index);
};

export default prepFiltersList;
