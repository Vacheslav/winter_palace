import { includes, cond, T } from "ramda";

/**
 * filterOrder - в этом порядке происходит фильтрация в селектах
 *
 * Если зачение путое, нужно убрать его из списка.
 *
 * Если значение не пустое и его нет в массиве, нужно добавить в конец
 *
 * @category atoms/SmartSearcher
 *
 * @param {Object} props - пропсы
 * @param {string} props.value - Значение поля
 * @param {string} props.title - ключ поля
 * @param {Function} props.cFilterOrder - функция обновления порядка фильтрации (стор)
 * @param {string[]} props.filterOrder - массив с изначальными данными (стор)
 *
 * @returns void
 */

const updateFilterOrder = ({ value, title, filterOrder, cFilterOrder }) =>
  cond([
    [
      (x) => !includes(x, filterOrder) && value,
      () => {
        cFilterOrder([...filterOrder, title]);
        return "added";
      },
    ],
    [
      (x) => includes(x, filterOrder) && !value,
      () => {
        cFilterOrder(filterOrder.filter((el) => el !== title));
        return "removed";
      },
    ],
    [T, () => null],
  ])(title);

export default updateFilterOrder;
