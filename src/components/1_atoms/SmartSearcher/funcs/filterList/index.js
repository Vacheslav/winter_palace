/**
 * filterList - собирает массив для отображения в селекте
 *
 * @category atoms/SmartSearcher
 *
 * @param {Object} props - пропсы
 * @param {string[]} props.filters - поля фильтрации
 * @param {Array} props.values - Сохраненные значения
 * @param {Array} props.dataSource - массив с изначальными данными
 *
 * @returns {string} - массив значений для селекта
 */

const filterList = ({ filters, dataSource, values }) => {
  if (filters.length === 0) return dataSource;
  return filters.reduce((acc, filterTitle) => {
    return acc.filter((el) => {
      return el[filterTitle] === values[filterTitle];
    });
  }, dataSource);
};

export default filterList;
