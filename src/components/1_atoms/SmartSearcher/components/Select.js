import React, { useContext } from "react";
import { SelectorFromList } from "@/atoms/index";
import { ComponentCtx } from "../store";
import { uniq } from "ramda";
import { updateFilterOrder, prepFiltersList, filterList } from "../funcs";

/**
 * Select - выпадающий список
 *
 * @category atoms/SmartSearcher
 *
 * @param {Object} props - пропсы
 * @param {ListField[]} props.listFields - список полей по которым будет идти фильтрация
 * @param {boolean} props.loading - список полей по которым будет идти фильтрация
 * @param {Array} props.dataSource - массив с изначальными данными
 * @param {Array} props.filtredList - массив который будет отфильтрован
 * @param {Function} props.cFilteredList - изменить массив который будет отфильтрован
 * @property {string} props.title
 * @property {string} props.desc
 *
 * @returns возвращает выпающий лист
 */
const Select = ({ desc, title, dataSource }) => {
  const { state, cValue, updateStoreFilterOrder } = useContext(ComponentCtx);
  const { values, filterOrder } = state;

  const value = values[title];

  const filters = prepFiltersList({ filterOrder, title, value });

  const list = uniq(
    filterList({ filters, dataSource, values }).map((el) => el[title])
  );

  return (
    <SelectorFromList
      loading={false}
      list={list}
      title={desc || title}
      placeholder={desc || title}
      value={value}
      onChange={(value) => {
        cValue({ key: title, value });
        updateFilterOrder({
          value,
          title,
          filterOrder,
          cFilterOrder: updateStoreFilterOrder,
        });
      }}
    />
  );
};

export default Select;
