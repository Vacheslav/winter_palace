import { useEffect, useContext } from "react";
import { ComponentCtx } from "../store";

const useInitState = ({ listFields, cFilteredList, dataSource }) => {
  const { cValues, cExternalUpdateFilterListFunction, cDataSource } =
    useContext(ComponentCtx);

  useEffect(() => {
    cValues(
      listFields.reduce(
        (acc, cur) => ({
          ...acc,
          [cur.title]: null,
        }),
        {}
      )
    );
    cDataSource(dataSource);
    cExternalUpdateFilterListFunction(cFilteredList);
  }, []);

  return true;
};

export default useInitState;
