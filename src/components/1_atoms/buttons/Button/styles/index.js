/* eslint-disable import/prefer-default-export */
import styled from "styled-components";
import { lighten, darken, opacify } from "polished";

export const Btn = styled.button`
  padding: 10px 50px;
  cursor: pointer;
  border-radius: 4px;
  ${({ disabled }) => disabled && "opacity: 0.5;"}
  ${({ disabled }) => disabled && "cursor: not-allowed;"}
  transition: all .3s;
  &.primary {
    ${({ color }) => `background-color: ${color};`}
    ${({ color }) => `border: 1px solid ${lighten(0.1, color)};`}
    color: #1B1A19;
    &:active {
      ${({ color }) => `background-color: ${lighten(0.1, color)};`}
      box-shadow: 0px 0px 0px 12px rgba(34, 60, 80, 0.2);
    }
    &:hover {
      ${({ color }) => `background-color: ${darken(0.1, color)};`}
      ${({ color }) => `border: 1px solid ${darken(0.3, color)};`}
      &:active {
        ${({ color }) => `background-color: ${darken(0.3, color)};`}
      }
    }
  }
  &.reversed {
    background-color: transparent;
    ${({ color }) => `border: 1px solid ${color};`}
    ${({ color }) => `color: ${color};`}
    &:active {
      ${({ color }) => `border: 1px solid ${lighten(0.2, color)};`}
      ${({ color }) => `color: ${lighten(0.2, color)};`}
      box-shadow: 0px 0px 0px 12px rgba(34, 60, 80, 0.2);
    }
    &:hover {
      ${({ color }) => `border: 1px solid ${darken(0.2, color)};`}
      ${({ color }) => `color: ${darken(0.2, color)};`}
    }
  }
`;
