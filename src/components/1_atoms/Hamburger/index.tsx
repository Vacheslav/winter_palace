import React from "react";
//@ts-ignore
import Hamburger from "react-hamburgers";
import styled from "styled-components";
import { elementsColor } from "@/js_styles/index";

const HamburgerBox = styled.div`
  & > button {
    & > span {
      & > span {
        background-color: ${elementsColor};
        &::before {
          background-color: ${elementsColor};
        }
        &::after {
          background-color: ${elementsColor};
        }
      }
    }
  }
`;
const HamburgerBlock = ({
  active,
  changeState,
}: {
  active: boolean;
  changeState: (state: boolean) => void;
}) => {
  return (
    <HamburgerBox
    // onClick={toggleSidebarState}
    >
      <Hamburger
        active={active}
        type="spin-r"
        onClick={() => changeState(!active)}
      />
    </HamburgerBox>
  );
};

export default HamburgerBlock as Hamburger;
