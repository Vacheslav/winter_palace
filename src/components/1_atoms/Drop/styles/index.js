/* eslint-disable import/prefer-default-export */
import styled from "styled-components";

export const Wrapper = styled.div`
  ${({ isActive }) =>
    !isActive
      ? `
   opacity: 0;
   transform: translate(-2px, -200px);
   pointer-events: none;
  `
      : `
  transform: translate(-2px, calc(100% + 15px));
  `}
  position: absolute;
  z-index: 2;
  background-color: white;
  box-shadow: 4px 4px 8px 0px rgba(34, 60, 80, 0.2);
  right: 0;
  bottom: 0;
  transition: all 0.3s;
`;
