import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  font-size: 20px;
  font-weight: 700;
  & .img {
    margin-right: 10px;
    display: flex;
    align-items: center;
  }
  & span {
    font-size: ${({ size }) => `${size - 3}px`};
    color: #2091f4;
  }
`;
