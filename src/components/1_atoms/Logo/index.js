import React from "react";
import { Wrapper } from "./styles";
import logo_black from "@/images/logo_black.png";
import logo_yellow from "@/images/logo_yellow.png";
import logo_white from "@/images/logo_white.png";
import Image from "next/image";

const Logo = ({ size = 100, logoType = "black" }) => {
  const logos = {
    logo_yellow,
    logo_black,
    logo_white,
  };

  const logo = logoType ? logos[`logo_${logoType}`] : logo_yellow;
  return (
    <Wrapper size={size}>
      <Image width={`${size} px`} height={`${size} px`} src={logo} />
    </Wrapper>
  );
};

export default Logo;
