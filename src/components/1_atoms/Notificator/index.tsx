/* eslint-disable react-hooks/exhaustive-deps */
//@ts-nocheck
import React, { useCallback, useEffect, useState } from "react";
import { UncontrolledDropdown, DropdownToggle } from "reactstrap";
import { errorColor, darkGrey } from "@/js_styles/";
import styled from "styled-components";
import Loader from "../Loaders";
import { useMutation, useQuery, useSubscription } from "@apollo/react-hooks";
import { GET_CURRENT_USER } from "@/gql/index";
import { find, propEq, reduce, add } from "ramda";
import { Link } from "react-router-dom";
import {
  GETMU_ALL_ROOMS,
  GET_SUPPORT_ROOM,
  MESSAGE_UPDATED,
  NEW_MESSAGE_ADDED,
} from "@/gql/index";
import { getArrayOfValuesByKey } from "@/utils/index";

export interface iNotificatorProps {
  notifications: any;
  readMessages: () => void;
}

const Notificator = (props: iNotificatorProps) => {
  const {
    data: user,
    loading: uLoading,
    error: uError,
  } = useQuery(GET_CURRENT_USER);
  const isChatAdmin = !!find(propEq("slug", "чат"))(user.me.roles);
  const [unreadMsgsCount, setUnreadMsgsCount] = useState(0);
  const roomSchema = useCallback(() => {
    if (isChatAdmin) {
      return GETMU_ALL_ROOMS;
    }
    return GET_SUPPORT_ROOM;
  }, [isChatAdmin]);
  let notificationsCount = unreadMsgsCount;
  const isNotifications = notificationsCount > 0;
  const [getRoomsList] = useMutation(roomSchema(), {
    onError: () => {},
  });
  const NotificationCountHelper = styled.div`
    color: white;
    background: ${(isNotifications && errorColor) || darkGrey};
    position: absolute;
    top: 2px;
    right: 2px;
    font-size: 8px;
    border-radius: 50%;
    padding: 2px;
    width: 15px;
    height: 15px;
  `;
  const getChatLink = useCallback(() => {
    if (isChatAdmin) {
      return "/chats/";
    }
    return "/chats/support";
  }, [user]);
  const updateRoomsListCounter = () => {
    getRoomsList().then((el) => {
      if (isChatAdmin && el && el.data) {
        const listAllRooms = el.data.listAllRooms;
        const arrayUnreadMsgs = getArrayOfValuesByKey(
          listAllRooms,
          "unreadMsgs"
        );
        setUnreadMsgsCount(reduce(add, 0, arrayUnreadMsgs));
      } else {
        const getSupportRoom = el && el.data ? el.data.getSupportRoom : null;
        if (getSupportRoom) {
          setUnreadMsgsCount(getSupportRoom.unreadMsgs);
        }
      }
    });
  };
  useEffect(() => {
    updateRoomsListCounter();
  }, []);
  useSubscription(NEW_MESSAGE_ADDED, {
    variables: { userId: user.me.id },
    onSubscriptionData: () => {
      updateRoomsListCounter();
    },
  });
  useSubscription(MESSAGE_UPDATED, {
    variables: { userId: user.me.id },
    onSubscriptionData: (data) => {
      updateRoomsListCounter();
    },
  });
  if (uLoading) return <Loader />;
  if (uError) {
    console.error(uError);
    return null;
  }

  return (
    <UncontrolledDropdown>
      <Link to={getChatLink()}>
        <DropdownToggle className="p-0" color="link">
          <div className="icon-wrapper ">
            <i
              style={{ color: (isNotifications && errorColor) || darkGrey }}
              className="fas fa-bell"
            />
            <div
              className={`icon-wrapper-bg ${
                isNotifications ? "bg-danger" : "bg-secondary"
              }`}
            />
            {isNotifications ? (
              <NotificationCountHelper>
                {notificationsCount}
              </NotificationCountHelper>
            ) : null}
          </div>
        </DropdownToggle>
      </Link>
    </UncontrolledDropdown>
  );
};

export default Notificator;
