import { is, curry } from 'ramda'

const Route = {
  HOME: 'home',
}

const setAtr = (el, atr, prefix = '', postfix = '') => {
  return el[atr] ? `${prefix || ''}${atr}=${el[atr]}${postfix || ''}` : ''
}

const assocLinks = curry(
  (listRoutes, route) =>
    is(Array, listRoutes) &&
    is(String, route) &&
    listRoutes.map((el) => {
      el.link = `/${route}?${setAtr(el, 'tab')}&${setAtr(el, 'id')}`
      return el
    }),
)

export const mergeTab = curry(
  (lib, tabsList) =>
    is(Object, lib) &&
    is(Array, tabsList) &&
    tabsList
      .map((el) => {
        el.component = lib[el.tab]?.component
        return { ...el, ...lib[el.tab] }
      })
      .filter((el) => el.component),
)

export const simsTabs = assocLinks(
  [
    {
      name: 'Список сим карт',
      tab: 'list',
      icon: 'fal fa-clipboard-list',
    },
    {
      name: 'Выбранная сим карта',
      tab: 'specific',
      icon: 'fal fa-clipboard-list',
    },
  ],
  'sim_cards',
)

export const machineTabs = assocLinks(
  [
    {
      name: 'Информация',
      tab: 'info',
      icon: 'fal fa-info-circle',
    },
    {
      name: 'Условия валидности',
      tab: 'valid',
      icon: 'fal fa-sensor-smoke',
    },
    {
      name: 'Данные о местоположении',
      tab: 'geo',
      icon: 'fal fa-globe-stand',
    },
    {
      name: 'Данные о пакетах',
      tab: 'packlogs',
      icon: 'fal fa-cubes',
    },
    {
      name: 'Инциденты',
      tab: 'incidents',
      icon: 'fal fa-clipboard-list',
    },
    {
      name: 'Монтажи',
      tab: 'mounts',
      icon: 'fal fa-cogs',
    },
    {
      name: 'Пакеты',
      tab: 'packs',
      icon: 'fal fa-cubes',
    },
    {
      name: 'Алгоритм вычисления',
      tab: 'algoritms',
      icon: 'fal fa-cauldron',
    },
  ],
  'machine',
)

export const deviceTabs = assocLinks(
  [
    {
      name: 'Информация',
      tab: 'info',
      icon: 'fal fa-info-circle',
    },
    {
      name: 'Монтаж',
      tab: 'mount',
      icon: 'fal fa-cogs',
    },
    {
      name: 'Описание теста',
      tab: 'testDesc',
      icon: 'fal fa-vials',
    },
    {
      name: 'Машины',
      tab: 'machines',
      icon: 'fal fa-train',
    },
    {
      name: 'Тип машины',
      tab: 'machineType',
      icon: 'fal fa-dolly-empty',
    },
    {
      name: 'Тест устройства (перв.)',
      tab: 'testFirst',
      icon: 'fal fa-engine-warning',
    },
    {
      name: 'Тест устройства (монтаж.)',
      tab: 'testMounts',
      icon: 'fal fa-steering-wheel',
    },
    {
      name: 'Пакеты',
      tab: 'packs',
      icon: 'fal fa-cubes',
    },
  ],
  'device',
)

export const machineGroupTabs = assocLinks(
  [
    {
      name: 'Информация',
      tab: 'info',
      icon: 'fal fa-info-circle',
    },
    {
      name: 'Управление файлами',
      tab: 'files',
      icon: 'fal fa-file-alt',
    },
    {
      name: 'Управление устройствами',
      tab: 'devices',
      icon: 'fal fa-tablet-rugged',
    },
    {
      name: 'Пакеты',
      tab: 'packs',
      icon: 'fal fa-cubes',
    },
  ],
  'machine_group',
)
export const packsTabs = assocLinks(
  [
    {
      name: 'Пакеты карточками',
      tab: 'cards',
      icon: 'fal fa-clipboard-list',
    },
    {
      name: 'Пакеты таблицей',
      tab: 'table',
      icon: 'fal fa-clipboard-list',
    },
    {
      name: 'Cоздать пакет',
      tab: 'new',
      icon: 'fal fa-clipboard-list',
    },
    {
      name: 'Редактировать пакет',
      tab: 'edit',
      icon: 'fal fa-clipboard-list',
    },
    {
      name: 'Нераспознанные пакеты',
      tab: 'unrecognized',
      icon: 'fal fa-box-full',
    },
  ],
  'packages',
)

export const routes = [
  {
    name: 'Главная',
    link: '/',
    icon: 'fal fa-home',
  },
  {
    name: 'Мастер монтажа',
    link: '/mount_master',
    icon: 'fal fa-flux-capacitor',
  },
  {
    name: 'Управление пакетами',
    link: '/packages',
    icon: 'fal fa-clipboard-list',
    childs: [
      {
        name: 'Пакеты карточками',
        link: '/packages?tab=cards',
        icon: 'fal fa-th-large',
      },
      {
        name: 'Пакеты таблицей',
        link: '/packages?tab=table',
        icon: 'fal fa-table',
      },
      {
        name: 'Cоздать пакет',
        link: '/packages?tab=new',
        icon: 'fal fa-layer-plus',
      },
      {
        name: 'Нераспознанные пакеты',
        link: '/packages?tab=unrecognized',
        icon: 'fal fa-box-full',
      },
    ],
  },
  {
    name: 'Менеджер устройств',
    link: '/devices',
    icon: 'fal fa-tablet-rugged',
    childs: [
      {
        name: 'Список устройств',
        link: '/devices',
        icon: 'fal fa-clipboard-list',
      },
      ...deviceTabs,
    ],
  },
  {
    name: 'Менеджер машин',
    link: '/machines',
    icon: 'fal fa-cogs',
    childs: [
      {
        name: 'Список машин',
        link: '/machines',
        icon: 'fal fa-subway',
      },
      ...machineTabs,
    ],
  },
  {
    name: 'Менеджер симкарт',
    link: '/simcards',
    icon: 'fal fa-sim-card',
    childs: [
      {
        name: 'Список сим карт',
        link: '/simcards',
        icon: 'fal fa-list-alt',
      },
    ],
  },
  {
    name: 'Группы машин',
    link: '',
    icon: 'fal fa-dice-d12',
    childs: [
      {
        name: 'Список групп',
        link: '/machine_groups',
        icon: 'fal fa-subway',
      },
      ...machineGroupTabs,
    ],
  },
  {
    name: 'Менеджер тестов',
    link: '/tests',
    icon: 'fal fa-vial',
    childs: [
      {
        name: 'Устройство (первичн)',
        link: '/test/device',
        icon: 'fal fa-clipboard-list',
      },
      {
        name: 'Устройство (монтаж)',
        link: '/test/mount',
        icon: 'fal fa-clipboard-list',
      },
      {
        name: 'Машина',
        link: '/test/machine',
        icon: 'fal fa-clipboard-list',
      },
      {
        name: 'Местопол. по устройству',
        link: '/test/geo_device',
        icon: 'fal fa-clipboard-list',
      },
      {
        name: 'Фронты. по устройству',
        link: '/test/fronts',
        icon: 'fal fa-clipboard-list',
      },
    ],
  },
  {
    name: 'Справочники',
    link: '/entity',
    icon: 'fal fa-server',
    childs: [
      {
        name: 'Главные',
        link: '/entity',
        icon: 'fal fa-clipboard-list',
      },
      {
        name: 'Вспомогательные',
        link: '/entity/secondary',
        icon: 'fal fa-clipboard-list',
      },
      {
        name: 'Сим карты',
        link: '/entity/sim_cards?tab=list',
        icon: 'fal fa-sim-card',
      },
    ],
  },
  {
    name: 'Управление логами',
    icon: 'fal fa-file-alt',
    childs: [
      {
        name: 'Список логов',
        link: '/logs',
        icon: 'fal fa-list-ul',
      },
      {
        name: 'Управление логом',
        link: '/log',
        icon: 'fal fa-cog',
      },
    ],
  },
  {
    name: 'Инструменты',
    icon: 'fal fa-tools',
    childs: [
      {
        name: 'Менеджер приложений',
        link: '/apps',
        icon: 'fal fa-box',
      },
      {
        name: 'Коммуникация с устройствами',
        link: '/devices/communication',
        icon: 'fal fa-wifi',
      },
    ],
  },
  {
    name: 'Эмуляторы',
    link: '/machines',
    icon: 'fal fa-vial',
    childs: [
      {
        name: 'Эмулятор устройства',
        link: '/emulator',
        icon: 'fal fa-car',
      },
    ],
  },
  {
    name: 'Авторизационный',
    link: '',
    icon: 'fal fa-cog',
    childs: [
      {
        name: 'Управление пользователями',
        link: '/admin/manage',
        icon: 'fal fa-user-shield',
      },
      {
        name: 'Управление сайтами',
        link: '/admin/sites',
        icon: 'fal fa-server',
      },
    ],
  },
]

export default Route
