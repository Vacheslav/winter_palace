import { request, getJwtToken } from "@/utils/index";

const fetchData = async (props) => {
  const token = getJwtToken(props);
  console.log(token);
  const user = request({ schema: "ME", token }).catch((e) => {
    console.log(e);
    return {};
  });

  return {
    ...(await user),
  };
};

export const getServerSidePropsFunc = async (props) => {
  const { locale } = props;

  const user = {
    type: null,
  };

  const REQUESTS = process.env.REQUESTS_TYPE || "faker";

  if (user?.type === "request-timeout")
    return {
      props: {
        me: {
          user: {
            type: null,
          },
        },
        status: "timeout",
      },
    };

  return {
    props: {
      //      me: user?.me,
      status: null,
      REQUESTS,
    },
  };
};
