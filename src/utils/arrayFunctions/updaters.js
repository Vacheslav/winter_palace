/* eslint-disable import/prefer-default-export */

export const insertKeysInArrObjs = (arr) => {
  return arr.map((el, idx) => {
    el.key = idx;
    return el;
  });
};

export const arrReplacer = ({ list = [], newItem = {}, field = "" }) => {
  let answer;
  try {
    const curPosEl = list.findIndex((el) => el[field] === newItem[field]);
    if (curPosEl > -1) {
      answer = list;
      answer = [
        ...list.slice(0, curPosEl),
        newItem,
        ...list.slice(curPosEl + 1, list.length),
      ];
    } else {
      throw Error("arrReplacer: Не найден элемент в массиве");
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
  }
  return answer;
};

export const arrRemove = ({ list = [], value = "", field = "" }) => {
  let answer;
  try {
    const curPosEl = list.findIndex((el) => {
      return el[field] === value;
    });
    if (curPosEl > -1) {
      answer = list;
      answer = [
        ...list.slice(0, curPosEl),
        ...list.slice(curPosEl + 1, list.length),
      ];
    } else {
      throw Error("arrRemove: Не найден элемент в массиве");
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
  }
  return answer;
};
