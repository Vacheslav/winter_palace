import { arrReplacer } from './updaters'

const data = [
  {
    name: 'lora',
    age: 23,
    contry: 'astana',
  },
  {
    name: 'imragim',
    age: 7,
    contry: 'russia',
  },
  {
    name: 'zolton',
    age: 65,
    contry: 'astana',
  },
  {
    name: 'victor',
    age: 42,
    contry: 'astana',
  },
]

const expected = [
  {
    name: 'lora',
    age: 25,
    contry: 'england',
  },
  {
    name: 'imragim',
    age: 7,
    contry: 'russia',
  },
  {
    name: 'zolton',
    age: 65,
    contry: 'astana',
  },
  {
    name: 'victor',
    age: 42,
    contry: 'astana',
  },
]

describe('arrReplacer', () => {
  test('should change by field name', () => {
    console.log(
      arrReplacer({
        list: data,
        newItem: { name: 'lora', age: 25, contry: 'england' },
        field: 'name',
      }),
    )

    console.log(expected)
    expect(
      arrReplacer({
        list: data,
        newItem: { name: 'lora', age: 25, contry: 'england' },
        field: 'name',
      }),
    ).toEqual(expected)
  })
})
