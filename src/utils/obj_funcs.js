import { isEmpty, cond, is, T, isNil } from 'ramda'
import { isArr } from './validators'

const clearItem = ([key, value]) =>
  cond([
    [(x) => isArr(x) && isEmpty(x), () => {}],
    [isNil, () => {}],
    [T, () => ({ [key]: value })],
  ])(value)

const clearNoNull = (obj) => {
  return (
    is(Object, obj) && {
      ...Object.entries(obj).reduce(
        (acc, cur) => ({
          ...acc,
          ...clearItem(cur),
        }),
        {},
      ),
    }
  )
}

export const fromEntries = (list = []) =>
  isArr(list) &&
  list.reduce(
    (acc, cur) => ({
      ...acc,
      [cur[0]]: cur[1],
    }),
    {},
  )

export const x = 1

export { clearNoNull }
