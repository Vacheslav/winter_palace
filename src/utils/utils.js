import {
  sort,
  trim,
  curry,
  is,
  cond,
  T,
  pipe,
  path,
  equals,
  assoc,
} from 'ramda'
import { isArr, isString } from './validators'
import faker from 'faker'
import { parseSepareteDate } from './time_funcs'

export const trimIfNeed = (v) => (isString(v) ? trim(v) : v)
export const sortByIndex = (source) => sort((a, b) => a.index - b.index, source)
export const numberWithCommas = (x) =>
  x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
export const stringify = curry((a, b, c) => JSON.stringify(c, a, b))
export const parse = (str) => JSON.parse(str)
export const randKey = () => Math.random().toString(32).substr(4)
export const redirectIfNeed = (checkItem, path = '/') => {
  if (checkItem) {
    window.location.href = path
  }
}

export const slicer = (str, limit = 30) => {
  if (str && str?.length > limit) return `${str.slice(0, limit - 3)}...`
  return str
}

export const reduceLs = (list) =>
  slicer(list.reduce((acc, cv) => (acc ? `${acc}, ${cv}` : `${cv}`)).trim())
const reduceOs = (list) =>
  slicer(
    list
      .reduce(
        (acc, { paramName: key, paramValue: value }) =>
          acc ? `${acc}, {${key}: ${value}}` : `{${key}: ${value}}`,
        null,
      )
      .trim(),
  )

export const stringifyList = (list) => {
  if (list?.name) return list.name
  if (is(String)(list)) return slicer(list.replace(' ', ', '))
  if (!Array.isArray(list)) return list
  return cond([
    [is(String), () => reduceLs(list)],
    [is(Object), () => reduceOs(list)],
    [T, (x) => x],
  ])(list[0])
}

export const getErrorMesage = (e) => {
  return pipe(
    JSON.stringify,
    JSON.parse,
    path(['response', 'errors']),
    (x) => is(Array, x) && x[0],
    (x) => x?.message,
  )(e)
}

export const getErrorStatus = (e) => {
  return cond([
    [equals('user not authenticated'), () => 401],
    [equals('ECONNREFUSED'), () => -1],
    [equals('EHOSTUNREACH'), () => -1],
    [equals('ENOTFOUND'), () => -1],
    [T, () => 400],
  ])(getErrorMesage(e) || e?.code)
}

export const getAvPages = (role) =>
  cond([
    [equals('ОРИС'), () => '/auth'],
    [T, () => '/auth'],
  ])(role)

export const withKeys = (arr = []) =>
  isArr(arr) && arr.map((el) => assoc('key', randKey(), el))

export const withIdx = (arr = []) =>
  isArr(arr) && arr.map((el, idx) => assoc('index', idx + 1, el))

export const delay = (ms) => {
  return new Promise((resolve) => {
    setTimeout(resolve, ms)
  })
}

export const randDate = ({ start = '2017-01-01', end = new Date() }) => {
  const { between } = faker.date
  return parseSepareteDate(between(start, end), '-', 'DateDataBase')
}

export const withSorter = (field, type) => ({
  sorter: ({ [field]: a }, { [field]: b }) =>
    cond([
      [equals('number'), () => a - b],
      [equals('date'), () => new Date(a).getTime() - new Date(b).getTime()],
      [equals('string'), () => `${a}`.localeCompare(b)],
      [T, () => null],
    ])(type),
})
