const substring = (string, length = 40) => {
  if (string && `${string}`.length > length) {
    return `${string}`.substring(0, length) + "...";
  }
  return string;
};

export default substring;
