const upFirstLetter = (string) => {
  if (string) return string.charAt(0).toUpperCase() + string.slice(1);
  else {
    const error = `error by func capitalizeFirstLetter, ivalid string. string can't equal ${string}`;
    console.error(error);
    return error;
  }
};

export default upFirstLetter;
