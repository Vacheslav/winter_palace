import toObject from './index'

describe('toObject ', () => {
  test('for nums fields', () => {
    const data = [
      { a: 1, b: false },
      { a: 2, b: true },
    ]

    const newItem = toObject(
      'a',
      () => 'valid',
      (v) => v * 2,
      data,
    )
    expect(newItem).toEqual({
      2: 'valid',
      4: 'valid',
    })
  })

  test('for string fields', () => {
    const data = [
      { a: 14.2, b: 'ser' },
      { a: 2.3, b: 'vyc' },
    ]

    const newItem = toObject(
      'b',
      (v, idx) => `puma${idx}`,
      (v) => `${v}_dom`,
      data,
    )
    expect(newItem).toEqual({
      ser_dom: 'puma0',
      vyc_dom: 'puma1',
    })
  })

  test('for null', () => {
    const data = null

    const newItem = toObject(
      'b',
      (v, idx) => `puma${idx}`,
      (v) => `${v}_dom`,
      data,
    )
    expect(newItem).toEqual(null)
  })

  test('for empty', () => {
    const data = []

    const newItem = toObject(
      'b',
      (v, idx) => `puma${idx}`,
      (v) => `${v}_dom`,
      data,
    )
    expect(newItem).toEqual({})
  })
})
