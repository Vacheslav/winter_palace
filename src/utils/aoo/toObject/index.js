import { curry } from 'ramda'
import { isArr } from '../../validators'

const AOOtoObject = curry((field, fVal, fField, aoo) => {
  if (!isArr(aoo)) return null

  return aoo.reduce(
    (acc, cur, idx) => ({
      ...acc,
      [fField(cur[field], idx)]: fVal(cur[fField], idx),
    }),
    {},
  )
})

export default AOOtoObject
