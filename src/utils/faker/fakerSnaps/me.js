const me = {
  accessTemplateId: 883,
  firstName: "Вячеслав",
  id: 905,
  login: "superuser",
  middleName: "Андреевич",
  notes: null,
  roles: [
    { id: 1, slug: "чат" },
    { id: 2, slug: "пользователь" },
    { id: 3, slug: "администратор" },
    { id: 4, slug: "суперпользователь" },
  ],
  surname: "Буглов",
};

export default me;
