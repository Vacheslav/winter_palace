import { generator } from "@dev-bi/rct_utils";
import faker from "faker";
import {
  availableMachines,
  me,
  GET_ON_OFF_AGREGATED_DRP_REPORT,
} from "./fakerSnaps/index";

const snapshots = {
  LIST_MACHINES_INFO: () => ({}),
  GET_AVAILABLE_MACHINES: () => availableMachines,
  ME: () => me,
  SIGN_IN_MUTATION: () => ({
    token: "token_token",
    user: {
      id: 832,
      login: "buglov",
    },
  }),
  ASKR_ON_OFF_REPORT_ALL: () => [
    {
      hadConnection: 23,
      hadNotConnection: 516,
      machines: 539,
    },
  ],
  ASKR_ON_OFF_REPORT_MACGINES_WORKED_ALL: () => [
    {
      machinesWorked: 418,
      machinesNotWorked: 52,
      machines: 533,
      noData: 66,
    },
  ],
  GET_WORK_REPORT_DIAGRAM_DATA_BYTYPES: () => [
    {
      classificationParentId: 140,
      classificationParentName: "УК СП",
      machinesWorked: 30,
      machinesNotWorked: 30,
      noData: 2,
      machines: 62,
    },
    {
      classificationParentId: 141,
      classificationParentName: "ВПО",
      machinesWorked: 6,
      machinesNotWorked: 21,
      noData: 0,
      machines: 27,
    },
    {
      machinesWorked: 2,
      machinesNotWorked: 50,
      noData: 3,
      machines: 55,
      classificationParentId: 142,
      classificationParentName: "ВПО-С",
    },
  ],
  GET_WORK_REPORT_DIAGRAM_DATA_BYDRP: () => [
    {
      machines: 7,
      machinesWorked: 3,
      machinesNotWorked: 1,
      noData: 3,
      id: null,
      machines: 4,
      railwayId: 51,
      railwayName: "С-Кав",
    },
    {
      machines: 3,
      machinesWorked: 0,
      machinesNotWorked: 0,
      noData: 3,
      id: null,
      machines: 4,
      railwayId: 58,
      railwayName: "Ю-Вост",
    },
    {
      machines: 5,
      machinesWorked: 2,
      machinesNotWorked: 0,
      noData: 3,
      id: null,
      machines: 3,
      railwayId: 61,
      railwayName: "Прив",
    },
    {
      machines: 3,
      machinesWorked: 1,
      machinesNotWorked: 1,
      noData: 1,
      id: null,
      machines: 3,
      railwayId: 63,
      railwayName: "Кбш",
    },
  ],
  GET_WORK_REPORT_DIAGRAM_DATA_BYORG: () => [
    {
      drpName: "Моск",
      machines: 5,
      machinesWorked: 1,
      machinesNotWorked: 1,
      noData: 3,
      id: 1190,
      orgId: 1190,
      orgName: "ОПМС-68",
    },
    {
      drpName: "Моск",

      machines: 3,
      machinesWorked: 1,
      machinesNotWorked: 2,
      noData: 0,
      id: 1223,
      machines: 1,
      orgId: 1223,
      orgName: "ПМС-12",
    },
    {
      drpName: "Моск",
      machines: 2,
      machinesWorked: 2,
      machinesNotWorked: 0,
      noData: 0,

      id: 1308,
      machines: 1,
      orgId: 1308,
      orgName: "ПМС-55",
    },
  ],
  GET_WORK_REPORT_DIAGRAM_DATA_BYSPS: () => [
    {
      machines: 1,
      machinesWorked: 1,
      machinesNotWorked: 0,
      noData: 0,
      drpName: "Моск",
      id: 3234,
      machines: 539,
      orgName: "ПМС-12",
      sps: "УК-25/28СП N41",
    },
  ],
  ASKR_ON_OFF_REPORT_BYTYPES: () => [
    {
      classificationParentId: 140,
      classificationParentName: "УК СП",
      hadConnection: 5,
      hadNotConnection: 57,
      machines: 62,
    },
    {
      classificationParentId: 141,
      classificationParentName: "ВПО",
      hadConnection: 6,
      hadNotConnection: 21,
      machines: 27,
    },
    {
      classificationParentId: 142,
      classificationParentName: "ВПО-С",
      hadConnection: 2,
      hadNotConnection: 53,
      machines: 55,
    },
    {
      classificationParentId: 143,
      classificationParentName: "УК",
      hadConnection: 37,
      hadNotConnection: 165,
      machines: 202,
    },
    {
      classificationParentId: 144,
      classificationParentName: "МПК",
      hadConnection: 27,
      hadNotConnection: 166,
      machines: 193,
    },
  ],
  GET_ON_OFF_AGREGATED_DRP_REPORT: () => GET_ON_OFF_AGREGATED_DRP_REPORT,
  GET_ON_OFF_AGREGATED_ORG_REPORT: () => [
    {
      drpName: "Моск",
      hadConnection: 0,
      hadNotConnection: 1,
      id: 1190,
      machines: 1,
      orgId: 1190,
      orgName: "ОПМС-68",
    },
    {
      drpName: "Моск",
      hadConnection: 0,
      hadNotConnection: 1,
      id: 1223,
      machines: 1,
      orgId: 1223,
      orgName: "ПМС-12",
    },
    {
      drpName: "Моск",
      hadConnection: 0,
      hadNotConnection: 1,
      id: 1308,
      machines: 1,
      orgId: 1308,
      orgName: "ПМС-55",
    },
    {
      drpName: "Моск",
      hadConnection: 1,
      hadNotConnection: 1,
      id: 1224,
      machines: 2,
      orgId: 1224,
      orgName: "ПМС-58",
    },
    {
      drpName: "Моск",
      hadConnection: 0,
      hadNotConnection: 1,
      id: 1225,
      machines: 1,
      orgId: 1225,
      orgName: "ПМС-96",
    },
  ],
  GET_ON_OFF_AGREGATED_MACHINE_REPORT: () => [
    {
      drpName: "Моск",
      hadConnection: 0,
      hadNotConnection: 1,
      id: 3234,
      machines: null,
      machines: 539,
      orgName: "ПМС-12",
      sps: "УК-25/28СП N41",
    },
  ],
};

const names = {
  listApplicationsNames: "listApplications",
  SIGN_IN_MUTATION: "signin",
  ME: "me",
  GET_AVAILABLE_MACHINES: "availableMachines",
  ASKR_ON_OFF_REPORT_MACGINES_WORKED_ALL: "getWorkReportDiagramData",
  GET_WORK_REPORT_DIAGRAM_DATA_BYTYPES: "getWorkReportDiagramData",
  GET_WORK_REPORT_DIAGRAM_DATA_BYDRP: "getWorkReportDiagramData",
  GET_WORK_REPORT_DIAGRAM_DATA_BYORG: "getWorkReportDiagramData",
  GET_WORK_REPORT_DIAGRAM_DATA_BYSPS: "getWorkReportDiagramData",
  ASKR_ON_OFF_REPORT_ALL: "askrOnOffReport",
  ASKR_ON_OFF_REPORT_BYTYPES: "askrOnOffReport",
  GET_ON_OFF_AGREGATED_DRP_REPORT: "askrOnOffReport",
  GET_ON_OFF_AGREGATED_ORG_REPORT: "askrOnOffReport",
  GET_ON_OFF_AGREGATED_MACHINE_REPORT: "askrOnOffReport",
};

// eslint-disable-next-line import/prefer-default-export
export const generateAPIData = (schema, ...args) => {
  return snapshots[schema]
    ? {
        [names[schema] || schema]: typeof snapshots[schema]
          ? snapshots[schema](...args)
          : snapshots[schema],
      }
    : null;
};
