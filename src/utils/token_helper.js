import CacheStore from "./cache_store";

export const isServerSide = typeof window === "undefined";
export const isClientSide = !isServerSide;
export const getJwtToken = (props) =>
  isServerSide
    ? CacheStore.cookie.from_req(props.req, "jwtToken")
    : CacheStore.get("userToken");

export const dropSession = () => {
  CacheStore.remove("user");
  CacheStore.remove("token");
  CacheStore.cookie.remove("jwtToken");
  localStorage.removeItem("auth-token");
  localStorage.removeItem("login");
  localStorage.removeItem("last-logged");
  localStorage.removeItem("second-token");
  localStorage.removeItem("second-login");
  localStorage.removeItem("second-logged");
  localStorage.removeItem("token");
  localStorage.removeItem("user");
};
