import { curry, and, has, pathEq, T, equals, cond } from "ramda";
import { GraphQLClient } from "graphql-request";
import { GRAPHQL_ENDPOINT, REQUESTS } from "@/config/index";
import { M } from "@/schemas/index";
import { generateAPIData } from "./faker";
import { nilOrEmpty } from "./validators";

export const asyncRes = curry((key, obj) => and(obj[key], has(key, obj)));
export const asyncErr = (key) => pathEq(["error"], key);

export const makeGQClient = async ({ token, timeout }) => {
  const headers = {};

  if (!nilOrEmpty(token)) {
    headers.authorization = `Bearer ${token}`;
  }
  return new GraphQLClient(GRAPHQL_ENDPOINT, { headers, timeout });
};

export const request = async ({
  token = null,
  schema,
  data,
  timeout = 3000,
}) => {
  const gqlClient = token
    ? await makeGQClient({ token, timeout })
    : await makeGQClient({ timeout });

  return cond([
    [equals("faker"), () => generateAPIData(schema)],
    [
      () => !!M[schema],
      () => {
        let answer = null;
        answer = gqlClient.request(M[schema], data);
        return answer;
      },
    ],
    [
      T,
      () => {
        throw new Error("graphql_helper.request error");
      },
    ],
  ])(REQUESTS);
};

export const later = (func, time = 200) => setTimeout(func, time);
