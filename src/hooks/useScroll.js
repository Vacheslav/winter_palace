import { useLayoutEffect, useState } from "react";

const useScroll = () => {
  const [coords, setCoords] = useState({ x: 0, y: 0 });

  const onScrollByUse = () => {
    setCoords({ x: window.pageXOffset, y: window.pageYOffset });
  };

  useLayoutEffect(() => {
    window.addEventListener("scroll", onScrollByUse);
    return () => {
      window.removeEventListener("scroll", onScrollByUse);
    };
  }, []);

  return coords;
};

export default useScroll;
