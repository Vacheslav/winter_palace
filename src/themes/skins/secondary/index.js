import { lighten, darken } from "polished";
import { color } from "./default";
import defaultParams from "./default";
import sidebar from "./sidebar";

const main = {
  ...defaultParams,
  sidebar,
};

export default main;
