import { geekBlue, cyan, black } from "../../pieces/colors.json";
import { lighten } from "polished";

const defaultParams = {
  name: "main",
  shadow: "0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)",
  colors: {
    primaryColor: geekBlue,
    secondaryColor: cyan,
    primaryNeitral: lighten(0.2, black),
  },
  font: {
    headlines: "Nunito",
    text: "Nunito",
    primaryColor: lighten(0.1, black),
    secondaryColor: lighten(0.3, black),
  },
};

export default defaultParams;
