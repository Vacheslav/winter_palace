import { lighten, darken } from "polished";
import { color } from "./default";
import defaultParams from "./default";
import sidebar from "./sidebar";
import header from "./header.json";

const main = {
  ...defaultParams,
  sidebar,
  header,
};

export default main;
