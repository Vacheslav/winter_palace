const sidebar = {
  backgroundColor: "white",
  zIndex: 15,
  width: "220px",
  paddingLeft: "5px",
  shadow: "2px 52px 6px 0px rgba(34, 60, 80, 0.1)",
};

export default sidebar;
