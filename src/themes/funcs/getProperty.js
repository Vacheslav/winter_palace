import skins from "../skins";
import { path } from "ramda";

const getProperty = (themeName = "", pathProp = [""]) =>
  path([themeName, ...pathProp], skins) || path(["main", ...pathProp], skins);

export default getProperty;
