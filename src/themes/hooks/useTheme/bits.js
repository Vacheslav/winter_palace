import { path } from "ramda";
import { skins } from "../../index";

export default ({ themeName, type }) => {
  // if (typeof window !== undefined) {
  //   const sidebarType = localStorage.getItem("sidebar-type");
  // }

  // if (!sidebarType) {
  //   localStorage.setItem("sidebar-type", "default");
  // }
  // console.log(sidebarType);

  const themeBits = {
    Sidebar: {
      ...path([themeName, "sidebar"], skins),
    },
    Skin: {
      ...path([themeName], skins),
    },
    SidebarLogo: {
      headerHeight: path(["main", "header"], skins)?.height,
      sidebarPaddingLeft: path([themeName, "sidebar"], skins)?.paddingLeft,
    },
    Header: {
      ...path(["main", "header"], skins),
      shadow: path(["main", "shadow"], skins),
    },
  };

  if (!themeBits[type]) {
    console.error("Внимание! стиль не найден");
  }

  return themeBits[type];
};
