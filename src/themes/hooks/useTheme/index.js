import getBit from "./bits";

const useTheme = ({ type }) => {
  //TODO написать функцию для получения темы из стора
  // const themeName = useSelector((state) => state.theme.currentTheme);
  const themeName = "main";
  return getBit({ themeName, type });
};

export default useTheme;
