/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import { MainPageWrapper } from "@/wrappers/index";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { getServerSidePropsFunc } from "@/utils/index";

import dynamic from "next/dynamic";

const Model = dynamic(
  () => import("../../src/components/5_pages/Model/index"),
  {
    ssr: false,
  }
);

export const getServerSideProps = async (props) =>
  getServerSidePropsFunc(props, serverSideTranslations);

const Index = (props) => {
  return (
    <MainPageWrapper {...props}>
      <Model />
    </MainPageWrapper>
  );
};

export default Index;
