import React from "react";
import FatalError from "@/components/4_templates/FatalError";
import "../styles/normalizer.css";
import "../styles/index.css";
import "../styles/font.css";
import "../styles/snippets.css";

import "../styles/fontawesome/font_awesome.css";
import "antd/dist/antd.css";

import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/navigation";
import "swiper/css/pagination";

import "leaflet/dist/leaflet.css";
import { appWithTranslation } from "next-i18next";

const App = ({ Component, pageProps, err }) => {
  return err ? (
    <FatalError />
  ) : (
    <div>
      <Component {...pageProps} />
    </div>
  );
};

export default appWithTranslation(App);
