/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import { MainPageWrapper } from "@/wrappers/index";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { getServerSidePropsFunc } from "@/utils/index";
import { History } from "@/pages/index";

export const getServerSideProps = async (props) =>
  getServerSidePropsFunc(props, serverSideTranslations);

const Index = (props) => {
  return (
    <MainPageWrapper {...props}>
      <History />
    </MainPageWrapper>
  );
};

export default Index;
