/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import { MainPageWrapper } from "@/wrappers/index";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { getServerSidePropsFunc } from "@/utils/index";
import dynamic from "next/dynamic";

const StatisticPage = dynamic(
  () => import("../../src/components/5_pages/Statistic/index"),
  {
    ssr: false,
  }
);

export const getServerSideProps = async (props) =>
  getServerSidePropsFunc(props, serverSideTranslations);

const Statistic = (props) => {
  return (
    <MainPageWrapper {...props}>
      <StatisticPage />
    </MainPageWrapper>
  );
};

export default Statistic;
