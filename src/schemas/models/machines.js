const LIST_MACHINES_INFO = `
  mutation{
    listMachinesInfo(filters: {filterBy: "", filterCase:""}) {
      imei
      type
      orgName
      machineId
      deviceNumber
      modification
      zns8zn
      zavNomer
      railwayName
    }
  }
`;

const GET_AVAILABLE_MACHINES = `
  query {
    availableMachines {
      typeId
      drpName
      orgName
      typeName
      zavNomer
      machineId
      orgId
      drpId
      imei
    }
  }
`;

export { LIST_MACHINES_INFO, GET_AVAILABLE_MACHINES };
