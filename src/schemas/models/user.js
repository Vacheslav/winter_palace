const ME = `
query {
  me {
    id
    firstName
    middleName
    surname
    accessTemplateId
    login
    notes
    roles {
      slug
      id
    }
  }
}
`;

const SIGN_IN_MUTATION = `
mutation($login: String, $password: String!) {
  signin(login: $login, password: $password) {
    token
    user {
      id
      login
    }
  }
}
`;

export { ME, SIGN_IN_MUTATION };
