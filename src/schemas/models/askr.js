export const ASKR_ON_OFF_REPORT_ALL = `
  mutation ($start: String, $finish: String, $machines: Int) {
    askrOnOffReport(
      dates: { start: $start, finish: $finish }
      group_by: { totals: "true" }
      filters: { filterBy: "machineId", filterCase: $machines }
    ) {
      machines
      hadConnection
      hadNotConnection
    }
  }
`;

export const ASKR_ON_OFF_REPORT_BYTYPES = `
  mutation ($start: String, $finish: String, $filterCase: Int) {
    askrOnOffReport(
      dates: { start: $start, finish: $finish }
      group_by: { classifications: [] }
      filters: { filterBy: "machineId", filterCase: $filterCase }
    ) {
      classificationParentId
      classificationParentName
      machines
      hadConnection
      hadNotConnection
    }
  }
`;

export const GET_ON_OFF_AGREGATED_DRP_REPORT = `
  mutation (
    $start: String
    $finish: String
    $classifications: [Int]
    $filterCase: [Int]
  ) {
    askrOnOffReport(
      dates: { start: $start, finish: $finish }
      group_by: { classifications: $classifications, railways: [] }
      filters: { filterBy: "machineId", filterCase: $filterCase }
    ) {
      id
      railwayId
      railwayName
      machines
      machinesTotal
      hadConnection
      hadNotConnection
    }
  }
`;

export const GET_ON_OFF_AGREGATED_ORG_REPORT = `
  mutation (
    $start: String
    $finish: String
    $railways: [Int]
    $classifications: [Int]
    $filterCase: [Int]
  ) {
    askrOnOffReport(
      dates: { start: $start, finish: $finish }
      group_by: {
        classifications: $classifications
        railways: $railways
        orgs: []
      }
      filters: { filterBy: "machineId", filterCase: $filterCase }
    ) {
      id
      drpName
      machines
      orgId
      orgName
      machinesTotal
      hadConnection
      hadNotConnection
    }
  }
`;

export const GET_ON_OFF_AGREGATED_MACHINE_REPORT = `
  mutation (
    $start: String
    $finish: String
    $railways: [Int]
    $orgs: [Int]
    $classifications: [Int]
    $filterCase: [Int]
  ) {
    askrOnOffReport(
      dates: { start: $start, finish: $finish }
      group_by: {
        classifications: $classifications
        railways: $railways
        orgs: $orgs
        machines: []
      }
      filters: { filterBy: "machineId", filterCase: $filterCase }
    ) {
      id
      drpName
      orgName
      sps
      machines
      machinesTotal
      hadConnection
      hadNotConnection
    }
  }
`;

export const ASKR_ON_OFF_REPORT_MACGINES_WORKED_ALL = `
  mutation{
    getWorkReportDiagramData(
      dates: { start: $start, finish: $finish }
      group_by: { totals: "true" }
      filters: { filterBy: "machineId", filterCase: $machines }
    ) {
      machinesWorked
      machinesNotWorked
      machinesTotal
      noData
    }
  }
`;
