import {
  ME,
  SIGN_IN_MUTATION,
  LIST_MACHINES_INFO,
  GET_AVAILABLE_MACHINES,
  ASKR_ON_OFF_REPORT_ALL,
  ASKR_ON_OFF_REPORT_BYTYPES,
  GET_ON_OFF_AGREGATED_DRP_REPORT,
  GET_ON_OFF_AGREGATED_MACHINE_REPORT,
  GET_ON_OFF_AGREGATED_ORG_REPORT,
} from "./models";

const M = {
  ME,
  SIGN_IN_MUTATION,
  LIST_MACHINES_INFO,
  GET_AVAILABLE_MACHINES,
  ASKR_ON_OFF_REPORT_ALL,
  ASKR_ON_OFF_REPORT_BYTYPES,
  GET_ON_OFF_AGREGATED_DRP_REPORT,
  GET_ON_OFF_AGREGATED_MACHINE_REPORT,
  GET_ON_OFF_AGREGATED_ORG_REPORT,
};

export { M };
