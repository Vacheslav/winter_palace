import React from "react";
import { LayoutWrapper } from "@/templates/index";
import T from "prop-types";

const MainPageWrapper = ({ children }) => {
  return <LayoutWrapper>{children}</LayoutWrapper>;
};

MainPageWrapper.propTypes = {
  me: T.any,
  status: T.any,
  children: T.node,
};

MainPageWrapper.defaultProps = {
  me: {},
  status: null,
  children: null,
};

export default MainPageWrapper;
