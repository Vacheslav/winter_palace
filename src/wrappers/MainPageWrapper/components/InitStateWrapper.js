import { useState, useLayoutEffect } from "react";
import { cond, T } from "ramda";
import { connectStore } from "@/store/index";
import { dropSession, request } from "@/utils/index";

const InitStateWrapper = ({ me, children, user }) => {
  const [redirect, setRedirect] = useState(null);

  const { initMe } = user;

  useLayoutEffect(() => {
    request({ schema: "ME", token: localStorage.getItem("auth-token") })
      .then((el) => {
        if (el?.me?.id) {
          initMe(el.me)
        }
      })
      .catch(() => {});

    if (!localStorage.getItem("token")) {
      dropSession();
    }
    if (redirect) {
      window.location.href = redirect;
    }
  }, [redirect]);


  return children;
};

export default connectStore(InitStateWrapper, "user");
