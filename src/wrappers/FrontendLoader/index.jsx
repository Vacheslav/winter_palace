import { useLayoutEffect, useState } from "react";
import { Result, Spin } from "antd";

const FrontendLoader = ({
  children,
  subTitle,
  times = 1000,
  title = "Данные загружаются",
}) => {
  const [isLoading, setLoading] = useState(true);

  useLayoutEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, times);
  }, []);

  return (
    <div>
      {isLoading && (
        <div
          style={{
            position: "absolute",
            backgroundColor: "white",
            zIndex: 10,
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
          }}
        >
          <Result
            icon={<Spin size="large" />}
            subTitle={subTitle}
            title={title}
          />
        </div>
      )}

      <div
        style={{
          zIndex: 1,
        }}
      >
        {children}
      </div>
    </div>
  );
};

export default FrontendLoader;
