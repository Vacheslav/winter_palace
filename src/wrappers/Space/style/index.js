// @ts-nocheck
import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  height: 100%;
  column-gap: 10px;
`;

export { Wrapper };
