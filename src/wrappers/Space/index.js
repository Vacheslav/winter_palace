import React, { useCallback } from "react";
import { randKey, isArr } from "@/utils/index";
import { Wrapper } from "./style";
import T from "prop-types";

const Space = ({ children, breakPoint, size, key, ...restProps }) => {
  if (!children || !isArr(children)) return null;

  return (
    <Wrapper size={size} postfix={key} breakPoint={breakPoint} {...restProps}>
      {children.map((el) => (
        <div key={randKey()} className={`item-${key}`}>
          {el}
        </div>
      ))}
    </Wrapper>
  );
};

Space.propTypes = {
  size: T.number,
  breakPoint: T.number,
  children: T.oneOfType([T.node, T.oneOf([null]).isRequired]),
  key: T.string,
};

Space.defaultProps = {
  size: 20,
  breakPoint: null,
  children: null,
  key: "dm412dw21",
};

export default Space;
