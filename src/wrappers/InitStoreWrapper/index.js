import React from "react";
import { Provider } from "mobx-react";
import { useStore } from "@/store/index";

const InitStoreWrapper = ({ children }) => {
  const store = useStore();

  return <Provider store={store}>{children}</Provider>;
};

export default InitStoreWrapper;
