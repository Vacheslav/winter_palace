import React, { useLayoutEffect, useState } from "react";
import { request } from "@/utils/index";
import { dropSession } from "@/utils/index";

const AuthPageWrapper = ({ children }) => {
  const [isLoaded, setMe] = useState(false);

  useLayoutEffect(() => {
    request({ schema: "ME", token: localStorage.getItem("auth-token") })
      .then((el) => {
        console.log(el);
        if (!el?.me?.id) {
          dropSession();
          window.location.href = "/auth";
          return null;
        }
        setMe(true);
      })
      .catch(() => {
        dropSession();
        window.location.href = "/auth";
      });
  }, []);

  if (!isLoaded) return <div></div>;

  return <div>{children}</div>;
};

export default AuthPageWrapper;
