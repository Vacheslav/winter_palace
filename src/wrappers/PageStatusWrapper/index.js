import React from "react";
import T from "prop-types";
import { UnconnectingPage } from "@/pages/index";

const PageStatusWrapper = ({ children, status }) => {
  if (status === "timeout") return <UnconnectingPage />;

  return <>{children}</>;
};

PageStatusWrapper.propTypes = {
  status: T.any,
  children: T.node,
};

PageStatusWrapper.defaultProps = {
  status: null,
  children: null,
};

export default PageStatusWrapper;
