const withStore = (Store, WrappedComponent) => {
  const ModifiedComponent = (ownProps) => {
    return (
      <Store>
        <WrappedComponent {...ownProps} />
      </Store>
    );
  };

  return ModifiedComponent;
};

export default withStore;
