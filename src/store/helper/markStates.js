import { keys, forEachObjIndexed, contains, isEmpty } from "ramda";

const markStates = (sobj, self) => {
  if (!isObject(sobj)) {
    throw new Error("mark: invalid sobj, exepect a object");
  }
  const selfKeys = keys(self);

  forEachObjIndexed((val, key) => {
    try {
      if (!contains(key, selfKeys)) return false;
      if (
        !isEmpty(val) &&
        !Array.isArray(val) &&
        isObject(val) &&
        self[key] !== null
      ) {
        self[key] = Object.assign(self[key], val);
      } else {
        self = Object.assign(self, { [key]: val });
      }
    } catch (error) {
      console.log(error);
    }
  }, sobj);

  return false;
};

export default markStates;
