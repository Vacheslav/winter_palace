import { head, split, toLower, curry, path } from "ramda";
import { inject } from "mobx-react";
import { observer } from "mobx-react-lite";

const storeFilter = curry((selectedStore, props) => ({
  [selectedStore]: path(["store", selectedStore], props),
}));

const connectStore = (container, store) => {
  let modelName = "";
  console.log(store);
  if (store) {
    modelName = store;
  } else {
    const cname = head(split("Container", container.displayName));
    modelName = toLower(head(cname)) + cname.slice(1);
  }
  return inject(storeFilter(modelName))(observer(container));
};

export default connectStore;
