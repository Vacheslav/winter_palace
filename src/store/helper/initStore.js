import { applySnapshot, types as T } from "mobx-state-tree";
import { uniq } from "ramda";
import { upFirstChar } from "@/utils/index";
import { stores as rawStores } from "../stores/Store";

let clientsideStore;

/**
 * initStore создает стор начальный стор
 * @param {string[]} customStores  - Названия сторов с маленькой буквы local | theme | user
 * @param {string[]} defaultStores  - дефолтные сторы
 *
 * @example
 *  initStore(['packs']) - будут созданы сторы PacksStore и дефолтные
 *  initStore(['packs'], []) - будет только PacksStore
 *  initStore(['packs'], [user]) - будет создан PacksStore и UserStore
 *
 * @returns {Object} store для прровайдера mobx
 */

const initStore = (
  customStores = [],
  defaultStores = ["locale", "user", "theme"]
) => {
  const stores = uniq([...customStores, ...defaultStores]);

  const Store = T.model(
    stores.reduce((acc, storeName) => {
      return {
        ...acc,
        [storeName]: T.optional(
          rawStores[`${upFirstChar(storeName)}Store`],
          {}
        ),
      };
    }, {})
  )
    .views((self) => ({}))
    .actions((self) => ({}));

  const store = Store.create();

  if (typeof window === "undefined") return store;

  if (!clientsideStore) clientsideStore = store;

  return clientsideStore;
};

export default initStore;
