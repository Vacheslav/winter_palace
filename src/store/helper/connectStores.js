import { inject } from "mobx-react";
import { observer } from "mobx-react-lite";

const connectStores = (container) => {
  return inject("store")(observer(container));
};

export default connectStores;
