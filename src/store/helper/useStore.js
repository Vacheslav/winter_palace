import { useMemo } from "react";
import initStore from "./initStore";

const useStore = (initialState) =>
  useMemo(() => initStore(initialState), [initialState]);

export default useStore;
