import { cond, T } from "ramda";

export const clearObject = (item) => {
  return is(Object, item)
    ? {
        ...Object.entries(item).reduce(
          (acc, cur) => ({
            ...acc,
            [cur[0]]: cond([
              [isArr, (x) => clearArr(x)],
              [is(Object), (x) => clearObject(x)],
              [T, (x) => x],
            ])(cur[1]),
          }),
          {}
        ),
      }
    : item;
};

export const clearArr = (arr = []) => {
  return (
    isArr(arr) && arr.map((el) => (isArr(el) ? clearArr(el) : clearObject(el)))
  );
};

//TODO написать функцию из clearObject && clearArr
const clearElement = () => {};

export default clearElement;
