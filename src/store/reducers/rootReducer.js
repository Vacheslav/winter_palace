const rootReducer = {
  counter: {},
  contactForm: {},
  theme: {},
  user: {},
};

export default rootReducer;
