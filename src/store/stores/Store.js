import { types as T } from "mobx-state-tree";
import { ThemeStore, UserStore } from "./bits";

const LocaleStore = T.model("Locale", {
  locale: T.optional(T.enumeration("locale", ["ru", "en"]), "ru"),
})
  .views((self) => ({}))
  .actions((self) => ({}));

const Store = T.model({
  locale: T.optional(LocaleStore, {}),
  theme: T.optional(ThemeStore, {}),
  user: T.optional(UserStore, {}),
})
  .views((self) => ({}))
  .actions((self) => ({}));

const stores = {
  ThemeStore,
  UserStore,
  LocaleStore,
};

export { stores };

export default Store;
