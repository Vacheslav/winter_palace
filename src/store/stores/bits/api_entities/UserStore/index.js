import { types as T } from "mobx-state-tree";
import { openNotifi } from "@/utils/index";

const Role = T.model("Role", {
  id: T.union(T.string, T.number),
  slug: T.string,
});

const Me = T.model("Me", {
  firstName: T.maybeNull(T.string),
  id: T.maybeNull(T.number),
  surname: T.maybeNull(T.string),
  login: T.maybeNull(T.string),
  roles: T.optional(T.array(Role), []),
});

// eslint-disable-next-line import/prefer-default-export
const UserStore = T.model("UserStore", {
  me: T.maybeNull(Me),
})
  .actions((self) => ({
    initMe: (user) => {
      self.me = user;
    },
    cManyValues(object) {
      Object.entries(object).forEach((el) => {
        self.cValue(el[0], el[1]);
      });
    },
    cValue(name, value) {
      try {
        self[name] = value;
      } catch (error) {
        console.log(error);
        openNotifi("Ошибка", "неверный тип данных", "error");
      }
    },
  }))
  .actions((self) => ({}));

export default UserStore;
