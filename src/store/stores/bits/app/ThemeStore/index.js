import { types as T, getParent } from "mobx-state-tree";
import { keys } from "ramda";

// import { DEFAULT_THEME } from "@/themes/";

export const ThemeDefaults = {
  currTheme: "main",
};

const ThemeStore = T.model("ThemeStore", {
  currTheme: T.optional(T.string, "main"),
  menuIsActive: T.optional(T.boolean, false),
})
  .views((self) => ({
    get root() {
      return getParent(self);
    },
    get themeData() {
      return themeSkins[self.currTheme];
    },
  }))
  .actions((self) => ({
    isMemberOf(type) {
      return self.root.isMemberOf(type);
    },
    toggleMenu() {
      self.menuIsActive = !self.menuIsActive;
    },
    cTheme(name) {
      self.currTheme = name;
    },
  }));

export default ThemeStore;
