const { i18n } = require("./next-i18next.config");

const localeSubpaths = {};

module.exports = {
  i18n,
  rewrites: async () => [],
  publicRuntimeConfig: {
    localeSubpaths,
    shallowRender: true,
  },
  webpack: (config) => {
    config.module.rules.push({
      test: /react-spring/,
      sideEffects: true,
    });

    return config;
  },
};
