import { REQUESTS_TYPE } from "./request";

const GRAPHQL_ENDPOINT = "http://91.151.186.110:19999/tsdrp/graphiql";
//const GRAPHQL_ENDPOINT = "http://172.25.78.27:8080/tsdrp/graphiql";
const BUILD_VERSION = "0.0.1";
const DEFAULT_THEME = "main";

const REQUESTS = REQUESTS_TYPE || "faker";

const HOST = "";
const PORT = "";

export { GRAPHQL_ENDPOINT, BUILD_VERSION, DEFAULT_THEME, REQUESTS, HOST, PORT };

//"REQUESTS" = "api" | "faker"
